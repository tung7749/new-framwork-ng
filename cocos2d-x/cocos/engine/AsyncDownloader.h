#pragma once
#include "cocos2d.h"
#include <string>
#include "network/HttpClient.h"

USING_NS_CC;
using namespace std;
using namespace network;
class AsyncDownloader : public Ref
{
public:
	AsyncDownloader(void);
	~AsyncDownloader(void);

	static AsyncDownloader *create(string url,string filePath,std::function<void(int ret,string path)> callback);
	void initDownload(string url,string filePath,std::function<void(int ret,string path)> callback);
	void startDownload();
	void httpCallback(HttpClient* client, HttpResponse* response);
	void setCallback(std::function<void(int ret,string path)> callback);

public:
	string _url;
	string _filename;
	bool _dowloading;
	bool _need_quit;
	void progressDownloaded();
	std::function<void(int ret,string path)> _callback;
};


class HttpMultipart : public Ref
{
public:
	HttpMultipart(string url,std::function<void(string data)> _callback);
	~HttpMultipart();

	void setTargetResponseHttp(CCObject *target,SEL_HttpResponse pSelector);

	void addFormPart(string paramName,string value);
	void addFilePart(string paramName,string fileName,const char* data,unsigned long lengthData);
	void addImage(string paramName,string fileName,string path);

	void executeAsyncTask();

	static HttpMultipart *create(string url,std::function<void(string data)> _callback);
	void setCallback(std::function<void(string data)> callback) {this->callback = callback;}
protected:
	HttpRequest *cRequest;
	string _url;
	string baseData;
	std::function<void(string data)> callback;
	void httpCallback(HttpClient* client, HttpResponse* response);
};
