#pragma once
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include <string>
#include "AsyncDownloader.h"
#include "cocostudio/CCSGUIReader.h"
#include "ui/UILayout.h"
#include "cocostudio/CCSSceneReader.h"
#include "../2d/CCLabel.h"
#include <stdlib.h>
#include <ctype.h>
#include "Handler.h"



using namespace std;
USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;
using namespace cocostudio;

// Default Img la anh ban dau` (kick thuoc ban dau`)
class UIAvatar : public ui::Widget
{
public:
	UIAvatar(void);
	~UIAvatar(void);

	static UIAvatar* create(string id,string url,string defaultImg);
	static UIAvatar* create(string defaultImg);
	static UIAvatar* createWithMask(string defaultImg,string pathMask,string extraImg = "");
	//static string createScreenshot(string name,string logo);
	static string getAppversionString(){
		return UserDefault::getInstance()->getStringForKey("appversion");
	}
	static string getStringForKey(string key)
	{
		return UserDefault::getInstance()->getStringForKey(key.c_str());
	}
	static void setStringForKey(string key,string value)
	{
		UserDefault::getInstance()->setStringForKey(key.c_str(),value.c_str());
		UserDefault::getInstance()->flush();
	}
	static int getIntegerForKey(string key)
	{
		return UserDefault::getInstance()->getIntegerForKey(key.c_str(),-10);
	}
	static void setIntegerForKey(string key,int value)
	{
		UserDefault::getInstance()->setIntegerForKey(key.c_str(),value);
		UserDefault::getInstance()->flush();
	}


	void setDefaultImage();
	void setTexture(Texture2D *texture);
	void setImage(string path);

	Size getImageSize();
	void callbackDownload(int ret,string path);

	void asyncExecute();
	void asyncExecuteWithUrl(string id,string url);

	void setOpacity(GLubyte opac);
protected:

	virtual void initUI(string defaultImg);
	virtual void initWithUrl(string id,string url,string defaultImg);
	virtual void initUI(string defaultImg,string pathMask,string extraImg = "");
	static std::string filePathFromUrl(const char* id);

	string _url;
	string _defaultImg;
	string _id;
	Sprite *_img;
	bool _downloading;
	AsyncDownloader * downloader;
};



class CircleMove : public ActionInterval
{
public:
    static CircleMove* create(float duration,float radius);
    
    virtual void update(float time) override;
    virtual void startWithTarget(Node *target) override;
protected:
    void initWithDuration(float duration,float radius);
    float radius;
    
};

class Loading: public Layer
{
public:
    static Loading* create(string particlePath,float duration,float radius);
    
protected:
    Sprite *particle;
    virtual void onEnter() override;
    
};

class TimeProgressEffect : public ProgressFromTo
{
public:
	TimeProgressEffect(void);
	~TimeProgressEffect(void);

	virtual void update(float time);
	static TimeProgressEffect* create(CCProgressTimer *timer,float duration,float percentFrom);
	void setNen(Sprite *);
protected:
	ProgressTimer *_timer;
	Sprite *_nen;

	float startPercent;
	float totalTime;
	float duration;
};

class MoveCircle : public ActionInterval
{
public:
	static MoveCircle* create(float duration,float radius,float alphaBegin,float alphaEnd);

	virtual void update(float time) override;
	virtual void startWithTarget(Node *target) override;
protected:
	void initWithDuration(float duration,float radius,float alphaBegin,float alphaEnd);
	float radius;
	float alphaBegin;
	float alphaEnd;

	cocos2d::Point startPos;

};

class CCShake : public ActionInterval
{
	// Code by Francois Guibert
	// Contact: www.frozax.com - http://twitter.com/frozax - www.facebook.com/frozax
public:
	CCShake();

	// Create the action with a time and a strength (same in x and y)
	static CCShake* actionWithDuration(float d, float strength );
	// Create the action with a time and strengths (different in x and y)
	static CCShake* actionWithDuration(float d, float strength_x, float strength_y );
	bool initWithDuration(float d, float strength_x, float strength_y );

	virtual void startWithTarget(CCNode *pTarget);
	virtual void update(float time);
	virtual void stop(void);

protected:
	// Initial position of the shaked node
	float _initial_x, _initial_y;
	// Strength of the action
	float _strength_x, _strength_y;
};

class NativeBridge{
public:
	static string getDeviceID();
	static string getRefer();
	static string getVersionNative();
	static string getVersionCode();
	static string getStringResourceByName(string name);
	static void openURLNative(string url);
	static void showNoNetwork();

	static void showYesNoDialog(string title,string message,string ok,string cancel,int type);
	static void showOKDialog(string title,string message,string ok,int type);

};

class BaseGUI : public Layer
{
public:
	BaseGUI() : Layer()
	{

	}
	void initWithJSONFile(string file){
		auto layout = SceneReader::getInstance()->createNodeWithSceneFile(file.c_str());
		if(layout)
			this->addChild(layout);
	}
	virtual void cusomizeGUI(){};
	virtual void customizeButton(string button,int id,Node *parent = NULL){};

public:
	Layout *layout;
};

int decodeURIComponent (char *sSource, char *sDest);




