#pragma once
#include "cocos2d.h"
#include <vector>

USING_NS_CC;
using namespace std;
class BroadcastData
{
public:
	BroadcastData()
	{
		gift = 0;
		username = "";
		numG = 0;
	}
	~BroadcastData(){}
	double gift;
	std::string username;
	int numG;
};
class BroadcastLayer : public CCLayer
{
public:
	BroadcastLayer(void);
	~BroadcastLayer(void);

public:
	virtual bool init();
	virtual void visit();DrawNode

	void onBroadcastReceive(std::string message);
	void onBroadcastReceiveEvent(std::string message, int type);
	void onBroadcastReceiveSystem(std::string message);				// Nhan duoc tin nhan tu he thong
	void start();
	void stop();
	void resetBroadcast();

	CREATE_FUNC(BroadcastLayer);

public:
	vector<BroadcastData> _listBroadcast;
	BroadcastData _currentBroadcast;
	CCNode *_node;
	CCNode *_nodeSystem;
	CCLabelTTF *lb_01,*lb_User,*lb_Gift,*lb_02,*lb_numG,*lb_03;
	CCLabelTTF *lb_system;

private:
	void createMessage(BroadcastData dat);		// Tao chuoi~ thong bao moi'
	void callbackBroad();
};

