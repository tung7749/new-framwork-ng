#include "LoadingUI.h"
LoadingGUI *LoadingGUI::instanceLoading = NULL;
#include "AppDelegate.h"
#include "IOSConnection.h"
#include <string>
#include "spine/spine.h"
using namespace spine;

bool enableCheckUpdateJS = true;
void LoadingGUI::checkNoNetwork()
{
    bool networkstatus = true;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    cocos2d::JniMethodInfo t;
    if (cocos2d::JniHelper::getStaticMethodInfo(t,
                                                "gsn/zingplay/utils/NetworkUtility",
                                                "checkNetworkAvaiable",
                                                "()Z"))
    {
        jboolean result = (jboolean)t.env->CallStaticBooleanMethod(t.classID, t.methodID);
        //networkstatus = (bool)((jboolean)t.env->CallStaticObjectMethod(t.classID,t.methodID));
        networkstatus = (bool)result;
        t.env->DeleteLocalRef(t.classID);
    }
#elif(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    {
        networkstatus = IOSConnection::networkReachable();    }
#endif
    if (networkstatus)
    {
        checkNewAPK();
        //checkAppversion();
        getBaseUrl();
        
        // init downloader
        _am = AssetsManagerEx::create("project.manifest", FileUtils::getInstance()->getWritablePath() + "update/");
        _am->retain();
    }
    else
    {
        std::function<void(string jData, Ref* a)> func = [this](string jData, Ref* a){
            CCLOG("%s", jData.c_str());
            rapidjson::Document obj;
            obj.Parse<0>(jData.c_str());
            if (obj["type"].GetInt() == 1)                    // nonetwork
            {
                if (obj["button"].GetInt() == 0)              // ok
                {
                    this->checkNewAPK();
                    //this->checkAppversion();
                    CCLOG("checkNewAPK X%s", jData.c_str());
                    getBaseUrl();
                    
                    // init downloader
                    _am = AssetsManagerEx::create("project.manifest", FileUtils::getInstance()->getWritablePath() + "update/");
                    _am->retain();
                }
                else {
                    
                    if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) {
                        Director::getInstance()->end();
                        exit(0);
                    }
                    else {
                        Director::getInstance()->end();
                    }
                    
                }
            }
            else if (obj["type"].GetInt() == 2)               // maintain
            {
                if (obj["button"].GetInt() == 0) {
                    if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) {
                        Director::getInstance()->end();
                        exit(0);
                    }
                    else {
                        Director::getInstance()->end();
                    }
                }
            }
            else if (obj["type"].GetInt() == 3)               // update
            {
                if (obj["button"].GetInt() == 0) {
                    NativeBridge::openURLNative(this->update_link);
                }
                else {
                    this->checkUpdateJS();
                    
                }
            }
        };
        
        HandlerManager::getInstance()->addHandler("dialog", func);
        NativeBridge::showNoNetwork();
    }
}

void LoadingGUI::checkNewAPK(){
    string currentversioncode = NativeBridge::getVersionCode();
    string versioncode = UserDefault::sharedUserDefault()->getStringForKey("versioncode");
    
    if (versioncode == "")		// apk dau` tien
    {
        UserDefault::sharedUserDefault()->setStringForKey("versioncode", currentversioncode);
        if (FileUtils::getInstance()->removeDirectory(FileUtils::getInstance()->getWritablePath() + "update"))
        {
            CCLOG("delete folder update successfully........................");
        }
        else
        {
            CCLOG("delete folder update failed........................");
        }
    }
    else
    {
        if (currentversioncode != versioncode)	// new apk -> delete folder update
        {
            UserDefault::sharedUserDefault()->setStringForKey("versioncode", currentversioncode);
            if (FileUtils::getInstance()->removeDirectory(FileUtils::getInstance()->getWritablePath() + "update/"))
            {
                CCLOG("delete folder update successfully........................");
            }
            else
            {
                CCLOG("delete folder update failed........................");
            }
        }
    }
}

void LoadingGUI::checkAppversion(string api_portal		){
    UserDefault::sharedUserDefault()->setStringForKey("appversion", "");
    cRequest = new HttpRequest();
    cRequest->setTag("CHECK APP_VERSION");
    string urlAppVersion =NativeBridge::getStringResourceByName("app_version_url");
    cRequest->setUrl(urlAppVersion);
    
    UserDefault::sharedUserDefault()->setStringForKey("appversion", "");
    cRequest = new HttpRequest();
    cRequest->setTag("CHECK APP_VERSION");
    cRequest->setUrl(api_portal);
    
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
    cRequest->setUrl("http://api.vinplay.com:8081/api");
#endif
    
    cRequest->setRequestType(HttpRequest::Type::POST);
    cRequest->setResponseCallback(CC_CALLBACK_2(LoadingGUI::httpCallback, this));
    
    string data = NativeBridge::getStringResourceByName("app_version_data") + NativeBridge::getDeviceID();
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
    data = "	" + NativeBridge::();
#endif
    /*
     #if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
     data += NativeBridge::getStringResourceByName("app_version_game");
     #elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
     data += GAME_IOS;
     #elif CC_TARGET_PLATFORM == CC_PLATFORM_WP8
     data += GAME_WP8;
     #else
     data += NativeBridge::getStringResourceByName("app_version_game");
     #endif
     data += ("&device=" + NativeBridge::getDeviceID() + "&version=" + NativeBridge::getStringResourceByName("app_version"));
     data += ("&referer=" + NativeBridge::getRefer());
     */
    
    CCLOG(data.c_str());
    cRequest->setRequestData(data.c_str(), data.size());
    HttpClient::getInstance()->setTimeoutForRead(10);
    HttpClient::getInstance()->setTimeoutForRead(10);
    HttpClient::getInstance()->send(cRequest);
    cRequest->release();
    cRequest = NULL;
    
    HandlerManager::getInstance()->addHandler("check_appversion", [this](string, Ref*){
        ignoreUpdate = true;
        this->checkUpdateJS();
        //this->onLoadEnd(true);
    });
    HandlerManager::getInstance()->getHandler("check_appversion")->setTimeOut(15, true);
}

void LoadingGUI::getBaseUrl()
{
    //UserDefault::sharedUserDefault()->setStringForKey("appversion", "");
    cRequest = new HttpRequest();
    cRequest->setTag("GET_BASE_URL");
    cRequest->setUrl(NativeBridge::getStringResourceByName("app_base_url"));
    //cRequest->setUrl("http://vinplay.com/config_api.html");
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
    cRequest->setUrl("http://vinplay.com/config_api.html");
    //cRequest->setUrl("http://192.168.0.112:8081/api");
#endif
    
    cRequest->setRequestType(HttpRequest::Type::GET);
    cRequest->setResponseCallback(CC_CALLBACK_2(LoadingGUI::httpCallbackGetConfixURl, this));
    
    
    HttpClient::getInstance()->setTimeoutForRead(10);
    HttpClient::getInstance()->setTimeoutForConnect(10);
    HttpClient::getInstance()->send(cRequest);
    cRequest->release();
    cRequest = NULL;
    HandlerManager::getInstance()->addHandler("get_baseUrl", [this](string, Ref*){
        
        this->getBaseUrl2();
    });
    HandlerManager::getInstance()->getHandler("get_baseUrl")->setTimeOut(15, true);
    
}

void LoadingGUI::getBaseUrl2()
{
    this->isGetBaseUrl2 = true;
    //UserDefault::sharedUserDefault()->setStringForKey("appversion", "");
    cRequest = new HttpRequest();
    cRequest->setTag("GET_BASE_URL2");
    cRequest->setUrl(NativeBridge::getStringResourceByName("app_base_url2"));
    //cRequest->setUrl("http://zikplay.com/config_api.html");
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
    cRequest->setUrl("http://api-core.net/config_api_mobile.html");
#endif
    
    cRequest->setRequestType(HttpRequest::Type::GET);
    cRequest->setResponseCallback(CC_CALLBACK_2(LoadingGUI::httpCallbackGetConfixURl, this));
    
    
    
    HttpClient::getInstance()->setTimeoutForRead(10);
    HttpClient::getInstance()->setTimeoutForConnect(10);
    HttpClient::getInstance()->send(cRequest);
    cRequest->release();
    cRequest = NULL;
    
}

void LoadingGUI::httpCallbackGetConfixURl(HttpClient* client, HttpResponse* response)
{
    HandlerManager::getInstance()->forceRemoveHandler("get_baseUrl");
    if (!response || !response->isSucceed())
    {
        
        if(!this->isGetBaseUrl2)
            this->getBaseUrl2();
        else{
            UserDefault::sharedUserDefault()->setStringForKey("api_portal", api_portal);
            checkAppversion(api_portal);
        }
        
        return;
    }
    CCLOG("%s is completed", response->getHttpRequest()->getTag());
    std::vector<char> *buffer = response->getResponseData();
    std::string result(buffer->begin(), buffer->end());
    
    if (result.find_first_of("{") == 0)	// json
    {
        rapidjson::Document d;
        d.Parse<0>(result.c_str());
        
        if (d.HasMember("api_portal"))
        {
            api_portal = d["api_portal"].GetString();
            CCLOG("api_portal = %s ", api_portal.c_str());
            UserDefault::sharedUserDefault()->setStringForKey("api_portal", api_portal);
            checkAppversion(api_portal);
            return;
        }
    }else
    {
        CCLOG("Loi api_portal = ");
        if(!this->isGetBaseUrl2)
            this->getBaseUrl2();
        else{
            UserDefault::sharedUserDefault()->setStringForKey("api_portal", api_portal);
            checkAppversion(api_portal);
        }
    }
    UserDefault::sharedUserDefault()->setStringForKey("api_portal", api_portal);
    
    
}

void LoadingGUI::httpCallback(HttpClient* client, HttpResponse* response){
    if (ignoreUpdate)
        return;
    HandlerManager::getInstance()->forceRemoveHandler("check_appversion");
    if (!response || !response->isSucceed())
    {
        this->checkUpdateJS();
        //this->onLoadEnd(true);
        //return;
    }
    CCLOG("%s is completed", response->getHttpRequest()->getTag());
    
    std::vector<char> *buffer = response->getResponseData();
    std::string result(buffer->begin(), buffer->end());
    UserDefault::sharedUserDefault()->setStringForKey("appversion", result);
    CCLOG("%s", result.c_str());
    
    if (result.find_first_of("{") == 0)	// json
    {
        rapidjson::Document d;
        d.Parse<0>(result.c_str());
        
        std::function<void(string jData, Ref* a)> func = [this](string jData, Ref* a){
            CCLOG("%s", jData.c_str());
            rapidjson::Document obj;
            obj.Parse<0>(jData.c_str());
            if (obj["type"].GetInt() == 1)                    // nonetwork
            {
                if (obj["button"].GetInt() == 0)              // ok
                {
                    this->checkNewAPK();
                    this->checkAppversion(api_portal);
                }
                else {
                    
                    if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) {
                        Director::getInstance()->end();
                        exit(0);
                    }
                    else {
                        Director::getInstance()->end();
                    }
                    
                }
            }
            else if (obj["type"].GetInt() == 2)               // maintain
            {
                if (obj["button"].GetInt() == 0) {
                    if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) {
                        Director::getInstance()->end();
                        exit(0);
                    }
                    else {
                        Director::getInstance()->end();
                    }
                }
            }
            else if (obj["type"].GetInt() == 3)               // update
            {
                if (obj["button"].GetInt() == 0) {
                    NativeBridge::openURLNative(this->update_link);
                }
                else {
                    this->checkUpdateJS();
                    //this->onLoadEnd(true);
                    
                }
            }
        };
        
        
        if (d.HasMember("maintain"))
        {
            int maintain = d["maintain"].GetInt();
            if (maintain == 1)
            {
                char dst[] = "Th%C3%B4ng%20b%C3%A1o"; decodeURIComponent(dst, dst);
                char capnhat[] = "Tho%C3%A1t"; decodeURIComponent(capnhat, capnhat);
                HandlerManager::getInstance()->addHandler("dialog", func);
                NativeBridge::showOKDialog(dst, d["maintain_message"].GetString(), capnhat, 2);
                return;
            }
        }
        if (d.HasMember("urlUpdate"))
        {
            update_link = d["urlUpdate"].GetString();
        }
        int update = d["update"].GetInt();
        ///enableGemClub dung cho update game GemClub
        
//        if(d.HasMember("enableGemClub") && d["enableGemClub"].GetBool())
//        {
//            enableCheckUpdateJS = true;
//        }
//        else
//        {
//            enableCheckUpdateJS = false;
//        }
        
        switch (update)
        {
            case 0:					// NO UPDATE
            {
                this->checkUpdateJS();
                //this->onLoadEnd(true);
            }
                break;
            case 1:					// Co ban cap nhat ,co the khong can update
            {
                CCLOG("case 1111111");
                
                std::string mess = "";
                if (d.HasMember("update_message"))
                {
                    mess = d["update_message"].GetString();
                }
                
                char dst[] = "%20T%C3%A1%20l%E1%BA%A3%20c%C3%B3%20b%E1%BA%A3n%20c%E1%BA%ADp%20nh%E1%BA%ADt%20m%E1%BB%9Bi"; decodeURIComponent(dst, dst);
                char dst1[] = "C%E1%BA%ADp%20nh%E1%BA%ADt"; decodeURIComponent(dst1, dst1);
                char dst2[] = "B%E1%BB%8F%20qua"; decodeURIComponent(dst2, dst2);
                
                HandlerManager::getInstance()->addHandler("dialog", func);
                NativeBridge::showYesNoDialog(dst, mess, dst1, dst2, 3);
                
            }
                break;
            case 2:					// Co ban cap nhat -- Can` update
            {
                CCLOG("case 2222222");
                
                std::string mess = "";
                if (d.HasMember("update_message"))
                {
                    mess = d["update_message"].GetString();
                }
                char dst[] = "%20T%C3%A1%20l%E1%BA%A3%20c%C3%B3%20b%E1%BA%A3n%20c%E1%BA%ADp%20nh%E1%BA%ADt%20m%E1%BB%9Bi"; decodeURIComponent(dst, dst);
                char dst1[] = "C%E1%BA%ADp%20nh%E1%BA%ADt"; decodeURIComponent(dst1, dst1);
                char dst2[] = "B%E1%BB%8F%20qua"; decodeURIComponent(dst2, dst2);
                
                HandlerManager::getInstance()->addHandler("dialog", func);
                NativeBridge::showOKDialog(dst, mess, dst1, 3);
            }
                break;
            default:
            {                this->checkUpdateJS();
                //this->onLoadEnd(true);
            }
                break;
        }
        
    }
    else
    {
        this->checkUpdateJS();
        //this->onLoadEnd(true);
    }
    
    
}

void LoadingGUI::checkUpdateJS(){
    //enableCheckUpdateJS = true;
    if(!enableCheckUpdateJS)
    {
        onLoadEnd(true);
        return;
    }
    else
    {
//        load->setVisible(true);
//        bar->setVisible(true);
    }
    
    CCLOG("START UPDATE JS");
    if (!_am->getLocalManifest()->isLoaded())
    {
        CCLOG("Fail to update assets, step skipped...");
        
//        onLoadEnd(true);
        onLoadEnd(false);
    }
    else
    {
        CCLOG("Start update JS from static...");
        _amListener = cocos2d::extension::EventListenerAssetsManagerEx::create(_am, [this](EventAssetsManagerEx* event){
            static int failCount = 0;
            switch (event->getEventCode())
            {
                case EventAssetsManagerEx::EventCode::ERROR_NO_LOCAL_MANIFEST:
                {
                    CCLOG("No local manifest file found, skip assets update.");
                    this->onLoadEnd(false);
                }
                    break;
                case EventAssetsManagerEx::EventCode::UPDATE_PROGRESSION:
                {
                    std::string assetId = event->getAssetId();
                    float percent = event->getPercent();
                    std::string str;
                    if (assetId == AssetsManagerEx::VERSION_ID)
                    {
                        str = StringUtils::format("Version file: %.2f", percent) + "%";
                    }
                    else if (assetId == AssetsManagerEx::MANIFEST_ID)
                    {
                        str = StringUtils::format("Manifest file: %.2f", percent) + "%";
                    }
                    else
                    {
                        str = StringUtils::format("%.2f", percent) + "%";
                        //CCLOG("%.2f Percent", percent);
                    }
                    if (event->getAssetsManagerEx()->_totalToDownload > 0)
                    {
                        
                        this->updateDownload(event->getAssetsManagerEx()->_totalToDownload - event->getAssetsManagerEx()->_totalWaitToDownload, event->getAssetsManagerEx()->_totalToDownload);
                        char dst[] = "%C4%90ang%20t%E1%BA%A3i%20c%E1%BA%ADp%20nh%E1%BA%ADt"; decodeURIComponent(dst, dst);
                        string message = ""; message += dst;
                        message += CCString::createWithFormat(" (%d/%d)", (event->getAssetsManagerEx()->_totalToDownload - event->getAssetsManagerEx()->_totalWaitToDownload), event->getAssetsManagerEx()->_totalToDownload)->getCString();
                        
                        if (this->load != nullptr)
                            this->load->setString(message);
                    }
                    
                    
                }
                    break;
                case EventAssetsManagerEx::EventCode::ERROR_DOWNLOAD_MANIFEST:
                case EventAssetsManagerEx::EventCode::ERROR_PARSE_MANIFEST:
                {
                    CCLOG("Fail to download manifest file, update skipped.");
                    this->onLoadEnd(false);
                }
                    break;
                case EventAssetsManagerEx::EventCode::ALREADY_UP_TO_DATE:
                case EventAssetsManagerEx::EventCode::UPDATE_FINISHED:
                {
                    CCLOG("Update finished. %s", event->getMessage().c_str());
                    this->onLoadEnd();
                }
                    break;
                case EventAssetsManagerEx::EventCode::UPDATE_FAILED:
                {
                    failCount++;
                    CCLOG("Update failed. %s   %d", event->getAssetId().c_str(), failCount);
                    if (failCount < 5)
                    {
                        _am->downloadFailedAssets();
                    }
                    else
                    {
                        CCLOG("Reach maximum fail count, exit update process");
                        failCount = 0;
                        this->onLoadEnd(false);
                    }
                }
                    break;
                case EventAssetsManagerEx::EventCode::ERROR_UPDATING:
                {
                    CCLOG("Asset %s : %s", event->getAssetId().c_str(), event->getMessage().c_str());
                }
                    break;
                case EventAssetsManagerEx::EventCode::ERROR_DECOMPRESS:
                {
                    CCLOG("%s", event->getMessage().c_str());
                }
                    break;
                default:
                    break;
            }
        });
        Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_amListener, 1);
        
        _am->update();
        
        
    }
}

void LoadingGUI::onLoadEnd(bool success)
{
    if (_amListener != NULL)
    {
        Director::sharedDirector()->getEventDispatcher()->removeEventListener(_amListener);
        _amListener = NULL;
    }
    if (_am != NULL)
    {
        _am->release(); _am = NULL;
    }
    
    if (success)
    {
        char dst[] = "C%E1%BA%ADp%20nh%E1%BA%ADt%20th%C3%A0nh%20c%C3%B4ng"; decodeURIComponent(dst, dst);
        load->setString(dst);
        
        CCLOG("OnEnterSuccess");
        AppDelegate::runScript();
    }
    else
    {
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
        AppDelegate::runScript();
#else
        std::function<void(string jData, Ref* a)> func = [this](string jData, Ref* a){
            CCLOG("%s", jData.c_str());
            //HandlerManager::getInstance()->forceRemoveHandler("dialog");
            rapidjson::Document obj;
            obj.Parse<0>(jData.c_str());
            if (obj["type"].GetInt() == 4)                    // cap nhat loi~
            {
                if (obj["button"].GetInt() == 0)              // ok
                {
                    CCLOG("DIT1");
                    if (LoadingGUI::instanceLoading)
                    {
                        CCLOG("DIT2");
                        LoadingGUI::instanceLoading->retryUpdateJS();
                    }
                }
                else {
                    
                    AppDelegate::runScript();
                }
            }
        };
        
        
        char dst[] = "Th%C3%B4ng%20b%C3%A1o"; decodeURIComponent(dst, dst);
        char capnhatloi[] = "X%E1%BA%A3y%20ra%20l%E1%BB%97i%20khi%20c%E1%BA%ADp%20nh%E1%BA%ADt"; decodeURIComponent(capnhatloi, capnhatloi);
        char capnhatlai[] = "C%E1%BA%ADp%20nh%E1%BA%ADt%20l%E1%BA%A1i"; decodeURIComponent(capnhatlai, capnhatlai);
        char vanvaogame[] = "V%E1%BA%ABn%20v%C3%A0o%20game"; decodeURIComponent(vanvaogame, vanvaogame);
        
        HandlerManager::getInstance()->addHandler("dialog", func);
        NativeBridge::showYesNoDialog(dst, capnhatloi, capnhatlai, vanvaogame, 4);
#endif
    }
}


#define NUM_DOT  42

bool LoadingGUI::init()
{
    BaseGUI::init();
    auto size = Director::getInstance()->getWinSize();
//    Sprite *bg = Sprite::create("res/Base/Lobby/GUI/mobile-lobby-bg.jpg");
    Sprite *bg = Sprite::create("res/Base/Lobby/GUI/lobby-bg-2.jpg");
    
    addChild(bg);
    bg->setPosition(size.width / 2, size.height / 2);
    auto content = bg->getContentSize();
    bg->setScaleX(size.width / content.width);
    bg->setScaleY(size.height / content.height);
    
    TTFConfig ttf; ttf.fontFilePath = "res/LoadingUI/fonts/tahoma.ttf"; ttf.fontSize = 16;
    ssize_t buff = 0;
    //string str = (char *)FileUtils::getInstance()->getFileData("version", "r", &buff);
    auto version = Label::createWithTTF(ttf, "v1.0", TextHAlignment::CENTER);
    addChild(version);
    version->setPosition(25, 15);
    
    
    
    
    float _scale = size.width / 1280;
    _scale = (_scale > 1) ? 1 : _scale;
    
    auto icon = Sprite::create("res/LoadingUI/logo.png");
    icon->setScale(_scale);
    icon->setPosition(size.width / 2, size.height / 2 - icon->getContentSize().height*0.125f + 50);
    addChild(icon);
    
    auto icon1 = Sprite::create("res/LoadingUI/manvipclub.png");
    icon1->setScale(_scale);
    icon1->setPosition(size.width / 2, size.height / 2 - icon->getContentSize().height*0.125f - 100);
    addChild(icon1);
    
    //skeletonNode = SkeletonAnimation::createWithJsonFile("res/LoadingUI/GEM.json", "res/LoadingUI/GEM.atlas", 1.0f);
    //skeletonNode->setAnimation(0, "loading_loop", true);
    
    //skeletonNode->setPosition(size.width / 2, size.height / 2);
    //skeletonNode->setScale(0.6);
    //addChild(skeletonNode);
    
    //     auto logo = Sprite::create("res/LoadingUI/gametitle_tala.png");
    //     addChild(logo);
    //     logo->setScale(_scale);
    //     logo->setPosition(icon->getPositionX() - logo->getContentSize().width*_scale*0.035f, icon->getPositionY() + logo->getContentSize().height*_scale*0.275);
    //     logo->setLocalZOrder(99);
    
    // BAR LOADING
    auto bar = Sprite::create("res/LoadingUI/bar.png");
    bar->setScale(_scale);
    bar->setPosition(size.width / 2, 42.5);
    addChild(bar);
    
    char dst[] = "%C4%90ang%20ki%E1%BB%83m%20tra%20c%E1%BA%ADp%20nh%E1%BA%ADt"; decodeURIComponent(dst, dst);
    load = Label::createWithTTF(ttf, dst, TextHAlignment::CENTER);
    addChild(load);
    
    load->setTextColor(Color4B(203, 204, 206, 255));
    load->setScale(_scale);
    load->setPosition(Vec2(size.width / 2, bar->getPositionY() + bar->getContentSize().height / 1.8f));
    
    dots = new vector<Sprite*>();
    float sX = 17;
    float sY = bar->getContentSize().height / 2;
    for (int i = 0; i < NUM_DOT; i++)
    {
        auto dot = Sprite::create("res/LoadingUI/dot.png");
        bar->addChild(dot);
        dot->setVisible(false);
        dot->setPosition(sX + i*dot->getContentSize().width*1.15f, sY);
        dots->push_back(dot);
        
        if (i == 0)
        {
            curPos = dot->getPosition();
        }
    }
    
    light = Sprite::create("res/LoadingUI/light.png");
    bar->addChild(light);
    light->setVisible(false);
    
    instanceLoading = this;
    
    return true;
}

void LoadingGUI::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
    if (keyCode == EventKeyboard::KeyCode::KEY_BACKSPACE){
        Director::sharedDirector()->end();
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS	
        exit(0);
#endif
    }
    
}

void LoadingGUI::updateDownload(int cur, int total)
{
    
    float per = cur*1.0 / total;
    int dot = (int)(per*NUM_DOT) - 1;
    
    if (dot <= curDot) return;
    
    if (dot > curDot && dot < NUM_DOT)
    {
        for (int i = curDot; i < dot; i++)
        {
            Sprite* item = dots->at(i);
            
            item->setVisible(true);
            item->setScale(1);
            
            //item->runAction(Sequence::create(DelayTime::create(0.1f*i), ScaleTo::create(0.2f, 3.0f), ScaleTo::create(0.15f, 1.0f), NULL));
        }
        
        light->setVisible(cur < total);
        //light->cleanup();
        //light->setPosition(curPos);
        //light->runAction(MoveTo::create(0.2f, dots->at(dot)->getPosition()));
        //curPos = dots->at(dot)->getPosition();
        light->setPosition(dots->at(dot)->getPosition());
    }
    else
    {
        light->setVisible(false);
    }
    
    curDot = dot;
}

void LoadingGUI::retryUpdateJS()
{
    _am = AssetsManagerEx::create("project.manifest", FileUtils::getInstance()->getWritablePath() + "update/");
    _am->retain();
    HandlerManager::getInstance()->addHandler("retry", [this](string jData, Ref* a){
        LoadingGUI::instanceLoading->checkUpdateJS();
    });
    HandlerManager::getInstance()->getHandler("retry")->setTimeOut(.1, true);
}

bool IntroGUI::init()
{
    BaseGUI::init();
    char dst[] = "%C4%90ang%20t%E1%BA%A3i%20c%E1%BA%ADp%20nh%E1%BA%ADt"; decodeURIComponent(dst, dst);
    string message = ""; message += dst;
    CCLOG("%s dstsad", dst);
    auto size = Director::getInstance()->getWinSize();
    
    //skeletonNode = SkeletonAnimation::createWithJsonFile("res/LoadingUI/GEM.json", "res/LoadingUI/GEM.atlas", 1.0f);
    //skeletonNode->setAnimation(0, "loading_in", false);
    
    //skeletonNode->setPosition(size.width / 2, size.height / 2);
    //skeletonNode->setScale(0.6);
    //addChild(skeletonNode);
    
    Sprite *bg = Sprite::create("res/Base/Lobby/GUI/mobile-lobby-bg.jpg");
    addChild(bg);
    bg->setPosition(size.width / 2, size.height / 2);
    auto content = bg->getContentSize();
    bg->setScaleX(size.width / content.width);
    bg->setScaleY(size.height / content.height);
    
    
    runAction(Sequence::create(DelayTime::create(1.5), CallFunc::create(this, callfunc_selector(IntroGUI::loading)), NULL));
    
    return true;
    
    
    
}
