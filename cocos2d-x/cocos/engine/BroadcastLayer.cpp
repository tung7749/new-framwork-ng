#include "BroadcastLayer.h"
#include "Engine/GUI/LabelBM.h"
#include "Engine/Utility/CCLocalizedString.h"
#include "Game/Utils/StringUtils.h"
#include "Engine/GlobalVar.h"

BroadcastLayer::BroadcastLayer(void)
{
}	


BroadcastLayer::~BroadcastLayer(void)
{
}

bool BroadcastLayer::init()
{
	Director
	CCLayer::init();
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	float scaleY = winSize.height / DESIGN_RESOLUTION_HEIGHT;
	_node = CCNode::create();
	_nodeSystem = CCNode::create();
	addChild(_node);
	addChild(_nodeSystem);
	_node->setPosition(winSize.width - 80,winSize.height - 45 * scaleY);
	_nodeSystem->setPosition(winSize.width - 80,winSize.height - 45 * scaleY);


	float x = 0;
	lb_01 = CCLabelTTF::create(CCLocalizedString("EVENT_NOTE14").c_str(),"arial",18);lb_01->setAnchorPoint(ccp(0,.5f));x += lb_01->getContentSize().width;
	lb_User = CCLabelTTF::create("vishiha","arial",20);lb_User->setColor(ccORANGE);lb_User->setAnchorPoint(ccp(0,.5f));lb_User->setPositionX(x);x += lb_User->getContentSize().width;
	lb_02 = CCLabelTTF::create(CCLocalizedString("EVENT_NOTE15").c_str(),"arial",18);lb_02->setAnchorPoint(ccp(0,.5f));lb_02->setPositionX(x);x+= lb_02->getContentSize().width;
	lb_Gift = CCLabelTTF::create(CCLocalizedString("EVENT_NOTE11").c_str(),"arial",20);lb_Gift->setAnchorPoint(ccp(0,.5f));lb_Gift->setColor(ccYELLOW);lb_Gift->setPositionX(x);x+=lb_Gift->getContentSize().width;
	lb_03 = CCLabelTTF::create(CCLocalizedString("EVENT_NOTE16").c_str(),"arial",18);lb_03->setAnchorPoint(ccp(0,.5f));lb_03->setPositionX(x);x+= lb_03->getContentSize().width;
	lb_numG = CCLabelTTF::create(CCLocalizedString("10G").c_str(),"arial",20);lb_numG->setAnchorPoint(ccp(0,.5f));lb_numG->setColor(ccGREEN);lb_numG->setPositionX(x);x+=lb_numG->getContentSize().width;

	_node->addChild(lb_01);
	_node->addChild(lb_User);
	_node->addChild(lb_02);
	_node->addChild(lb_Gift);
	_node->addChild(lb_03);
	_node->addChild(lb_numG);

	lb_system = CCLabelTTF::create("","arial",18);lb_system->setAnchorPoint(ccp(0,.5f));
	_nodeSystem->addChild(lb_system);

	_node->setVisible(false);
	_nodeSystem->setVisible(false);

	return true;
}

void BroadcastLayer::createMessage(BroadcastData dat)
{

	float x = lb_01->getContentSize().width;
	CCLOG("CREATE MESSAGE %s ", dat.username.c_str());
	lb_User->setString(dat.username.c_str());x+=lb_User->getContentSize().width;
	lb_02->setPositionX(x);x+=lb_02->getContentSize().width;
	std::string _gift;

// 	if (dat.gift == 1)
// 	{
// 		_gift = CCLocalizedString("EVENT_NOTE10");
// 	}
// 	else if (dat.gift == 2)
// 	{
// 		_gift = CCLocalizedString("EVENT_NOTE11");
// 	}
// 	else if (dat.gift == 3)
// 	{
// 		_gift = CCLocalizedString("EVENT_NOTE12");
// 	}
// 	else
// 	{
// 		_gift = StringUtils::configNumber(dat.gift);
// 		_gift.append(" ");
// 		_gift.append(CCLocalizedString("EVENT_GOLD3"));
// 	}

	if(dat.gift > 100)
	{
		_gift = StringUtils::configNumber(dat.gift);
		_gift.append(" ");
		_gift.append(CCLocalizedString("EVENT_GOLD3"));
	}
	else
	{
		_gift = game->eventTetData.arrayHienVat[(int)dat.gift];
	}

	
	lb_Gift->setString(_gift.c_str());lb_Gift->setPositionX(x);x+=lb_Gift->getContentSize().width;
	lb_03->setPositionX(x);x+=lb_03->getContentSize().width;
	std::string _g = StringUtils::standartNumber(dat.numG);
	//_g.append("G");
	switch (dat.numG)
	{
	case 1:
		_g = CCLocalizedString("BAO_LI_XI");
		break;
	case 2:
		_g = CCLocalizedString("BANH_CHUNG");
		break;
	case 3:
		_g = CCLocalizedString("CAY_QUAT");
		break;
	case 4:
		_g = CCLocalizedString("CAY_MAI");
		break;
	default:
		break;
	}
	lb_numG->setString(_g.c_str());lb_numG->setPositionX(x);
}

void BroadcastLayer::visit()
{
	CCSize scissorSize = CCDirector::sharedDirector()->getWinSize();
	CCRect scissorRect = CCRect(scissorSize.width - 500, 0 , 420, scissorSize.height); //only the lower half of the screen is visible

	kmGLPushMatrix();
	glEnable(GL_SCISSOR_TEST);

	CCEGLView::sharedOpenGLView()->setScissorInPoints(scissorRect.origin.x, scissorRect.origin.y,
		scissorRect.size.width, scissorRect.size.height);

	CCLayer::visit();
	glDisable(GL_SCISSOR_TEST);
	kmGLPopMatrix();
}

void BroadcastLayer::onBroadcastReceive(std::string message)
{
	if (_nodeSystem->isVisible())
	{
		return;
	}
	rapidjson::Document config;
	config.Parse<0>(message.c_str());

	BroadcastData dat;
// 	dat.gift = config["gift"].GetDouble();
// 	dat.username = config["username"].GetString();
// 	dat.numG = config["numG"].GetInt();

	dat.gift = config["giftValue"].GetDouble();
	dat.username = config["user"].GetString();
	dat.numG = config["coin"].GetInt();

	if (_listBroadcast.size() >= 15)
	{
		_listBroadcast.erase(_listBroadcast.begin());
	}
	
	_listBroadcast.push_back(dat);

	if (_listBroadcast.size() == 1 && _currentBroadcast.numG == 0)
	{
		stopAllActions();
		start();
	}
	
}

void BroadcastLayer::onBroadcastReceiveSystem(std::string message)
{
	if (_nodeSystem->isVisible())
	{
		return;
	}
	stopAllActions();
	_node->stopAllActions();
	_node->setVisible(false);

	_nodeSystem->stopAllActions();
	_nodeSystem->setVisible(true);
	lb_system->setString(message.c_str());

	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	_nodeSystem->setPositionX(winSize.width - 80);
	_nodeSystem->runAction(CCSequence::create(CCMoveBy::create(30,ccp(-(lb_system->getContentSize().width + 430),0)),CCHide::create(),NULL));
}	



void BroadcastLayer::onBroadcastReceiveEvent(std::string message, int type)
{
	if (_nodeSystem->isVisible())
	{
		return;
	}
	rapidjson::Document config;
	config.Parse<0>(message.c_str());

	BroadcastData dat;
	// 	dat.gift = config["gift"].GetDouble();
	// 	dat.username = config["username"].GetString();
	// 	dat.numG = config["numG"].GetInt();

	if (config.IsArray())
	{
		int stt = 1;
		for (rapidjson::Document::ConstValueIterator iter = config.onBegin(); iter != config.onEnd(); iter++)
		{
			BroadcastData data;

			if(type == 1)
			{
				rapidjson::Document::ConstMemberIterator member = iter->MemberonBegin();	
				
				data.numG = member->value.GetInt();
				member++;

				data.username = member->value.GetString();
				member++;

				data.gift = member->value.GetInt();
				member++;
			}
			else
			{
				rapidjson::Document::ConstMemberIterator member = iter->MemberonBegin();	
				member++;
				data.numG = member->value.GetInt();
				member++;

				data.username = member->value.GetString();
				member++;

				data.gift = member->value.GetInt();
				member++;
			}
		

			if (_listBroadcast.size() >= 15)
			{
				_listBroadcast.erase(_listBroadcast.begin());
			}

			_listBroadcast.push_back(data);

		}
	}

// 	dat.gift = config["giftValue"].GetDouble();
// 	dat.username = config["user"].GetString();
// 	dat.numG = config["coin"].GetInt();
// 
// 	if (_listBroadcast.size() >= 15)

// 	{

// 		_listBroadcast.erase(_listBroadcast.begin());

// 	}

// 
// 	_listBroadcast.push_back(dat);

	if (_listBroadcast.size() == 1 && _currentBroadcast.numG == 0)
	{
		stopAllActions();
		start();
		CCLOG("CALLBACK ** 2");
	}

}

void BroadcastLayer::resetBroadcast()
{
	_listBroadcast.clear();
	_currentBroadcast.gift = 0;
	_currentBroadcast.numG = 0;
	_currentBroadcast.username = "";
	stopAllActions();
	_node->stopAllActions();
	_node->setVisible(false);

	CCLOG("CALLBACK ** 1");
}

void BroadcastLayer::start()
{
	if(_listBroadcast.empty() && _currentBroadcast.numG <= 0)
	{
		_node->setVisible(false);
	}
	this->runAction(CCRepeatForever::create(CCSequence::create(CCCallFunc::create(this,callfunc_selector(BroadcastLayer::callbackBroad)),CCDelayTime::create(300),NULL)));
}

void BroadcastLayer::stop()
{

}

void BroadcastLayer::callbackBroad()
{
	CCLOG("CALLBACK ** ");
	if (!_listBroadcast.empty())
	{
		_currentBroadcast = _listBroadcast.back();
		createMessage(_currentBroadcast);
		_listBroadcast.erase(_listBroadcast.begin() + _listBroadcast.size()-1);
		_node->stopAllActions();
		_node->setVisible(true);
		CCSize winSize = CCDirector::sharedDirector()->getWinSize();
		_node->setPositionX(winSize.width - 80);
		_node->runAction(CCMoveBy::create(30,ccp(-1400,0)));
	}
	else
	{
		if(_currentBroadcast.numG > 0)
		{
			createMessage(_currentBroadcast);
			_node->stopAllActions();
			_node->setVisible(true);
			CCSize winSize = CCDirector::sharedDirector()->getWinSize();
			_node->setPositionX(winSize.width - 80);
			_node->runAction(CCMoveBy::create(30,ccp(-1300,0)));

		}
	}
}