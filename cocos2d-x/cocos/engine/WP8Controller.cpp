#include "cocos2d.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
#include "pch.h"
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
#include "WP8Controller.h"
namespace PhoneDirect3DXamlAppComponent
{

static ICallbackPlatform^ globalCallback = nullptr;

void WP8Controller::loginFB()
{
	if (globalCallback != nullptr)
	{
		globalCallback->loginFB();
	}
}

void WP8Controller::setCallback(ICallbackPlatform^ _callback)
{
	globalCallback = _callback;
}

void WP8Controller::logoutFB()
{
	if (globalCallback != nullptr)
	{
		globalCallback->logoutFB();
	}
}
void WP8Controller::loginZalo()
{
	if (globalCallback != nullptr)
	{
		globalCallback->loginZalo();
	}
}

void WP8Controller::getZaloFriends()
{
	if (globalCallback != nullptr)
	{
		globalCallback->getZaloFriends();
	}
}
void WP8Controller::logoutZalo()
{
	if (globalCallback != nullptr)
	{
		globalCallback->logoutZalo();
	}
}
bool WP8Controller::checkNetworkAvaiable()
{
	if (globalCallback != nullptr)
	{
		return globalCallback->checkNetworkAvaiable();
	}
}


void WP8Controller::openURL(Platform::String^ URL)
{
	if (globalCallback != nullptr)
	{
		globalCallback->openURL(URL);
	}
}

void WP8Controller::openURLUpdate(Platform::String^ URL)
{
	if (globalCallback != nullptr)
	{
		globalCallback->openURLUpdate(URL);
	}
}

void WP8Controller::sendMessage(Platform::String^ phonenumber,Platform::String^ msg)
{
	if (globalCallback != nullptr)
	{
		globalCallback->sendMessage(phonenumber,msg);
	}
}

Platform::String^ WP8Controller::getIMEI()
{
	if (globalCallback != nullptr)
	{
		return globalCallback->getIMEI();
	}
}

Platform::String^ WP8Controller::getZaloAppID()
{
	if (globalCallback != nullptr)
	{
		return globalCallback->getZaloAppID();
	}
}

Platform::String^ WP8Controller::getZaloAppSecret()
{
	if (globalCallback != nullptr)
	{
		return globalCallback->getZaloAppSecret();
	}
}

Platform::String^ WP8Controller::getZaloOAucode()
{
	if (globalCallback != nullptr)
	{
		return globalCallback->getZaloOAucode();
	}
}
Platform::String^ WP8Controller::getZaloID()
{
	if (globalCallback != nullptr)
	{
		return globalCallback->getZaloID();
	}
}
}
#endif


