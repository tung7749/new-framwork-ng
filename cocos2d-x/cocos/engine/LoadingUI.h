#pragma once
#include "cocos2d.h"
#include "UIAvatar.h"
#include "Handler.h"
#include <string>
#include <spine/spine-cocos2dx.h>
USING_NS_CC;
class LoadingGUI : public BaseGUI
{
public:
	AssetsManagerEx* _am;
	EventListenerAssetsManagerEx* _amListener;
	Label *load;
	Label *percent;
	HttpRequest* cRequest;
	string update_link;
	bool ignoreUpdate;
    bool isGetBaseUrl2;
    string api_portal = "http://portal.api-core.net/api";

	vector<Sprite*> *dots;
	Sprite *light;
    Sprite *bar;
	int curDot;
	Vec2 curPos;

	bool isFinishLoaded;
	bool isWaitingDownload;
	LoadingGUI() : BaseGUI()
	{
		ignoreUpdate = false;
        isGetBaseUrl2 = false;
		_am = NULL;
		curDot = 0;

	}

	virtual void onExit()
	{
		BaseGUI::onExit();
		_am = NULL;
		instanceLoading = NULL;
	}
	virtual bool init();
	virtual void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event);

	void checkNoNetwork();
	void checkNewAPK();
	void updateDownload(int cur, int total);
	void checkAppversion(string url);
	void checkUpdateJS();
	void retryUpdateJS();
	void httpCallback(HttpClient* client, HttpResponse* response);
    void httpCallbackGetConfixURl(HttpClient* client, HttpResponse* response);
    void getBaseUrl();
    void getBaseUrl2();
	
	virtual void onEnterTransitionDidFinish(){
		BaseGUI::onEnterTransitionDidFinish();

		checkNoNetwork();
	}

	void onLoadEnd(bool success = true);

	static void onBackground()
	{
		if (instanceLoading && instanceLoading->_am)
		{
			instanceLoading->_am->saveFile();
		}
	}
	static LoadingGUI *instanceLoading;
	static LoadingGUI *create()
	{
		LoadingGUI *ret = new LoadingGUI();
		ret->init();

		return ret;
	}
private:
    spine::SkeletonAnimation* skeletonNode;
};

class IntroGUI : public BaseGUI
{
public:
	IntroGUI() : BaseGUI()
	{

	}
	virtual bool init();
	void loading(){
		auto scene = Scene::create();
		scene->addChild(LoadingGUI::create());
		Director::getInstance()->replaceScene(TransitionFade::create(.35, scene));
	}
	static IntroGUI *create()
	{
		IntroGUI *ret = new IntroGUI();
		ret->init();

		return ret;
	}
private:
    spine::SkeletonAnimation* skeletonNode;
};
