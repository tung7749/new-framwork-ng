#pragma once
#include <string>

using namespace std;
#if CC_TARGET_PLATFORM == CC_PLATFORM_WP8
namespace PhoneDirect3DXamlAppComponent
{
public interface class ICallbackPlatform
{
public:
	virtual void loginFB();
	virtual void logoutFB();

	virtual void loginZalo();
	virtual void logoutZalo();
	virtual void getZaloFriends();

	virtual void openURL(Platform::String^ URL);
	virtual void openURLUpdate(Platform::String^ URL);
	virtual void sendMessage(Platform::String^ phonenumber,Platform::String^ msg);
	virtual bool checkNetworkAvaiable();
	virtual Platform::String^ getIMEI();
	virtual Platform::String^ getZaloAppID();
	virtual Platform::String^ getZaloAppSecret();
	virtual Platform::String^ getZaloOAucode();
	virtual Platform::String^ getZaloID();

};



public ref class WP8Controller sealed
{
public:
	static void loginFB();
	static void logoutFB();

	static void loginZalo();
	static void logoutZalo();
	static void getZaloFriends();

	static void openURL(Platform::String^ URL);
	static void openURLUpdate(Platform::String^ URL);
	static void sendMessage(Platform::String^ phonenumber,Platform::String^ msg);
	static bool checkNetworkAvaiable();
	static Platform::String^ getIMEI();

	static Platform::String^ getZaloAppID();
	static Platform::String^ getZaloAppSecret();
	static Platform::String^ getZaloOAucode();
	static Platform::String^ getZaloID();

	static void setCallback(ICallbackPlatform^ _callback);

};

}

#endif