#include "Handler.h"
#include "WP8Controller.h"



Handler::Handler(string _id):
id(_id), userData(NULL), needStop(false), jData(""), callback(nullptr), _timeElapsed(0), _timeOut(0), needTimeOut(false), count(0), dem(0)
{
	
}

Handler::Handler() :
	id(""),userData(NULL),needStop(false),jData(""),callback(nullptr),_timeElapsed(0),_timeOut(0),needTimeOut(false)
{

}

void Handler::setID(string _id)
{
	id = _id;
}

Handler::~Handler(void)
{
	
}

void Handler::set(CallbackHandler _callback)
{
	callback = _callback;
	//userData = uData;
}

void Handler::setTimeOut(double t,bool needTOut)
{
	_timeOut = t;
	needTimeOut = needTOut;
}
void Handler::setCount(int count, bool needTOut)
{
	this->count = count;
	needTimeOut = needTOut;
}

void Handler::stop(std::string jdata){
	if(callback)
	{
		callback(jData,userData);
	}
}

static HandlerManager* instance = NULL;
HandlerManager::HandlerManager()
{
    
}

HandlerManager::~HandlerManager()
{

}

HandlerManager* HandlerManager::getInstance()
{
	if (instance == NULL)
	{
		instance = new HandlerManager();
		Director::getInstance()->getScheduler()->scheduleUpdate(instance,0,false);
	}
	return instance;
}

void HandlerManager::destroyInstance()
{
	Director::getInstance()->getScheduler()->unscheduleAllForTarget(instance);
	CC_SAFE_DELETE(instance);
}

void HandlerManager::update(float dt)
{
	map<string,Handler*>::iterator iter = handlers.begin();

	vector<string> needRemoves;

	for (;iter !=handlers.end();iter++)
	{
		if (iter->second->needTimeOut && !iter->second->needStop)
		{
			iter->second->_timeElapsed += dt;
			if (iter->second->_timeElapsed >= iter->second->_timeOut)
			{
				needRemoves.push_back(iter->first);
				iter->second->callback(iter->second->jData,iter->second->userData);
						
				continue;
			}
		}
		if (iter->second->needTimeOut && (iter->second->count > 0))
		{
			iter->second->dem++;
			if (iter->second->dem >= iter->second->count)
			{
				needRemoves.push_back(iter->first);
				iter->second->callback(iter->second->jData, iter->second->userData);
				continue;
			}
		}
		if (iter->second->needStop)
		{
			needRemoves.push_back(iter->first);
			if (iter->second->callback)
			{
				iter->second->callback(iter->second->jData,iter->second->userData);
			}			
		
		}
	}

	for (int i=0;i<needRemoves.size();i++)
	{
		forceRemoveHandler(needRemoves[i]);
	}
}

Handler* HandlerManager::getHandler(string id)
{
	if (handlers.find(id) != handlers.end())
	{
		return (handlers.find(id)->second);
	}
	else
	{
		return NULL;
	}
}

void HandlerManager::addHandler(string id,CallbackHandler callback)
{
	Handler *_hander = new Handler(id);
	_hander->set(callback);
	addHandler(_hander);
	
}

void HandlerManager::addHandler(Handler *_handler)
{
	if (handlers.find(_handler->id) != handlers.end())
	{
		CCLOG("this handler : %s has in handler manager , return ...",_handler->id.c_str());
	}
	else
	{
		handlers.insert(std::pair<string,Handler*>(_handler->id,_handler));
	}
}

void HandlerManager::stopHandler(string id,string jdata)
{
	
	if (handlers.find(id) != handlers.end())
	{
		handlers.at(id)->jData = jdata;
		handlers.at(id)->needStop = true;
	}
}

void HandlerManager::forceRemoveHandler(string id)
{
	if (handlers.find(id) != handlers.end())
	{
        //CCLOG("deleted handler : %s !!",id.c_str());
		delete handlers[id];
		handlers.erase(id);
	}
    else{
        CCLOG("handler: %s  donot exist !!",id.c_str());
    }
}

void HandlerManager::exitIOS()
{
    Director::getInstance()->end();
    ScriptEngineManager::destroyInstance();
#if (COCOS2D_DEBUG > 0 && CC_CODE_IDE_DEBUG_SUPPORT > 0 && RUNTIME_ENABLE)
    // NOTE:Please don't remove this call if you want to debug with Cocos Code IDE
    endRuntime();
#endif
    
    //ConfigParser::purge();
    exit(0);
    //if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    //Director *hi = NULL;
    //hi->end();
}

std::string StringUtil::json_2_string(rapidjson::Document &d)
{
    return "";
    
}


extern "C"{
#if(CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID)
	void Java_gsn_zingplay_utils_HandleHelper_stopHandler(JNIEnv* env,jobject thiz,jstring handlerID,jstring _jdata)
	{
		std::string handler = env->GetStringUTFChars(handlerID, 0);
		std::string data = env->GetStringUTFChars(_jdata, 0);

		HandlerManager::getInstance()->stopHandler(handler,data);

	}

#endif
}

#if CC_TARGET_PLATFORM == CC_PLATFORM_WP8


Platform::String^ char2String(const char* str)
{
	std::string s_str = str;
	std::wstring wid_str = std::wstring(s_str.begin(), s_str.end());
	const wchar_t* w_char = wid_str.c_str();
	return ref new Platform::String(w_char);
}


void WP8Bridgle::loginFB(){
	WP8Controller::loginFB();
}

void WP8Bridgle::logoutFB(){
	WP8Controller::logoutFB();
}

void WP8Bridgle::loginZALO(){
	WP8Controller::loginZalo();
}

void WP8Bridgle::logoutZALO(){
	WP8Controller::logoutZalo();
}

void WP8Bridgle::getZALOFriends(){
	WP8Controller::getZaloFriends();
}

void WP8Bridgle::openURL(std::string URL){
	WP8Controller::openURL(char2String(URL.c_str()));
}

void WP8Bridgle::openURLUpdate(std::string URL){
	WP8Controller::openURLUpdate(char2String(URL.c_str()));
}

void WP8Bridgle::sendMessage(std::string phone, std::string message)
{
	WP8Controller::sendMessage(char2String(phone.c_str()), char2String(message.c_str()));
}

bool WP8Bridgle::checkNetworkAvaiable(){
	return WP8Controller::checkNetworkAvaiable();
}

std::string WP8Bridgle::getIMEI(){

	Platform::String^ str = WP8Controller::getIMEI();
	std::string _tmp(str->Begin(), str->End());

	return _tmp;
}

void WP8Bridgle::sendLogLogin(std::string uID, std::string typeLogin, std::string openID, std::string zName){
	WP8Controller::sendLogLogin(char2String(uID.c_str()), char2String(typeLogin.c_str()), char2String(openID.c_str()), char2String(zName.c_str()));
}
void WP8Bridgle::submitLoginZalo(std::string uID, std::string typeLogin, std::string source, std::string refer){
	WP8Controller::submitLoginZalo(char2String(uID.c_str()), char2String(typeLogin.c_str()), char2String(source.c_str()), char2String(refer.c_str()));
}
void WP8Bridgle::payment(std::string user, std::string uid, std::string type, std::string amount){
	WP8Controller::payment(char2String(user.c_str()), char2String(uid.c_str()), char2String(type.c_str()), char2String(amount.c_str()));
}

void WP8Bridgle::showMaintain(std::string maintain)
{
	WP8Controller::showMaintain(char2String(maintain.c_str()));
}

void WP8Bridgle::showUpdate1(std::string maintain)
{
	WP8Controller::showUpdate1(char2String(maintain.c_str()));
}

void WP8Bridgle::showUpdate2(std::string maintain)
{
	WP8Controller::showUpdate2(char2String(maintain.c_str()));
}

void WP8Bridgle::showNoNetwork(){
	WP8Controller::showNoNetwork();
}

#else

void WP8Bridgle::loginFB(){
}

void WP8Bridgle::logoutFB(){
}

void WP8Bridgle::loginZALO(){
}

void WP8Bridgle::logoutZALO(){
}

void WP8Bridgle::getZALOFriends(){
}

void WP8Bridgle::openURL(std::string URL){
}

void WP8Bridgle::openURLUpdate(std::string URL){
}

void WP8Bridgle::sendMessage(std::string phone, std::string message)
{
}

bool WP8Bridgle::checkNetworkAvaiable(){
	return true;
}

std::string WP8Bridgle::getIMEI(){


	return "";
}

void WP8Bridgle::sendLogLogin(std::string uID, std::string typeLogin, std::string openID, std::string zName){
}
void WP8Bridgle::submitLoginZalo(std::string uID, std::string typeLogin, std::string source, std::string refer){
}
void WP8Bridgle::payment(std::string user, std::string uid, std::string type, std::string amount){
}

void WP8Bridgle::showMaintain(std::string maintain)
{
}

void WP8Bridgle::showUpdate1(std::string maintain)
{
}

void WP8Bridgle::showUpdate2(std::string maintain)
{
}

void WP8Bridgle::showNoNetwork(){
}


#endif

