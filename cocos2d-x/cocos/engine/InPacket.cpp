#include "InPacket.h"
#include "GsnClient.h"

using namespace std;
USING_NS_CC;


InPacket::InPacket()
{
	retain();
}

InPacket::~InPacket()
{
}
void InPacket::init(sPackage* pkg)
{
	_pos = 0;
	_data = pkg->_data;
	_length = pkg->_size;
	_controllerId = parseByte();
	_cmdId = getShort();
	_error = parseByte();
    
    //CCLOG("In Packet :  %d  error : %d",_length,_error);
}
int InPacket::getCmdId()
{
	return _cmdId;
}
int InPacket::getControllerId()
{
	return _controllerId;
}
int InPacket::getError()
{
	return _error;
}
char InPacket::parseByte()
{
	CCAssert(_pos < _length, "IndexOutOfBoundsException");
	char b = _data[_pos++];
	return b;
}
int InPacket::getByte() {
	return (int)parseByte();
}

bool InPacket::getBool() {
	CCAssert(_pos < _length, "IndexOutOfBoundsException");
	char b = _data[_pos++];
	return b > 0;
}

char* InPacket::getBytes(int size) {
	CCAssert(_pos + size <= _length, "IndexOutOfBoundsException");
	char *bytes = new char[size];
	for (int i = 0; i < size; i++) {
		bytes[i] = parseByte();
	}
	return bytes;
}

int InPacket::getShort() {
	CCAssert(_pos + 2 <= _length, "IndexOutOfBoundsException");
	if (_pos + 2 > _length) {
		return 0;
	}
	return (short)((parseByte() << 8) + (parseByte() & 255));
}

unsigned short InPacket::getUnsignedShort() {
	CCAssert(_pos + 2 <= _length, "IndexOutOfBoundsException");
	int a = (parseByte() & 255) << 8;
	int b = (parseByte() & 255) << 0;
	return a + b;
}

int InPacket::getInt() {
	CCAssert(_pos + 4 <= _length, "IndexOutOfBoundsException");
	return ((parseByte() & 255) << 24) +
		((parseByte() & 255) << 16) +
		((parseByte() & 255) << 8) +
		((parseByte() & 255) << 0);
}

//long long InPacket::getLong(){
//	CCAssert(_pos + 8 <= _length, "IndexOutOfBoundsException");
//	return ((long long)(parseByte() & 255) << 56) +
//		((long long)(parseByte() & 255) << 48) +
//		((long long)(parseByte() & 255) << 40) +
//		((long long)(parseByte() & 255) << 32) +
//		((long long)(parseByte() & 255) << 24) +
//		((long long)(parseByte() & 255) << 16) +
//		((long long)(parseByte() & 255) << 8) +
//		((long long)(parseByte() & 255) << 0);
//}

long long InPacket::getLong(){
    CCAssert(_pos + 8 <= _length, "IndexOutOfBoundsException");
    unsigned char data[8];
    for(int i = 0; i < 8; i++){
        data[i] = parseByte();
    }

    bool positive = true;
    long long value = 0;

    if(data[0] == (255 & 0xff)){
        positive = false;
    }
    if(positive){
        for (int i = 0; i < 8; i++) {
            value = (value * 256) + data[i];
        }
    }else{
        value = 1;
        for(int i= 1; i <=7; i++){
            value = value*256 - data[i];
        }
        value = -value;
    }
    return value;
}

double InPacket::getDouble()
{
	char *byteArray = new char[8];
	for ( int i = 7; i >= 0; i--)
	{
		byteArray[i] = parseByte();
	}
	double final;
	memcpy(&final, byteArray, 8);
	return final;
}
char* InPacket::getCharArray(int& size) {
	size = getUnsignedShort();
	return getBytes(size);
}
string InPacket::getString(){
	int size = 0;
	char* out = getCharArray(size);
	std::string str(out, size);
	delete[]out;
	return str;
}

void InPacket::clean()
{
    release();
}
