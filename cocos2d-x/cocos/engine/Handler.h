#pragma once
#include <string>
#include <vector>
#include <map>
#include "cocos2d.h"
#include <cmath>
#include "GsnClient.h"
#include "InPacket.h"
#include "OutPacket.h"
#include "sPackage.h"
#include "json/rapidjson.h"
#include "json/document.h"

USING_NS_CC;
using namespace std;
typedef std::function<void(string,Ref*)> CallbackHandler;		// callback when finish handler

class Handler : public Ref
{
public:
	Handler(string _id);
	Handler();
	~Handler(void);

	void set(CallbackHandler _callback);
	void setTimeOut(double t,bool needTOut);
	void setCount(int count, bool needTimeOut);
	void setID(string _id);
	void stop(std::string _jdata);
private:
	friend class HandlerManager;
	std::string id;
	std::string jData;
	bool needStop;
	Ref *userData;
	CallbackHandler callback;
	float _timeElapsed;
	float _timeOut;
	bool needTimeOut;
	int count;
	int dem;
};

class HandlerManager : public Ref
{
public:
	HandlerManager();
	~HandlerManager();

protected:
	map<string,Handler* > handlers;
public:
	virtual void update(float dt);

public:
	static HandlerManager *getInstance();
	static void destroyInstance();

	void addHandler(Handler *_handler);
	void addHandler(string id,CallbackHandler callback);

	Handler* getHandler(string id);

	void stopHandler(string id,string jdata);
	void forceRemoveHandler(string id);
    void exitIOS();
    
};

class StringUtil
{
public:
    static std::string json_2_string(rapidjson::Document &d);
};


class WP8Bridgle : public Ref
{
public:
	static void loginFB();
	static void logoutFB();

	static void loginZALO();
	static void logoutZALO();

	static void getZALOFriends();
	static void openURL(std::string URL);
	static void openURLUpdate(std::string URL);
	static void sendMessage(std::string phonenumber, std::string msg);
	static bool checkNetworkAvaiable();
	static std::string getIMEI();

	static void sendLogLogin(std::string uID, std::string typeLogin, std::string openID, std::string zName);
	static void submitLoginZalo(std::string uID, std::string typeLogin, std::string source, std::string refer);
	static void payment(std::string user, std::string uid, std::string type, std::string amount);

	static void showMaintain(std::string maintain);
	static void showUpdate1(std::string update);
	static void showUpdate2(std::string update);
	static void showNoNetwork();
};