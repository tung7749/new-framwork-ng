#include "scripting/js-bindings/auto/jsb_engine_auto.hpp"
#include "scripting/js-bindings/manual/cocos2d_specifics.hpp"
#include "engine/AsyncDownloader.h"
#include "engine/UIAvatar.h"
#include "engine/Handler.h"

template<class T>
static bool dummy_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS_ReportError(cx, "Constructor for the requested class is not available, please refer to the API reference.");
    return false;
}

static bool empty_constructor(JSContext *cx, uint32_t argc, jsval *vp) {
    return false;
}

static bool js_is_native_obj(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    args.rval().setBoolean(true);
    return true;
}
JSClass  *jsb_AsyncDownloader_class;
JSObject *jsb_AsyncDownloader_prototype;

bool js_cocos2dx_engine_AsyncDownloader_startDownload(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    AsyncDownloader* cobj = (AsyncDownloader *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_AsyncDownloader_startDownload : Invalid Native Object");
    if (argc == 0) {
        cobj->startDownload();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_AsyncDownloader_startDownload : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_AsyncDownloader_initDownload(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    AsyncDownloader* cobj = (AsyncDownloader *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_AsyncDownloader_initDownload : Invalid Native Object");
    if (argc == 3) {
        std::string arg0;
        std::string arg1;
        std::function<void (int, std::basic_string<char>)> arg2;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        do {
		    if(JS_TypeOfValue(cx, args.get(2)) == JSTYPE_FUNCTION)
		    {
		        JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
		        std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(2), args.thisv()));
		        auto lambda = [=](int larg0, std::basic_string<char> larg1) -> void {
		            JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
		            jsval largv[2];
		            largv[0] = int32_to_jsval(cx, larg0);
		            largv[1] = std_string_to_jsval(cx, larg1);
		            JS::RootedValue rval(cx);
		            bool succeed = func->invoke(2, &largv[0], &rval);
		            if (!succeed && JS_IsExceptionPending(cx)) {
		                JS_ReportPendingException(cx);
		            }
		        };
		        arg2 = lambda;
		    }
		    else
		    {
		        arg2 = nullptr;
		    }
		} while(0)
		;
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_AsyncDownloader_initDownload : Error processing arguments");
        cobj->initDownload(arg0, arg1, arg2);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_AsyncDownloader_initDownload : wrong number of arguments: %d, was expecting %d", argc, 3);
    return false;
}
bool js_cocos2dx_engine_AsyncDownloader_progressDownloaded(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    AsyncDownloader* cobj = (AsyncDownloader *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_AsyncDownloader_progressDownloaded : Invalid Native Object");
    if (argc == 0) {
        cobj->progressDownloaded();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_AsyncDownloader_progressDownloaded : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_AsyncDownloader_setCallback(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    AsyncDownloader* cobj = (AsyncDownloader *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_AsyncDownloader_setCallback : Invalid Native Object");
    if (argc == 1) {
        std::function<void (int, std::basic_string<char>)> arg0;
        do {
		    if(JS_TypeOfValue(cx, args.get(0)) == JSTYPE_FUNCTION)
		    {
		        JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
		        std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(0), args.thisv()));
		        auto lambda = [=](int larg0, std::basic_string<char> larg1) -> void {
		            JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
		            jsval largv[2];
		            largv[0] = int32_to_jsval(cx, larg0);
		            largv[1] = std_string_to_jsval(cx, larg1);
		            JS::RootedValue rval(cx);
		            bool succeed = func->invoke(2, &largv[0], &rval);
		            if (!succeed && JS_IsExceptionPending(cx)) {
		                JS_ReportPendingException(cx);
		            }
		        };
		        arg0 = lambda;
		    }
		    else
		    {
		        arg0 = nullptr;
		    }
		} while(0)
		;
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_AsyncDownloader_setCallback : Error processing arguments");
        cobj->setCallback(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_AsyncDownloader_setCallback : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_AsyncDownloader_httpCallback(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    AsyncDownloader* cobj = (AsyncDownloader *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_AsyncDownloader_httpCallback : Invalid Native Object");
    if (argc == 2) {
        cocos2d::network::HttpClient* arg0 = nullptr;
        cocos2d::network::HttpResponse* arg1 = nullptr;
        do {
            if (args.get(0).isNull()) { arg0 = nullptr; break; }
            if (!args.get(0).isObject()) { ok = false; break; }
            js_proxy_t *jsProxy;
            JS::RootedObject tmpObj(cx, args.get(0).toObjectOrNull());
            jsProxy = jsb_get_js_proxy(tmpObj);
            arg0 = (cocos2d::network::HttpClient*)(jsProxy ? jsProxy->ptr : NULL);
            JSB_PRECONDITION2( arg0, cx, false, "Invalid Native Object");
        } while (0);
        do {
            if (args.get(1).isNull()) { arg1 = nullptr; break; }
            if (!args.get(1).isObject()) { ok = false; break; }
            js_proxy_t *jsProxy;
            JS::RootedObject tmpObj(cx, args.get(1).toObjectOrNull());
            jsProxy = jsb_get_js_proxy(tmpObj);
            arg1 = (cocos2d::network::HttpResponse*)(jsProxy ? jsProxy->ptr : NULL);
            JSB_PRECONDITION2( arg1, cx, false, "Invalid Native Object");
        } while (0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_AsyncDownloader_httpCallback : Error processing arguments");
        cobj->httpCallback(arg0, arg1);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_AsyncDownloader_httpCallback : wrong number of arguments: %d, was expecting %d", argc, 2);
    return false;
}
bool js_cocos2dx_engine_AsyncDownloader_create(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 3) {
        std::string arg0;
        std::string arg1;
        std::function<void (int, std::basic_string<char>)> arg2;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        do {
		    if(JS_TypeOfValue(cx, args.get(2)) == JSTYPE_FUNCTION)
		    {
		        JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
		        std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(2), args.thisv()));
		        auto lambda = [=](int larg0, std::basic_string<char> larg1) -> void {
		            JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
		            jsval largv[2];
		            largv[0] = int32_to_jsval(cx, larg0);
		            largv[1] = std_string_to_jsval(cx, larg1);
		            JS::RootedValue rval(cx);
		            bool succeed = func->invoke(2, &largv[0], &rval);
		            if (!succeed && JS_IsExceptionPending(cx)) {
		                JS_ReportPendingException(cx);
		            }
		        };
		        arg2 = lambda;
		    }
		    else
		    {
		        arg2 = nullptr;
		    }
		} while(0)
		;
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_AsyncDownloader_create : Error processing arguments");

        auto ret = AsyncDownloader::create(arg0, arg1, arg2);
        js_type_class_t *typeClass = js_get_type_from_native<AsyncDownloader>(ret);
        JS::RootedObject jsret(cx, jsb_ref_autoreleased_create_jsobject(cx, ret, typeClass, "AsyncDownloader"));
        args.rval().set(OBJECT_TO_JSVAL(jsret));
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_AsyncDownloader_create : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_AsyncDownloader_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    AsyncDownloader* cobj = new (std::nothrow) AsyncDownloader();

    js_type_class_t *typeClass = js_get_type_from_native<AsyncDownloader>(cobj);

    // link the native object with the javascript object
    JS::RootedObject jsobj(cx, jsb_ref_create_jsobject(cx, cobj, typeClass, "AsyncDownloader"));
    args.rval().set(OBJECT_TO_JSVAL(jsobj));
    if (JS_HasProperty(cx, jsobj, "_ctor", &ok) && ok)
        ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsobj), "_ctor", args);
    return true;
}


void js_register_cocos2dx_engine_AsyncDownloader(JSContext *cx, JS::HandleObject global) {
    jsb_AsyncDownloader_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_AsyncDownloader_class->name = "AsyncDownloader";
    jsb_AsyncDownloader_class->addProperty = JS_PropertyStub;
    jsb_AsyncDownloader_class->delProperty = JS_DeletePropertyStub;
    jsb_AsyncDownloader_class->getProperty = JS_PropertyStub;
    jsb_AsyncDownloader_class->setProperty = JS_StrictPropertyStub;
    jsb_AsyncDownloader_class->enumerate = JS_EnumerateStub;
    jsb_AsyncDownloader_class->resolve = JS_ResolveStub;
    jsb_AsyncDownloader_class->convert = JS_ConvertStub;
    jsb_AsyncDownloader_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FN("startDownload", js_cocos2dx_engine_AsyncDownloader_startDownload, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("initDownload", js_cocos2dx_engine_AsyncDownloader_initDownload, 3, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("progressDownloaded", js_cocos2dx_engine_AsyncDownloader_progressDownloaded, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setCallback", js_cocos2dx_engine_AsyncDownloader_setCallback, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("httpCallback", js_cocos2dx_engine_AsyncDownloader_httpCallback, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    static JSFunctionSpec st_funcs[] = {
        JS_FN("create", js_cocos2dx_engine_AsyncDownloader_create, 3, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    jsb_AsyncDownloader_prototype = JS_InitClass(
        cx, global,
        JS::NullPtr(),
        jsb_AsyncDownloader_class,
        js_cocos2dx_engine_AsyncDownloader_constructor, 0, // constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, jsb_AsyncDownloader_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "AsyncDownloader"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::TrueHandleValue);
    // add the proto and JSClass to the type->js info hash table
    jsb_register_class<AsyncDownloader>(cx, jsb_AsyncDownloader_class, proto, JS::NullPtr());
}

JSClass  *jsb_HttpMultipart_class;
JSObject *jsb_HttpMultipart_prototype;

bool js_cocos2dx_engine_HttpMultipart_executeAsyncTask(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    HttpMultipart* cobj = (HttpMultipart *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_HttpMultipart_executeAsyncTask : Invalid Native Object");
    if (argc == 0) {
        cobj->executeAsyncTask();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_HttpMultipart_executeAsyncTask : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_HttpMultipart_addFilePart(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    HttpMultipart* cobj = (HttpMultipart *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_HttpMultipart_addFilePart : Invalid Native Object");
    if (argc == 4) {
        std::string arg0;
        std::string arg1;
        const char* arg2 = nullptr;
        unsigned long arg3 = 0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        std::string arg2_tmp; ok &= jsval_to_std_string(cx, args.get(2), &arg2_tmp); arg2 = arg2_tmp.c_str();
        ok &= jsval_to_ulong(cx, args.get(3), &arg3);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_HttpMultipart_addFilePart : Error processing arguments");
        cobj->addFilePart(arg0, arg1, arg2, arg3);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_HttpMultipart_addFilePart : wrong number of arguments: %d, was expecting %d", argc, 4);
    return false;
}
bool js_cocos2dx_engine_HttpMultipart_addImage(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    HttpMultipart* cobj = (HttpMultipart *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_HttpMultipart_addImage : Invalid Native Object");
    if (argc == 3) {
        std::string arg0;
        std::string arg1;
        std::string arg2;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        ok &= jsval_to_std_string(cx, args.get(2), &arg2);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_HttpMultipart_addImage : Error processing arguments");
        cobj->addImage(arg0, arg1, arg2);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_HttpMultipart_addImage : wrong number of arguments: %d, was expecting %d", argc, 3);
    return false;
}
bool js_cocos2dx_engine_HttpMultipart_setCallback(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    HttpMultipart* cobj = (HttpMultipart *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_HttpMultipart_setCallback : Invalid Native Object");
    if (argc == 1) {
        std::function<void (std::basic_string<char>)> arg0;
        do {
		    if(JS_TypeOfValue(cx, args.get(0)) == JSTYPE_FUNCTION)
		    {
		        JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
		        std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(0), args.thisv()));
		        auto lambda = [=](std::basic_string<char> larg0) -> void {
		            JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
		            jsval largv[1];
		            largv[0] = std_string_to_jsval(cx, larg0);
		            JS::RootedValue rval(cx);
		            bool succeed = func->invoke(1, &largv[0], &rval);
		            if (!succeed && JS_IsExceptionPending(cx)) {
		                JS_ReportPendingException(cx);
		            }
		        };
		        arg0 = lambda;
		    }
		    else
		    {
		        arg0 = nullptr;
		    }
		} while(0)
		;
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_HttpMultipart_setCallback : Error processing arguments");
        cobj->setCallback(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_HttpMultipart_setCallback : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_HttpMultipart_addFormPart(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    HttpMultipart* cobj = (HttpMultipart *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_HttpMultipart_addFormPart : Invalid Native Object");
    if (argc == 2) {
        std::string arg0;
        std::string arg1;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_HttpMultipart_addFormPart : Error processing arguments");
        cobj->addFormPart(arg0, arg1);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_HttpMultipart_addFormPart : wrong number of arguments: %d, was expecting %d", argc, 2);
    return false;
}
bool js_cocos2dx_engine_HttpMultipart_create(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 2) {
        std::string arg0;
        std::function<void (std::basic_string<char>)> arg1;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        do {
		    if(JS_TypeOfValue(cx, args.get(1)) == JSTYPE_FUNCTION)
		    {
		        JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
		        std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(1), args.thisv()));
		        auto lambda = [=](std::basic_string<char> larg0) -> void {
		            JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
		            jsval largv[1];
		            largv[0] = std_string_to_jsval(cx, larg0);
		            JS::RootedValue rval(cx);
		            bool succeed = func->invoke(1, &largv[0], &rval);
		            if (!succeed && JS_IsExceptionPending(cx)) {
		                JS_ReportPendingException(cx);
		            }
		        };
		        arg1 = lambda;
		    }
		    else
		    {
		        arg1 = nullptr;
		    }
		} while(0)
		;
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_HttpMultipart_create : Error processing arguments");

        auto ret = HttpMultipart::create(arg0, arg1);
        js_type_class_t *typeClass = js_get_type_from_native<HttpMultipart>(ret);
        JS::RootedObject jsret(cx, jsb_ref_autoreleased_create_jsobject(cx, ret, typeClass, "HttpMultipart"));
        args.rval().set(OBJECT_TO_JSVAL(jsret));
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_HttpMultipart_create : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_HttpMultipart_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    std::string arg0;
    std::function<void (std::basic_string<char>)> arg1;
    ok &= jsval_to_std_string(cx, args.get(0), &arg0);
    do {
		    if(JS_TypeOfValue(cx, args.get(1)) == JSTYPE_FUNCTION)
		    {
		        JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
		        std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(1), args.thisv()));
		        auto lambda = [=](std::basic_string<char> larg0) -> void {
		            JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
		            jsval largv[1];
		            largv[0] = std_string_to_jsval(cx, larg0);
		            JS::RootedValue rval(cx);
		            bool succeed = func->invoke(1, &largv[0], &rval);
		            if (!succeed && JS_IsExceptionPending(cx)) {
		                JS_ReportPendingException(cx);
		            }
		        };
		        arg1 = lambda;
		    }
		    else
		    {
		        arg1 = nullptr;
		    }
		} while(0)
		;
    JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_HttpMultipart_constructor : Error processing arguments");
    HttpMultipart* cobj = new (std::nothrow) HttpMultipart(arg0, arg1);

    js_type_class_t *typeClass = js_get_type_from_native<HttpMultipart>(cobj);

    // link the native object with the javascript object
    JS::RootedObject jsobj(cx, jsb_ref_create_jsobject(cx, cobj, typeClass, "HttpMultipart"));
    args.rval().set(OBJECT_TO_JSVAL(jsobj));
    if (JS_HasProperty(cx, jsobj, "_ctor", &ok) && ok)
        ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsobj), "_ctor", args);
    return true;
}


void js_register_cocos2dx_engine_HttpMultipart(JSContext *cx, JS::HandleObject global) {
    jsb_HttpMultipart_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_HttpMultipart_class->name = "HttpMultipart";
    jsb_HttpMultipart_class->addProperty = JS_PropertyStub;
    jsb_HttpMultipart_class->delProperty = JS_DeletePropertyStub;
    jsb_HttpMultipart_class->getProperty = JS_PropertyStub;
    jsb_HttpMultipart_class->setProperty = JS_StrictPropertyStub;
    jsb_HttpMultipart_class->enumerate = JS_EnumerateStub;
    jsb_HttpMultipart_class->resolve = JS_ResolveStub;
    jsb_HttpMultipart_class->convert = JS_ConvertStub;
    jsb_HttpMultipart_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FN("executeAsyncTask", js_cocos2dx_engine_HttpMultipart_executeAsyncTask, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("addFilePart", js_cocos2dx_engine_HttpMultipart_addFilePart, 4, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("addImage", js_cocos2dx_engine_HttpMultipart_addImage, 3, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setCallback", js_cocos2dx_engine_HttpMultipart_setCallback, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("addFormPart", js_cocos2dx_engine_HttpMultipart_addFormPart, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    static JSFunctionSpec st_funcs[] = {
        JS_FN("create", js_cocos2dx_engine_HttpMultipart_create, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    jsb_HttpMultipart_prototype = JS_InitClass(
        cx, global,
        JS::NullPtr(),
        jsb_HttpMultipart_class,
        js_cocos2dx_engine_HttpMultipart_constructor, 0, // constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, jsb_HttpMultipart_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "HttpMultipart"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::TrueHandleValue);
    // add the proto and JSClass to the type->js info hash table
    jsb_register_class<HttpMultipart>(cx, jsb_HttpMultipart_class, proto, JS::NullPtr());
}

JSClass  *jsb_sPackage_class;
JSObject *jsb_sPackage_prototype;

bool js_cocos2dx_engine_sPackage_clean(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    sPackage* cobj = (sPackage *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_sPackage_clean : Invalid Native Object");
    if (argc == 0) {
        cobj->clean();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_sPackage_clean : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_sPackage_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    sPackage* cobj = new (std::nothrow) sPackage();

    js_type_class_t *typeClass = js_get_type_from_native<sPackage>(cobj);

    // link the native object with the javascript object
    JS::RootedObject jsobj(cx, jsb_ref_create_jsobject(cx, cobj, typeClass, "sPackage"));
    args.rval().set(OBJECT_TO_JSVAL(jsobj));
    if (JS_HasProperty(cx, jsobj, "_ctor", &ok) && ok)
        ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsobj), "_ctor", args);
    return true;
}


void js_register_cocos2dx_engine_sPackage(JSContext *cx, JS::HandleObject global) {
    jsb_sPackage_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_sPackage_class->name = "sPackage";
    jsb_sPackage_class->addProperty = JS_PropertyStub;
    jsb_sPackage_class->delProperty = JS_DeletePropertyStub;
    jsb_sPackage_class->getProperty = JS_PropertyStub;
    jsb_sPackage_class->setProperty = JS_StrictPropertyStub;
    jsb_sPackage_class->enumerate = JS_EnumerateStub;
    jsb_sPackage_class->resolve = JS_ResolveStub;
    jsb_sPackage_class->convert = JS_ConvertStub;
    jsb_sPackage_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FN("clean", js_cocos2dx_engine_sPackage_clean, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    JSFunctionSpec *st_funcs = NULL;

    jsb_sPackage_prototype = JS_InitClass(
        cx, global,
        JS::NullPtr(),
        jsb_sPackage_class,
        js_cocos2dx_engine_sPackage_constructor, 0, // constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, jsb_sPackage_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "sPackage"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::TrueHandleValue);
    // add the proto and JSClass to the type->js info hash table
    jsb_register_class<sPackage>(cx, jsb_sPackage_class, proto, JS::NullPtr());
}

JSClass  *jsb_InPacket_class;
JSObject *jsb_InPacket_prototype;

bool js_cocos2dx_engine_InPacket_getDouble(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_getDouble : Invalid Native Object");
    if (argc == 0) {
        double ret = cobj->getDouble();
        jsval jsret = JSVAL_NULL;
        jsret = DOUBLE_TO_JSVAL(ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_getDouble : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_InPacket_getCmdId(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_getCmdId : Invalid Native Object");
    if (argc == 0) {
        int ret = cobj->getCmdId();
        jsval jsret = JSVAL_NULL;
        jsret = int32_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_getCmdId : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_InPacket_getByte(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_getByte : Invalid Native Object");
    if (argc == 0) {
        int ret = cobj->getByte();
        jsval jsret = JSVAL_NULL;
        jsret = int32_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_getByte : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_InPacket_getError(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_getError : Invalid Native Object");
    if (argc == 0) {
        int ret = cobj->getError();
        jsval jsret = JSVAL_NULL;
        jsret = int32_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_getError : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_InPacket_getLong(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_getLong : Invalid Native Object");
    if (argc == 0) {
        long long ret = cobj->getLong();
        jsval jsret = JSVAL_NULL;
        jsret = long_long_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_getLong : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_InPacket_getBool(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_getBool : Invalid Native Object");
    if (argc == 0) {
        bool ret = cobj->getBool();
        jsval jsret = JSVAL_NULL;
        jsret = BOOLEAN_TO_JSVAL(ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_getBool : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_InPacket_getString(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_getString : Invalid Native Object");
    if (argc == 0) {
        std::string ret = cobj->getString();
        jsval jsret = JSVAL_NULL;
        jsret = std_string_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_getString : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_InPacket_getInt(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_getInt : Invalid Native Object");
    if (argc == 0) {
        int ret = cobj->getInt();
        jsval jsret = JSVAL_NULL;
        jsret = int32_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_getInt : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_InPacket_init(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_init : Invalid Native Object");
    if (argc == 1) {
        sPackage* arg0 = nullptr;
        do {
            if (args.get(0).isNull()) { arg0 = nullptr; break; }
            if (!args.get(0).isObject()) { ok = false; break; }
            js_proxy_t *jsProxy;
            JS::RootedObject tmpObj(cx, args.get(0).toObjectOrNull());
            jsProxy = jsb_get_js_proxy(tmpObj);
            arg0 = (sPackage*)(jsProxy ? jsProxy->ptr : NULL);
            JSB_PRECONDITION2( arg0, cx, false, "Invalid Native Object");
        } while (0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_InPacket_init : Error processing arguments");
        cobj->init(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_init : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_InPacket_getControllerId(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_getControllerId : Invalid Native Object");
    if (argc == 0) {
        int ret = cobj->getControllerId();
        jsval jsret = JSVAL_NULL;
        jsret = int32_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_getControllerId : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_InPacket_clean(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_clean : Invalid Native Object");
    if (argc == 0) {
        cobj->clean();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_clean : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_InPacket_getUnsignedShort(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_getUnsignedShort : Invalid Native Object");
    if (argc == 0) {
        unsigned short ret = cobj->getUnsignedShort();
        jsval jsret = JSVAL_NULL;
        jsret = ushort_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_getUnsignedShort : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_InPacket_getShort(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_getShort : Invalid Native Object");
    if (argc == 0) {
        int ret = cobj->getShort();
        jsval jsret = JSVAL_NULL;
        jsret = int32_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_getShort : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_InPacket_getCharArray(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_getCharArray : Invalid Native Object");
    if (argc == 1) {
        int arg0 = 0;
        ok &= jsval_to_int32(cx, args.get(0), (int32_t *)&arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_InPacket_getCharArray : Error processing arguments");
        char* ret = cobj->getCharArray(arg0);
        jsval jsret = JSVAL_NULL;
        jsret = c_string_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_getCharArray : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_InPacket_getBytes(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    InPacket* cobj = (InPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_InPacket_getBytes : Invalid Native Object");
    if (argc == 1) {
        int arg0 = 0;
        ok &= jsval_to_int32(cx, args.get(0), (int32_t *)&arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_InPacket_getBytes : Error processing arguments");
        char* ret = cobj->getBytes(arg0);
        jsval jsret = JSVAL_NULL;
        jsret = c_string_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_InPacket_getBytes : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_InPacket_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    InPacket* cobj = new (std::nothrow) InPacket();

    js_type_class_t *typeClass = js_get_type_from_native<InPacket>(cobj);

    // link the native object with the javascript object
    JS::RootedObject jsobj(cx, jsb_ref_create_jsobject(cx, cobj, typeClass, "InPacket"));
    args.rval().set(OBJECT_TO_JSVAL(jsobj));
    if (JS_HasProperty(cx, jsobj, "_ctor", &ok) && ok)
        ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsobj), "_ctor", args);
    return true;
}
static bool js_cocos2dx_engine_InPacket_ctor(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    InPacket *nobj = new (std::nothrow) InPacket();
    auto newproxy = jsb_new_proxy(nobj, obj);
    jsb_ref_init(cx, &newproxy->obj, nobj, "InPacket");
    bool isFound = false;
    if (JS_HasProperty(cx, obj, "_ctor", &isFound) && isFound)
        ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(obj), "_ctor", args);
    args.rval().setUndefined();
    return true;
}


    
void js_register_cocos2dx_engine_InPacket(JSContext *cx, JS::HandleObject global) {
    jsb_InPacket_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_InPacket_class->name = "InPacket";
    jsb_InPacket_class->addProperty = JS_PropertyStub;
    jsb_InPacket_class->delProperty = JS_DeletePropertyStub;
    jsb_InPacket_class->getProperty = JS_PropertyStub;
    jsb_InPacket_class->setProperty = JS_StrictPropertyStub;
    jsb_InPacket_class->enumerate = JS_EnumerateStub;
    jsb_InPacket_class->resolve = JS_ResolveStub;
    jsb_InPacket_class->convert = JS_ConvertStub;
    jsb_InPacket_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FN("getDouble", js_cocos2dx_engine_InPacket_getDouble, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getCmdId", js_cocos2dx_engine_InPacket_getCmdId, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getByte", js_cocos2dx_engine_InPacket_getByte, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getError", js_cocos2dx_engine_InPacket_getError, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getLong", js_cocos2dx_engine_InPacket_getLong, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getBool", js_cocos2dx_engine_InPacket_getBool, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getString", js_cocos2dx_engine_InPacket_getString, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getInt", js_cocos2dx_engine_InPacket_getInt, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("init", js_cocos2dx_engine_InPacket_init, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getControllerId", js_cocos2dx_engine_InPacket_getControllerId, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("clean", js_cocos2dx_engine_InPacket_clean, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getUnsignedShort", js_cocos2dx_engine_InPacket_getUnsignedShort, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getShort", js_cocos2dx_engine_InPacket_getShort, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getCharArray", js_cocos2dx_engine_InPacket_getCharArray, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getBytes", js_cocos2dx_engine_InPacket_getBytes, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("ctor", js_cocos2dx_engine_InPacket_ctor, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    JSFunctionSpec *st_funcs = NULL;

    jsb_InPacket_prototype = JS_InitClass(
        cx, global,
        JS::NullPtr(),
        jsb_InPacket_class,
        js_cocos2dx_engine_InPacket_constructor, 0, // constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, jsb_InPacket_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "InPacket"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::TrueHandleValue);
    // add the proto and JSClass to the type->js info hash table
    jsb_register_class<InPacket>(cx, jsb_InPacket_class, proto, JS::NullPtr());
    anonEvaluate(cx, global, "(function () { engine.InPacket.extend = cc.Class.extend; })()");
}

JSClass  *jsb_OutPacket_class;
JSObject *jsb_OutPacket_prototype;

bool js_cocos2dx_engine_OutPacket_reset(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_reset : Invalid Native Object");
    if (argc == 0) {
        cobj->reset();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_reset : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_OutPacket_putInt(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_putInt : Invalid Native Object");
    if (argc == 1) {
        int arg0 = 0;
        ok &= jsval_to_int32(cx, args.get(0), (int32_t *)&arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_OutPacket_putInt : Error processing arguments");
        OutPacket* ret = cobj->putInt(arg0);
        jsval jsret = JSVAL_NULL;
        if (ret) {
            jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<OutPacket>(cx, (OutPacket*)ret));
        } else {
            jsret = JSVAL_NULL;
        };
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_putInt : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_OutPacket_setCmdId(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_setCmdId : Invalid Native Object");
    if (argc == 1) {
        int arg0 = 0;
        ok &= jsval_to_int32(cx, args.get(0), (int32_t *)&arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_OutPacket_setCmdId : Error processing arguments");
        cobj->setCmdId(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_setCmdId : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_OutPacket_packHeader(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_packHeader : Invalid Native Object");
    if (argc == 0) {
        cobj->packHeader();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_packHeader : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_OutPacket_setControllerId(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_setControllerId : Invalid Native Object");
    if (argc == 1) {
        int arg0 = 0;
        ok &= jsval_to_int32(cx, args.get(0), (int32_t *)&arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_OutPacket_setControllerId : Error processing arguments");
        cobj->setControllerId(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_setControllerId : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_OutPacket_putUnsignedShort(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_putUnsignedShort : Invalid Native Object");
    if (argc == 1) {
        unsigned short arg0 = 0;
        ok &= jsval_to_ushort(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_OutPacket_putUnsignedShort : Error processing arguments");
        OutPacket* ret = cobj->putUnsignedShort(arg0);
        jsval jsret = JSVAL_NULL;
        if (ret) {
            jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<OutPacket>(cx, (OutPacket*)ret));
        } else {
            jsret = JSVAL_NULL;
        };
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_putUnsignedShort : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_OutPacket_putShort(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_putShort : Invalid Native Object");
    if (argc == 1) {
        int arg0 = 0;
        ok &= jsval_to_int32(cx, args.get(0), (int32_t *)&arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_OutPacket_putShort : Error processing arguments");
        OutPacket* ret = cobj->putShort(arg0);
        jsval jsret = JSVAL_NULL;
        if (ret) {
            jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<OutPacket>(cx, (OutPacket*)ret));
        } else {
            jsret = JSVAL_NULL;
        };
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_putShort : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_OutPacket_putString(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_putString : Invalid Native Object");
    if (argc == 1) {
        std::string arg0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_OutPacket_putString : Error processing arguments");
        OutPacket* ret = cobj->putString(arg0);
        jsval jsret = JSVAL_NULL;
        if (ret) {
            jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<OutPacket>(cx, (OutPacket*)ret));
        } else {
            jsret = JSVAL_NULL;
        };
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_putString : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_OutPacket_initData(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_initData : Invalid Native Object");
    if (argc == 1) {
        int arg0 = 0;
        ok &= jsval_to_int32(cx, args.get(0), (int32_t *)&arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_OutPacket_initData : Error processing arguments");
        cobj->initData(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_initData : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_OutPacket_putBytes(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_putBytes : Invalid Native Object");
    if (argc == 2) {
        const char* arg0 = nullptr;
        int arg1 = 0;
        std::string arg0_tmp; ok &= jsval_to_std_string(cx, args.get(0), &arg0_tmp); arg0 = arg0_tmp.c_str();
        ok &= jsval_to_int32(cx, args.get(1), (int32_t *)&arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_OutPacket_putBytes : Error processing arguments");
        OutPacket* ret = cobj->putBytes(arg0, arg1);
        jsval jsret = JSVAL_NULL;
        if (ret) {
            jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<OutPacket>(cx, (OutPacket*)ret));
        } else {
            jsret = JSVAL_NULL;
        };
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_putBytes : wrong number of arguments: %d, was expecting %d", argc, 2);
    return false;
}
bool js_cocos2dx_engine_OutPacket_putLong(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_putLong : Invalid Native Object");
    if (argc == 1) {
        double arg0 = 0;
        ok &= JS::ToNumber( cx, args.get(0), &arg0) && !std::isnan(arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_OutPacket_putLong : Error processing arguments");
        OutPacket* ret = cobj->putLong(arg0);
        jsval jsret = JSVAL_NULL;
        if (ret) {
            jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<OutPacket>(cx, (OutPacket*)ret));
        } else {
            jsret = JSVAL_NULL;
        };
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_putLong : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_OutPacket_clean(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_clean : Invalid Native Object");
    if (argc == 0) {
        cobj->clean();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_clean : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_OutPacket_updateSize(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_updateSize : Invalid Native Object");
    if (argc == 0) {
        cobj->updateSize();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_updateSize : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_OutPacket_getData(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_getData : Invalid Native Object");
    if (argc == 1) {
        int arg0 = 0;
        ok &= jsval_to_int32(cx, args.get(0), (int32_t *)&arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_OutPacket_getData : Error processing arguments");
        char* ret = cobj->getData(arg0);
        jsval jsret = JSVAL_NULL;
        jsret = c_string_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_getData : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_OutPacket_putByteArray(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_putByteArray : Invalid Native Object");
    if (argc == 2) {
        const char* arg0 = nullptr;
        int arg1 = 0;
        std::string arg0_tmp; ok &= jsval_to_std_string(cx, args.get(0), &arg0_tmp); arg0 = arg0_tmp.c_str();
        ok &= jsval_to_int32(cx, args.get(1), (int32_t *)&arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_OutPacket_putByteArray : Error processing arguments");
        OutPacket* ret = cobj->putByteArray(arg0, arg1);
        jsval jsret = JSVAL_NULL;
        if (ret) {
            jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<OutPacket>(cx, (OutPacket*)ret));
        } else {
            jsret = JSVAL_NULL;
        };
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_putByteArray : wrong number of arguments: %d, was expecting %d", argc, 2);
    return false;
}
bool js_cocos2dx_engine_OutPacket_putByte(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_putByte : Invalid Native Object");
    if (argc == 1) {
        int arg0 = 0;
        ok &= jsval_to_int32(cx, args.get(0), (int32_t *)&arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_OutPacket_putByte : Error processing arguments");
        OutPacket* ret = cobj->putByte(arg0);
        jsval jsret = JSVAL_NULL;
        if (ret) {
            jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<OutPacket>(cx, (OutPacket*)ret));
        } else {
            jsret = JSVAL_NULL;
        };
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_putByte : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_OutPacket_updateUnsignedShortAtPos(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    OutPacket* cobj = (OutPacket *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_OutPacket_updateUnsignedShortAtPos : Invalid Native Object");
    if (argc == 2) {
        unsigned short arg0 = 0;
        int arg1 = 0;
        ok &= jsval_to_ushort(cx, args.get(0), &arg0);
        ok &= jsval_to_int32(cx, args.get(1), (int32_t *)&arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_OutPacket_updateUnsignedShortAtPos : Error processing arguments");
        cobj->updateUnsignedShortAtPos(arg0, arg1);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_OutPacket_updateUnsignedShortAtPos : wrong number of arguments: %d, was expecting %d", argc, 2);
    return false;
}
bool js_cocos2dx_engine_OutPacket_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    OutPacket* cobj = new (std::nothrow) OutPacket();

    js_type_class_t *typeClass = js_get_type_from_native<OutPacket>(cobj);

    // link the native object with the javascript object
    JS::RootedObject jsobj(cx, jsb_ref_create_jsobject(cx, cobj, typeClass, "OutPacket"));
    args.rval().set(OBJECT_TO_JSVAL(jsobj));
    if (JS_HasProperty(cx, jsobj, "_ctor", &ok) && ok)
        ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsobj), "_ctor", args);
    return true;
}
static bool js_cocos2dx_engine_OutPacket_ctor(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    OutPacket *nobj = new (std::nothrow) OutPacket();
    auto newproxy = jsb_new_proxy(nobj, obj);
    jsb_ref_init(cx, &newproxy->obj, nobj, "OutPacket");
    bool isFound = false;
    if (JS_HasProperty(cx, obj, "_ctor", &isFound) && isFound)
        ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(obj), "_ctor", args);
    args.rval().setUndefined();
    return true;
}


    
void js_register_cocos2dx_engine_OutPacket(JSContext *cx, JS::HandleObject global) {
    jsb_OutPacket_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_OutPacket_class->name = "OutPacket";
    jsb_OutPacket_class->addProperty = JS_PropertyStub;
    jsb_OutPacket_class->delProperty = JS_DeletePropertyStub;
    jsb_OutPacket_class->getProperty = JS_PropertyStub;
    jsb_OutPacket_class->setProperty = JS_StrictPropertyStub;
    jsb_OutPacket_class->enumerate = JS_EnumerateStub;
    jsb_OutPacket_class->resolve = JS_ResolveStub;
    jsb_OutPacket_class->convert = JS_ConvertStub;
    jsb_OutPacket_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FN("reset", js_cocos2dx_engine_OutPacket_reset, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("putInt", js_cocos2dx_engine_OutPacket_putInt, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setCmdId", js_cocos2dx_engine_OutPacket_setCmdId, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("packHeader", js_cocos2dx_engine_OutPacket_packHeader, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setControllerId", js_cocos2dx_engine_OutPacket_setControllerId, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("putUnsignedShort", js_cocos2dx_engine_OutPacket_putUnsignedShort, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("putShort", js_cocos2dx_engine_OutPacket_putShort, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("putString", js_cocos2dx_engine_OutPacket_putString, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("initData", js_cocos2dx_engine_OutPacket_initData, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("putBytes", js_cocos2dx_engine_OutPacket_putBytes, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("putLong", js_cocos2dx_engine_OutPacket_putLong, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("clean", js_cocos2dx_engine_OutPacket_clean, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("updateSize", js_cocos2dx_engine_OutPacket_updateSize, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getData", js_cocos2dx_engine_OutPacket_getData, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("putByteArray", js_cocos2dx_engine_OutPacket_putByteArray, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("putByte", js_cocos2dx_engine_OutPacket_putByte, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("updateUnsignedShortAtPos", js_cocos2dx_engine_OutPacket_updateUnsignedShortAtPos, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("ctor", js_cocos2dx_engine_OutPacket_ctor, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    JSFunctionSpec *st_funcs = NULL;

    jsb_OutPacket_prototype = JS_InitClass(
        cx, global,
        JS::NullPtr(),
        jsb_OutPacket_class,
        js_cocos2dx_engine_OutPacket_constructor, 0, // constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, jsb_OutPacket_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "OutPacket"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::TrueHandleValue);
    // add the proto and JSClass to the type->js info hash table
    jsb_register_class<OutPacket>(cx, jsb_OutPacket_class, proto, JS::NullPtr());
    anonEvaluate(cx, global, "(function () { engine.OutPacket.extend = cc.Class.extend; })()");
}

JSClass  *jsb_GsnClient_class;
JSObject *jsb_GsnClient_prototype;

bool js_cocos2dx_engine_GsnClient_onSubThreadStarted(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_onSubThreadStarted : Invalid Native Object");
    if (argc == 0) {
        cobj->onSubThreadStarted();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_onSubThreadStarted : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_GsnClient_onSubThreadEnded(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_onSubThreadEnded : Invalid Native Object");
    if (argc == 0) {
        cobj->onSubThreadEnded();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_onSubThreadEnded : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_GsnClient_setTimeoutForRead(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_setTimeoutForRead : Invalid Native Object");
    if (argc == 1) {
        int arg0 = 0;
        ok &= jsval_to_int32(cx, args.get(0), (int32_t *)&arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_GsnClient_setTimeoutForRead : Error processing arguments");
        cobj->setTimeoutForRead(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_setTimeoutForRead : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_GsnClient_disconnect(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_disconnect : Invalid Native Object");
    if (argc == 0) {
        cobj->disconnect();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_disconnect : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_GsnClient_setReceiveDataListener(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_setReceiveDataListener : Invalid Native Object");
    if (argc == 1) {
        std::function<void (int, sPackage *)> arg0;
        do {
		    if(JS_TypeOfValue(cx, args.get(0)) == JSTYPE_FUNCTION)
		    {
		        JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
		        std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(0), args.thisv()));
		        auto lambda = [=](int larg0, sPackage* larg1) -> void {
		            JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
		            jsval largv[2];
		            largv[0] = int32_to_jsval(cx, larg0);
		            if (larg1) {
		            largv[1] = OBJECT_TO_JSVAL(js_get_or_create_jsobject<sPackage>(cx, (sPackage*)larg1));
		        } else {
		            largv[1] = JSVAL_NULL;
		        };
		            JS::RootedValue rval(cx);
		            bool succeed = func->invoke(2, &largv[0], &rval);
		            if (!succeed && JS_IsExceptionPending(cx)) {
		                JS_ReportPendingException(cx);
		            }
		        };
		        arg0 = lambda;
		    }
		    else
		    {
		        arg0 = nullptr;
		    }
		} while(0)
		;
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_GsnClient_setReceiveDataListener : Error processing arguments");
        cobj->setReceiveDataListener(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_setReceiveDataListener : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_GsnClient_isDoConnection(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_isDoConnection : Invalid Native Object");
    if (argc == 0) {
        bool ret = cobj->isDoConnection();
        jsval jsret = JSVAL_NULL;
        jsret = BOOLEAN_TO_JSVAL(ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_isDoConnection : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_GsnClient_initThread(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_initThread : Invalid Native Object");
    if (argc == 0) {
        bool ret = cobj->initThread();
        jsval jsret = JSVAL_NULL;
        jsret = BOOLEAN_TO_JSVAL(ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_initThread : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_GsnClient_setTimeoutForConnect(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_setTimeoutForConnect : Invalid Native Object");
    if (argc == 1) {
        int arg0 = 0;
        ok &= jsval_to_int32(cx, args.get(0), (int32_t *)&arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_GsnClient_setTimeoutForConnect : Error processing arguments");
        cobj->setTimeoutForConnect(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_setTimeoutForConnect : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_GsnClient_createSocket(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_createSocket : Invalid Native Object");
    if (argc == 0) {
        cobj->createSocket();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_createSocket : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_GsnClient_onSubThreadLoop(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_onSubThreadLoop : Invalid Native Object");
    if (argc == 0) {
        bool ret = cobj->onSubThreadLoop();
        jsval jsret = JSVAL_NULL;
        jsret = BOOLEAN_TO_JSVAL(ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_onSubThreadLoop : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_GsnClient_getTimeoutForRead(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_getTimeoutForRead : Invalid Native Object");
    if (argc == 0) {
        int ret = cobj->getTimeoutForRead();
        jsval jsret = JSVAL_NULL;
        jsret = int32_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_getTimeoutForRead : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_GsnClient_setFinishConnectListener(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_setFinishConnectListener : Invalid Native Object");
    if (argc == 1) {
        std::function<void (bool)> arg0;
        do {
		    if(JS_TypeOfValue(cx, args.get(0)) == JSTYPE_FUNCTION)
		    {
		        JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
		        std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(0), args.thisv()));
		        auto lambda = [=](bool larg0) -> void {
		            JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
		            jsval largv[1];
		            largv[0] = BOOLEAN_TO_JSVAL(larg0);
		            JS::RootedValue rval(cx);
		            bool succeed = func->invoke(1, &largv[0], &rval);
		            if (!succeed && JS_IsExceptionPending(cx)) {
		                JS_ReportPendingException(cx);
		            }
		        };
		        arg0 = lambda;
		    }
		    else
		    {
		        arg0 = nullptr;
		    }
		} while(0)
		;
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_GsnClient_setFinishConnectListener : Error processing arguments");
        cobj->setFinishConnectListener(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_setFinishConnectListener : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_GsnClient_onUIThreadReceiveMessage(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_onUIThreadReceiveMessage : Invalid Native Object");
    if (argc == 1) {
        sPackage* arg0 = nullptr;
        do {
            if (args.get(0).isNull()) { arg0 = nullptr; break; }
            if (!args.get(0).isObject()) { ok = false; break; }
            js_proxy_t *jsProxy;
            JS::RootedObject tmpObj(cx, args.get(0).toObjectOrNull());
            jsProxy = jsb_get_js_proxy(tmpObj);
            arg0 = (sPackage*)(jsProxy ? jsProxy->ptr : NULL);
            JSB_PRECONDITION2( arg0, cx, false, "Invalid Native Object");
        } while (0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_GsnClient_onUIThreadReceiveMessage : Error processing arguments");
        cobj->onUIThreadReceiveMessage(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_onUIThreadReceiveMessage : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_GsnClient_setDisconnectListener(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_setDisconnectListener : Invalid Native Object");
    if (argc == 1) {
        std::function<void ()> arg0;
        do {
		    if(JS_TypeOfValue(cx, args.get(0)) == JSTYPE_FUNCTION)
		    {
		        JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
		        std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(0), args.thisv()));
		        auto lambda = [=]() -> void {
		            JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
		            JS::RootedValue rval(cx);
		            bool succeed = func->invoke(0, nullptr, &rval);
		            if (!succeed && JS_IsExceptionPending(cx)) {
		                JS_ReportPendingException(cx);
		            }
		        };
		        arg0 = lambda;
		    }
		    else
		    {
		        arg0 = nullptr;
		    }
		} while(0)
		;
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_GsnClient_setDisconnectListener : Error processing arguments");
        cobj->setDisconnectListener(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_setDisconnectListener : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_GsnClient_setListener(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_setListener : Invalid Native Object");
    if (argc == 1) {
        ClientListener* arg0 = nullptr;
        do {
            if (args.get(0).isNull()) { arg0 = nullptr; break; }
            if (!args.get(0).isObject()) { ok = false; break; }
            js_proxy_t *jsProxy;
            JS::RootedObject tmpObj(cx, args.get(0).toObjectOrNull());
            jsProxy = jsb_get_js_proxy(tmpObj);
            arg0 = (ClientListener*)(jsProxy ? jsProxy->ptr : NULL);
            JSB_PRECONDITION2( arg0, cx, false, "Invalid Native Object");
        } while (0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_GsnClient_setListener : Error processing arguments");
        cobj->setListener(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_setListener : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_GsnClient_getTimeoutForConnect(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_getTimeoutForConnect : Invalid Native Object");
    if (argc == 0) {
        int ret = cobj->getTimeoutForConnect();
        jsval jsret = JSVAL_NULL;
        jsret = int32_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_getTimeoutForConnect : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_GsnClient_connect(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_connect : Invalid Native Object");
    if (argc == 2) {
        const char* arg0 = nullptr;
        int arg1 = 0;
        std::string arg0_tmp; ok &= jsval_to_std_string(cx, args.get(0), &arg0_tmp); arg0 = arg0_tmp.c_str();
        ok &= jsval_to_int32(cx, args.get(1), (int32_t *)&arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_GsnClient_connect : Error processing arguments");
        cobj->connect(arg0, arg1);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_connect : wrong number of arguments: %d, was expecting %d", argc, 2);
    return false;
}
bool js_cocos2dx_engine_GsnClient_reconnect(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_reconnect : Invalid Native Object");
    if (argc == 0) {
        bool ret = cobj->reconnect();
        jsval jsret = JSVAL_NULL;
        jsret = BOOLEAN_TO_JSVAL(ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_reconnect : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_GsnClient_clearQueue(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_clearQueue : Invalid Native Object");
    if (argc == 0) {
        cobj->clearQueue();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_clearQueue : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_GsnClient_send(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    GsnClient* cobj = (GsnClient *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_GsnClient_send : Invalid Native Object");
    if (argc == 1) {
        OutPacket* arg0 = nullptr;
        do {
            if (args.get(0).isNull()) { arg0 = nullptr; break; }
            if (!args.get(0).isObject()) { ok = false; break; }
            js_proxy_t *jsProxy;
            JS::RootedObject tmpObj(cx, args.get(0).toObjectOrNull());
            jsProxy = jsb_get_js_proxy(tmpObj);
            arg0 = (OutPacket*)(jsProxy ? jsProxy->ptr : NULL);
            JSB_PRECONDITION2( arg0, cx, false, "Invalid Native Object");
        } while (0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_GsnClient_send : Error processing arguments");
        cobj->send(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_send : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_GsnClient_destroyInstance(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 0) {
        GsnClient::destroyInstance();
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_destroyInstance : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_GsnClient_create(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 0) {

        auto ret = GsnClient::create();
        js_type_class_t *typeClass = js_get_type_from_native<GsnClient>(ret);
        JS::RootedObject jsret(cx, jsb_ref_autoreleased_create_jsobject(cx, ret, typeClass, "GsnClient"));
        args.rval().set(OBJECT_TO_JSVAL(jsret));
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_create : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_GsnClient_getInstance(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 0) {

        auto ret = GsnClient::getInstance();
        js_type_class_t *typeClass = js_get_type_from_native<GsnClient>(ret);
        JS::RootedObject jsret(cx, jsb_ref_get_or_create_jsobject(cx, ret, typeClass, "GsnClient"));
        args.rval().set(OBJECT_TO_JSVAL(jsret));
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_GsnClient_getInstance : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_GsnClient_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    GsnClient* cobj = new (std::nothrow) GsnClient();

    js_type_class_t *typeClass = js_get_type_from_native<GsnClient>(cobj);

    // link the native object with the javascript object
    JS::RootedObject jsobj(cx, jsb_ref_create_jsobject(cx, cobj, typeClass, "GsnClient"));
    args.rval().set(OBJECT_TO_JSVAL(jsobj));
    if (JS_HasProperty(cx, jsobj, "_ctor", &ok) && ok)
        ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsobj), "_ctor", args);
    return true;
}


void js_register_cocos2dx_engine_GsnClient(JSContext *cx, JS::HandleObject global) {
    jsb_GsnClient_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_GsnClient_class->name = "GsnClient";
    jsb_GsnClient_class->addProperty = JS_PropertyStub;
    jsb_GsnClient_class->delProperty = JS_DeletePropertyStub;
    jsb_GsnClient_class->getProperty = JS_PropertyStub;
    jsb_GsnClient_class->setProperty = JS_StrictPropertyStub;
    jsb_GsnClient_class->enumerate = JS_EnumerateStub;
    jsb_GsnClient_class->resolve = JS_ResolveStub;
    jsb_GsnClient_class->convert = JS_ConvertStub;
    jsb_GsnClient_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FN("onSubThreadStarted", js_cocos2dx_engine_GsnClient_onSubThreadStarted, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("onSubThreadEnded", js_cocos2dx_engine_GsnClient_onSubThreadEnded, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setTimeoutForRead", js_cocos2dx_engine_GsnClient_setTimeoutForRead, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("disconnect", js_cocos2dx_engine_GsnClient_disconnect, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setReceiveDataListener", js_cocos2dx_engine_GsnClient_setReceiveDataListener, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("isDoConnection", js_cocos2dx_engine_GsnClient_isDoConnection, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("initThread", js_cocos2dx_engine_GsnClient_initThread, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setTimeoutForConnect", js_cocos2dx_engine_GsnClient_setTimeoutForConnect, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("createSocket", js_cocos2dx_engine_GsnClient_createSocket, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("onSubThreadLoop", js_cocos2dx_engine_GsnClient_onSubThreadLoop, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getTimeoutForRead", js_cocos2dx_engine_GsnClient_getTimeoutForRead, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setFinishConnectListener", js_cocos2dx_engine_GsnClient_setFinishConnectListener, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("onUIThreadReceiveMessage", js_cocos2dx_engine_GsnClient_onUIThreadReceiveMessage, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setDisconnectListener", js_cocos2dx_engine_GsnClient_setDisconnectListener, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setListener", js_cocos2dx_engine_GsnClient_setListener, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getTimeoutForConnect", js_cocos2dx_engine_GsnClient_getTimeoutForConnect, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("connect", js_cocos2dx_engine_GsnClient_connect, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("reconnect", js_cocos2dx_engine_GsnClient_reconnect, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("clearQueue", js_cocos2dx_engine_GsnClient_clearQueue, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("send", js_cocos2dx_engine_GsnClient_send, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    static JSFunctionSpec st_funcs[] = {
        JS_FN("destroyInstance", js_cocos2dx_engine_GsnClient_destroyInstance, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("create", js_cocos2dx_engine_GsnClient_create, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getInstance", js_cocos2dx_engine_GsnClient_getInstance, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    jsb_GsnClient_prototype = JS_InitClass(
        cx, global,
        JS::NullPtr(),
        jsb_GsnClient_class,
        js_cocos2dx_engine_GsnClient_constructor, 0, // constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, jsb_GsnClient_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "GsnClient"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::TrueHandleValue);
    // add the proto and JSClass to the type->js info hash table
    jsb_register_class<GsnClient>(cx, jsb_GsnClient_class, proto, JS::NullPtr());
}

JSClass  *jsb_Handler_class;
JSObject *jsb_Handler_prototype;

bool js_cocos2dx_engine_Handler_setTimeOut(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    Handler* cobj = (Handler *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_Handler_setTimeOut : Invalid Native Object");
    if (argc == 2) {
        double arg0 = 0;
        bool arg1;
        ok &= JS::ToNumber( cx, args.get(0), &arg0) && !std::isnan(arg0);
        arg1 = JS::ToBoolean(args.get(1));
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_Handler_setTimeOut : Error processing arguments");
        cobj->setTimeOut(arg0, arg1);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_Handler_setTimeOut : wrong number of arguments: %d, was expecting %d", argc, 2);
    return false;
}
bool js_cocos2dx_engine_Handler_setID(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    Handler* cobj = (Handler *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_Handler_setID : Invalid Native Object");
    if (argc == 1) {
        std::string arg0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_Handler_setID : Error processing arguments");
        cobj->setID(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_Handler_setID : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_Handler_set(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    Handler* cobj = (Handler *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_Handler_set : Invalid Native Object");
    if (argc == 1) {
        std::function<void (std::basic_string<char>, cocos2d::Ref *)> arg0;
        do {
		    if(JS_TypeOfValue(cx, args.get(0)) == JSTYPE_FUNCTION)
		    {
		        JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
		        std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(0), args.thisv()));
		        auto lambda = [=](std::basic_string<char> larg0, cocos2d::Ref* larg1) -> void {
		            JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
		            jsval largv[2];
		            largv[0] = std_string_to_jsval(cx, larg0);
		            if (larg1) {
		            largv[1] = OBJECT_TO_JSVAL(js_get_or_create_jsobject<cocos2d::Ref>(cx, (cocos2d::Ref*)larg1));
		        } else {
		            largv[1] = JSVAL_NULL;
		        };
		            JS::RootedValue rval(cx);
		            bool succeed = func->invoke(2, &largv[0], &rval);
		            if (!succeed && JS_IsExceptionPending(cx)) {
		                JS_ReportPendingException(cx);
		            }
		        };
		        arg0 = lambda;
		    }
		    else
		    {
		        arg0 = nullptr;
		    }
		} while(0)
		;
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_Handler_set : Error processing arguments");
        cobj->set(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_Handler_set : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_Handler_stop(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    Handler* cobj = (Handler *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_Handler_stop : Invalid Native Object");
    if (argc == 1) {
        std::string arg0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_Handler_stop : Error processing arguments");
        cobj->stop(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_Handler_stop : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_Handler_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
    bool ok = true;
    Handler* cobj = nullptr;

    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx);
    do {
        if (argc == 0) {
            cobj = new (std::nothrow) Handler();

            js_type_class_t *typeClass = js_get_type_from_native<Handler>(cobj);
            JS::RootedObject proto(cx, typeClass->proto.ref());
            JS::RootedObject parent(cx, typeClass->parentProto.ref());
            obj = JS_NewObject(cx, typeClass->jsclass, proto, parent);
            js_proxy_t* p = jsb_new_proxy(cobj, obj);
            jsb_ref_init(cx, &p->obj, cobj, "Handler");
        }
    } while(0);

    do {
        if (argc == 1) {
            std::string arg0;
            ok &= jsval_to_std_string(cx, args.get(0), &arg0);
            if (!ok) { ok = true; break; }
            cobj = new (std::nothrow) Handler(arg0);

            js_type_class_t *typeClass = js_get_type_from_native<Handler>(cobj);
            JS::RootedObject proto(cx, typeClass->proto.ref());
            JS::RootedObject parent(cx, typeClass->parentProto.ref());
            obj = JS_NewObject(cx, typeClass->jsclass, proto, parent);
            js_proxy_t* p = jsb_new_proxy(cobj, obj);
            jsb_ref_init(cx, &p->obj, cobj, "Handler");
        }
    } while(0);

    if (cobj) {
        if (JS_HasProperty(cx, obj, "_ctor", &ok) && ok)
                ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(obj), "_ctor", args);

        args.rval().set(OBJECT_TO_JSVAL(obj));
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_Handler_constructor : wrong number of arguments");
    return false;
}


void js_register_cocos2dx_engine_Handler(JSContext *cx, JS::HandleObject global) {
    jsb_Handler_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_Handler_class->name = "Handler";
    jsb_Handler_class->addProperty = JS_PropertyStub;
    jsb_Handler_class->delProperty = JS_DeletePropertyStub;
    jsb_Handler_class->getProperty = JS_PropertyStub;
    jsb_Handler_class->setProperty = JS_StrictPropertyStub;
    jsb_Handler_class->enumerate = JS_EnumerateStub;
    jsb_Handler_class->resolve = JS_ResolveStub;
    jsb_Handler_class->convert = JS_ConvertStub;
    jsb_Handler_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FN("setTimeOut", js_cocos2dx_engine_Handler_setTimeOut, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setID", js_cocos2dx_engine_Handler_setID, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("set", js_cocos2dx_engine_Handler_set, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("stop", js_cocos2dx_engine_Handler_stop, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    JSFunctionSpec *st_funcs = NULL;

    jsb_Handler_prototype = JS_InitClass(
        cx, global,
        JS::NullPtr(),
        jsb_Handler_class,
        js_cocos2dx_engine_Handler_constructor, 0, // constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, jsb_Handler_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "Handler"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::TrueHandleValue);
    // add the proto and JSClass to the type->js info hash table
    jsb_register_class<Handler>(cx, jsb_Handler_class, proto, JS::NullPtr());
}

JSClass  *jsb_HandlerManager_class;
JSObject *jsb_HandlerManager_prototype;

bool js_cocos2dx_engine_HandlerManager_addHandler(JSContext *cx, uint32_t argc, jsval *vp)
{
    bool ok = true;
    HandlerManager* cobj = nullptr;

    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx);
    obj.set(args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    cobj = (HandlerManager *)(proxy ? proxy->ptr : nullptr);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_HandlerManager_addHandler : Invalid Native Object");
    do {
        if (argc == 2) {
            std::string arg0;
            ok &= jsval_to_std_string(cx, args.get(0), &arg0);
            if (!ok) { ok = true; break; }
            std::function<void (std::basic_string<char>, cocos2d::Ref *)> arg1;
            do {
			    if(JS_TypeOfValue(cx, args.get(1)) == JSTYPE_FUNCTION)
			    {
			        JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
			        std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(1), args.thisv()));
			        auto lambda = [=](std::basic_string<char> larg0, cocos2d::Ref* larg1) -> void {
			            JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
			            jsval largv[2];
			            largv[0] = std_string_to_jsval(cx, larg0);
			            if (larg1) {
			            largv[1] = OBJECT_TO_JSVAL(js_get_or_create_jsobject<cocos2d::Ref>(cx, (cocos2d::Ref*)larg1));
			        } else {
			            largv[1] = JSVAL_NULL;
			        };
			            JS::RootedValue rval(cx);
			            bool succeed = func->invoke(2, &largv[0], &rval);
			            if (!succeed && JS_IsExceptionPending(cx)) {
			                JS_ReportPendingException(cx);
			            }
			        };
			        arg1 = lambda;
			    }
			    else
			    {
			        arg1 = nullptr;
			    }
			} while(0)
			;
            if (!ok) { ok = true; break; }
            cobj->addHandler(arg0, arg1);
            args.rval().setUndefined();
            return true;
        }
    } while(0);

    do {
        if (argc == 1) {
            Handler* arg0 = nullptr;
            do {
                if (args.get(0).isNull()) { arg0 = nullptr; break; }
                if (!args.get(0).isObject()) { ok = false; break; }
                js_proxy_t *jsProxy;
                JS::RootedObject tmpObj(cx, args.get(0).toObjectOrNull());
                jsProxy = jsb_get_js_proxy(tmpObj);
                arg0 = (Handler*)(jsProxy ? jsProxy->ptr : NULL);
                JSB_PRECONDITION2( arg0, cx, false, "Invalid Native Object");
            } while (0);
            if (!ok) { ok = true; break; }
            cobj->addHandler(arg0);
            args.rval().setUndefined();
            return true;
        }
    } while(0);

    JS_ReportError(cx, "js_cocos2dx_engine_HandlerManager_addHandler : wrong number of arguments");
    return false;
}
bool js_cocos2dx_engine_HandlerManager_update(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    HandlerManager* cobj = (HandlerManager *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_HandlerManager_update : Invalid Native Object");
    if (argc == 1) {
        double arg0 = 0;
        ok &= JS::ToNumber( cx, args.get(0), &arg0) && !std::isnan(arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_HandlerManager_update : Error processing arguments");
        cobj->update(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_HandlerManager_update : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_HandlerManager_forceRemoveHandler(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    HandlerManager* cobj = (HandlerManager *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_HandlerManager_forceRemoveHandler : Invalid Native Object");
    if (argc == 1) {
        std::string arg0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_HandlerManager_forceRemoveHandler : Error processing arguments");
        cobj->forceRemoveHandler(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_HandlerManager_forceRemoveHandler : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_HandlerManager_stopHandler(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    HandlerManager* cobj = (HandlerManager *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_HandlerManager_stopHandler : Invalid Native Object");
    if (argc == 2) {
        std::string arg0;
        std::string arg1;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_HandlerManager_stopHandler : Error processing arguments");
        cobj->stopHandler(arg0, arg1);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_HandlerManager_stopHandler : wrong number of arguments: %d, was expecting %d", argc, 2);
    return false;
}
bool js_cocos2dx_engine_HandlerManager_exitIOS(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    HandlerManager* cobj = (HandlerManager *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_HandlerManager_exitIOS : Invalid Native Object");
    if (argc == 0) {
        cobj->exitIOS();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_HandlerManager_exitIOS : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_HandlerManager_getHandler(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    HandlerManager* cobj = (HandlerManager *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_HandlerManager_getHandler : Invalid Native Object");
    if (argc == 1) {
        std::string arg0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_HandlerManager_getHandler : Error processing arguments");
        Handler* ret = cobj->getHandler(arg0);
        jsval jsret = JSVAL_NULL;
        if (ret) {
            jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<Handler>(cx, (Handler*)ret));
        } else {
            jsret = JSVAL_NULL;
        };
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_HandlerManager_getHandler : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_HandlerManager_destroyInstance(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 0) {
        HandlerManager::destroyInstance();
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_HandlerManager_destroyInstance : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_HandlerManager_getInstance(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 0) {

        auto ret = HandlerManager::getInstance();
        js_type_class_t *typeClass = js_get_type_from_native<HandlerManager>(ret);
        JS::RootedObject jsret(cx, jsb_ref_get_or_create_jsobject(cx, ret, typeClass, "HandlerManager"));
        args.rval().set(OBJECT_TO_JSVAL(jsret));
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_HandlerManager_getInstance : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_HandlerManager_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    HandlerManager* cobj = new (std::nothrow) HandlerManager();

    js_type_class_t *typeClass = js_get_type_from_native<HandlerManager>(cobj);

    // link the native object with the javascript object
    JS::RootedObject jsobj(cx, jsb_ref_create_jsobject(cx, cobj, typeClass, "HandlerManager"));
    args.rval().set(OBJECT_TO_JSVAL(jsobj));
    if (JS_HasProperty(cx, jsobj, "_ctor", &ok) && ok)
        ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsobj), "_ctor", args);
    return true;
}


void js_register_cocos2dx_engine_HandlerManager(JSContext *cx, JS::HandleObject global) {
    jsb_HandlerManager_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_HandlerManager_class->name = "HandlerManager";
    jsb_HandlerManager_class->addProperty = JS_PropertyStub;
    jsb_HandlerManager_class->delProperty = JS_DeletePropertyStub;
    jsb_HandlerManager_class->getProperty = JS_PropertyStub;
    jsb_HandlerManager_class->setProperty = JS_StrictPropertyStub;
    jsb_HandlerManager_class->enumerate = JS_EnumerateStub;
    jsb_HandlerManager_class->resolve = JS_ResolveStub;
    jsb_HandlerManager_class->convert = JS_ConvertStub;
    jsb_HandlerManager_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FN("addHandler", js_cocos2dx_engine_HandlerManager_addHandler, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("update", js_cocos2dx_engine_HandlerManager_update, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("forceRemoveHandler", js_cocos2dx_engine_HandlerManager_forceRemoveHandler, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("stopHandler", js_cocos2dx_engine_HandlerManager_stopHandler, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("exitIOS", js_cocos2dx_engine_HandlerManager_exitIOS, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getHandler", js_cocos2dx_engine_HandlerManager_getHandler, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    static JSFunctionSpec st_funcs[] = {
        JS_FN("destroyInstance", js_cocos2dx_engine_HandlerManager_destroyInstance, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getInstance", js_cocos2dx_engine_HandlerManager_getInstance, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    jsb_HandlerManager_prototype = JS_InitClass(
        cx, global,
        JS::NullPtr(),
        jsb_HandlerManager_class,
        js_cocos2dx_engine_HandlerManager_constructor, 0, // constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, jsb_HandlerManager_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "HandlerManager"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::TrueHandleValue);
    // add the proto and JSClass to the type->js info hash table
    jsb_register_class<HandlerManager>(cx, jsb_HandlerManager_class, proto, JS::NullPtr());
}

JSClass  *jsb_WP8Bridgle_class;
JSObject *jsb_WP8Bridgle_prototype;

bool js_cocos2dx_engine_WP8Bridgle_openURL(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 1) {
        std::string arg0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_WP8Bridgle_openURL : Error processing arguments");
        WP8Bridgle::openURL(arg0);
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_openURL : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_getIMEI(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 0) {

        std::string ret = WP8Bridgle::getIMEI();
        jsval jsret = JSVAL_NULL;
        jsret = std_string_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_getIMEI : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_submitLoginZalo(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 4) {
        std::string arg0;
        std::string arg1;
        std::string arg2;
        std::string arg3;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        ok &= jsval_to_std_string(cx, args.get(2), &arg2);
        ok &= jsval_to_std_string(cx, args.get(3), &arg3);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_WP8Bridgle_submitLoginZalo : Error processing arguments");
        WP8Bridgle::submitLoginZalo(arg0, arg1, arg2, arg3);
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_submitLoginZalo : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_showNoNetwork(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 0) {
        WP8Bridgle::showNoNetwork();
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_showNoNetwork : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_logoutZALO(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 0) {
        WP8Bridgle::logoutZALO();
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_logoutZALO : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_sendLogLogin(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 4) {
        std::string arg0;
        std::string arg1;
        std::string arg2;
        std::string arg3;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        ok &= jsval_to_std_string(cx, args.get(2), &arg2);
        ok &= jsval_to_std_string(cx, args.get(3), &arg3);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_WP8Bridgle_sendLogLogin : Error processing arguments");
        WP8Bridgle::sendLogLogin(arg0, arg1, arg2, arg3);
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_sendLogLogin : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_showUpdate2(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 1) {
        std::string arg0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_WP8Bridgle_showUpdate2 : Error processing arguments");
        WP8Bridgle::showUpdate2(arg0);
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_showUpdate2 : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_showUpdate1(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 1) {
        std::string arg0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_WP8Bridgle_showUpdate1 : Error processing arguments");
        WP8Bridgle::showUpdate1(arg0);
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_showUpdate1 : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_loginFB(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 0) {
        WP8Bridgle::loginFB();
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_loginFB : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_getZALOFriends(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 0) {
        WP8Bridgle::getZALOFriends();
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_getZALOFriends : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_loginZALO(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 0) {
        WP8Bridgle::loginZALO();
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_loginZALO : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_checkNetworkAvaiable(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 0) {

        bool ret = WP8Bridgle::checkNetworkAvaiable();
        jsval jsret = JSVAL_NULL;
        jsret = BOOLEAN_TO_JSVAL(ret);
        args.rval().set(jsret);
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_checkNetworkAvaiable : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_sendMessage(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 2) {
        std::string arg0;
        std::string arg1;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_WP8Bridgle_sendMessage : Error processing arguments");
        WP8Bridgle::sendMessage(arg0, arg1);
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_sendMessage : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_logoutFB(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 0) {
        WP8Bridgle::logoutFB();
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_logoutFB : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_payment(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 4) {
        std::string arg0;
        std::string arg1;
        std::string arg2;
        std::string arg3;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        ok &= jsval_to_std_string(cx, args.get(2), &arg2);
        ok &= jsval_to_std_string(cx, args.get(3), &arg3);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_WP8Bridgle_payment : Error processing arguments");
        WP8Bridgle::payment(arg0, arg1, arg2, arg3);
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_payment : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_showMaintain(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 1) {
        std::string arg0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_WP8Bridgle_showMaintain : Error processing arguments");
        WP8Bridgle::showMaintain(arg0);
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_showMaintain : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_WP8Bridgle_openURLUpdate(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 1) {
        std::string arg0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_WP8Bridgle_openURLUpdate : Error processing arguments");
        WP8Bridgle::openURLUpdate(arg0);
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_WP8Bridgle_openURLUpdate : wrong number of arguments");
    return false;
}


void js_register_cocos2dx_engine_WP8Bridgle(JSContext *cx, JS::HandleObject global) {
    jsb_WP8Bridgle_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_WP8Bridgle_class->name = "WP8Bridgle";
    jsb_WP8Bridgle_class->addProperty = JS_PropertyStub;
    jsb_WP8Bridgle_class->delProperty = JS_DeletePropertyStub;
    jsb_WP8Bridgle_class->getProperty = JS_PropertyStub;
    jsb_WP8Bridgle_class->setProperty = JS_StrictPropertyStub;
    jsb_WP8Bridgle_class->enumerate = JS_EnumerateStub;
    jsb_WP8Bridgle_class->resolve = JS_ResolveStub;
    jsb_WP8Bridgle_class->convert = JS_ConvertStub;
    jsb_WP8Bridgle_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FS_END
    };

    static JSFunctionSpec st_funcs[] = {
        JS_FN("openURL", js_cocos2dx_engine_WP8Bridgle_openURL, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getIMEI", js_cocos2dx_engine_WP8Bridgle_getIMEI, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("submitLoginZalo", js_cocos2dx_engine_WP8Bridgle_submitLoginZalo, 4, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("showNoNetwork", js_cocos2dx_engine_WP8Bridgle_showNoNetwork, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("logoutZALO", js_cocos2dx_engine_WP8Bridgle_logoutZALO, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("sendLogLogin", js_cocos2dx_engine_WP8Bridgle_sendLogLogin, 4, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("showUpdate2", js_cocos2dx_engine_WP8Bridgle_showUpdate2, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("showUpdate1", js_cocos2dx_engine_WP8Bridgle_showUpdate1, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("loginFB", js_cocos2dx_engine_WP8Bridgle_loginFB, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getZALOFriends", js_cocos2dx_engine_WP8Bridgle_getZALOFriends, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("loginZALO", js_cocos2dx_engine_WP8Bridgle_loginZALO, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("checkNetworkAvaiable", js_cocos2dx_engine_WP8Bridgle_checkNetworkAvaiable, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("sendMessage", js_cocos2dx_engine_WP8Bridgle_sendMessage, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("logoutFB", js_cocos2dx_engine_WP8Bridgle_logoutFB, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("payment", js_cocos2dx_engine_WP8Bridgle_payment, 4, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("showMaintain", js_cocos2dx_engine_WP8Bridgle_showMaintain, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("openURLUpdate", js_cocos2dx_engine_WP8Bridgle_openURLUpdate, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    jsb_WP8Bridgle_prototype = JS_InitClass(
        cx, global,
        JS::NullPtr(),
        jsb_WP8Bridgle_class,
        dummy_constructor<WP8Bridgle>, 0, // no constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, jsb_WP8Bridgle_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "WP8Bridgle"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::TrueHandleValue);
    // add the proto and JSClass to the type->js info hash table
    jsb_register_class<WP8Bridgle>(cx, jsb_WP8Bridgle_class, proto, JS::NullPtr());
}

JSClass  *jsb_UIAvatar_class;
JSObject *jsb_UIAvatar_prototype;

bool js_cocos2dx_engine_UIAvatar_setTexture(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    UIAvatar* cobj = (UIAvatar *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_UIAvatar_setTexture : Invalid Native Object");
    if (argc == 1) {
        cocos2d::Texture2D* arg0 = nullptr;
        do {
            if (args.get(0).isNull()) { arg0 = nullptr; break; }
            if (!args.get(0).isObject()) { ok = false; break; }
            js_proxy_t *jsProxy;
            JS::RootedObject tmpObj(cx, args.get(0).toObjectOrNull());
            jsProxy = jsb_get_js_proxy(tmpObj);
            arg0 = (cocos2d::Texture2D*)(jsProxy ? jsProxy->ptr : NULL);
            JSB_PRECONDITION2( arg0, cx, false, "Invalid Native Object");
        } while (0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_UIAvatar_setTexture : Error processing arguments");
        cobj->setTexture(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_setTexture : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_UIAvatar_callbackDownload(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    UIAvatar* cobj = (UIAvatar *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_UIAvatar_callbackDownload : Invalid Native Object");
    if (argc == 2) {
        int arg0 = 0;
        std::string arg1;
        ok &= jsval_to_int32(cx, args.get(0), (int32_t *)&arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_UIAvatar_callbackDownload : Error processing arguments");
        cobj->callbackDownload(arg0, arg1);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_callbackDownload : wrong number of arguments: %d, was expecting %d", argc, 2);
    return false;
}
bool js_cocos2dx_engine_UIAvatar_setImage(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    UIAvatar* cobj = (UIAvatar *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_UIAvatar_setImage : Invalid Native Object");
    if (argc == 1) {
        std::string arg0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_UIAvatar_setImage : Error processing arguments");
        cobj->setImage(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_setImage : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_UIAvatar_setDefaultImage(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    UIAvatar* cobj = (UIAvatar *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_UIAvatar_setDefaultImage : Invalid Native Object");
    if (argc == 0) {
        cobj->setDefaultImage();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_setDefaultImage : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_UIAvatar_asyncExecuteWithUrl(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    UIAvatar* cobj = (UIAvatar *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_UIAvatar_asyncExecuteWithUrl : Invalid Native Object");
    if (argc == 2) {
        std::string arg0;
        std::string arg1;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_UIAvatar_asyncExecuteWithUrl : Error processing arguments");
        cobj->asyncExecuteWithUrl(arg0, arg1);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_asyncExecuteWithUrl : wrong number of arguments: %d, was expecting %d", argc, 2);
    return false;
}
bool js_cocos2dx_engine_UIAvatar_setOpacity(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    UIAvatar* cobj = (UIAvatar *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_UIAvatar_setOpacity : Invalid Native Object");
    if (argc == 1) {
        uint16_t arg0;
        ok &= jsval_to_uint16(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_UIAvatar_setOpacity : Error processing arguments");
        cobj->setOpacity(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_setOpacity : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_UIAvatar_getImageSize(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    UIAvatar* cobj = (UIAvatar *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_UIAvatar_getImageSize : Invalid Native Object");
    if (argc == 0) {
        cocos2d::Size ret = cobj->getImageSize();
        jsval jsret = JSVAL_NULL;
        jsret = ccsize_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_getImageSize : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_UIAvatar_asyncExecute(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    UIAvatar* cobj = (UIAvatar *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_UIAvatar_asyncExecute : Invalid Native Object");
    if (argc == 0) {
        cobj->asyncExecute();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_asyncExecute : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_UIAvatar_setIntegerForKey(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 2) {
        std::string arg0;
        int arg1 = 0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_int32(cx, args.get(1), (int32_t *)&arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_UIAvatar_setIntegerForKey : Error processing arguments");
        UIAvatar::setIntegerForKey(arg0, arg1);
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_setIntegerForKey : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_UIAvatar_getAppversionString(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    if (argc == 0) {

        std::string ret = UIAvatar::getAppversionString();
        jsval jsret = JSVAL_NULL;
        jsret = std_string_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_getAppversionString : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_UIAvatar_create(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    
    do {
        if (argc == 1) {
            std::string arg0;
            ok &= jsval_to_std_string(cx, args.get(0), &arg0);
            if (!ok) { ok = true; break; }
            UIAvatar* ret = UIAvatar::create(arg0);
            jsval jsret = JSVAL_NULL;
            if (ret) {
                jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<UIAvatar>(cx, (UIAvatar*)ret));
            } else {
                jsret = JSVAL_NULL;
            };
            args.rval().set(jsret);
            return true;
        }
    } while (0);
    
    do {
        if (argc == 3) {
            std::string arg0;
            ok &= jsval_to_std_string(cx, args.get(0), &arg0);
            if (!ok) { ok = true; break; }
            std::string arg1;
            ok &= jsval_to_std_string(cx, args.get(1), &arg1);
            if (!ok) { ok = true; break; }
            std::string arg2;
            ok &= jsval_to_std_string(cx, args.get(2), &arg2);
            if (!ok) { ok = true; break; }
            UIAvatar* ret = UIAvatar::create(arg0, arg1, arg2);
            jsval jsret = JSVAL_NULL;
            if (ret) {
                jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<UIAvatar>(cx, (UIAvatar*)ret));
            } else {
                jsret = JSVAL_NULL;
            };
            args.rval().set(jsret);
            return true;
        }
    } while (0);
    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_create : wrong number of arguments");
    return false;
}
bool js_cocos2dx_engine_UIAvatar_getStringForKey(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 1) {
        std::string arg0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_UIAvatar_getStringForKey : Error processing arguments");

        std::string ret = UIAvatar::getStringForKey(arg0);
        jsval jsret = JSVAL_NULL;
        jsret = std_string_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_getStringForKey : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_UIAvatar_setStringForKey(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 2) {
        std::string arg0;
        std::string arg1;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_UIAvatar_setStringForKey : Error processing arguments");
        UIAvatar::setStringForKey(arg0, arg1);
        args.rval().setUndefined();
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_setStringForKey : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_UIAvatar_createWithMask(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 2) {
        std::string arg0;
        std::string arg1;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_UIAvatar_createWithMask : Error processing arguments");

        auto ret = UIAvatar::createWithMask(arg0, arg1);
        js_type_class_t *typeClass = js_get_type_from_native<UIAvatar>(ret);
        JS::RootedObject jsret(cx, jsb_ref_autoreleased_create_jsobject(cx, ret, typeClass, "UIAvatar"));
        args.rval().set(OBJECT_TO_JSVAL(jsret));
        return true;
    }
    if (argc == 3) {
        std::string arg0;
        std::string arg1;
        std::string arg2;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        ok &= jsval_to_std_string(cx, args.get(1), &arg1);
        ok &= jsval_to_std_string(cx, args.get(2), &arg2);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_UIAvatar_createWithMask : Error processing arguments");

        auto ret = UIAvatar::createWithMask(arg0, arg1, arg2);
        js_type_class_t *typeClass = js_get_type_from_native<UIAvatar>(ret);
        JS::RootedObject jsret(cx, jsb_ref_autoreleased_create_jsobject(cx, ret, typeClass, "UIAvatar"));
        args.rval().set(OBJECT_TO_JSVAL(jsret));
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_createWithMask : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_UIAvatar_getIntegerForKey(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 1) {
        std::string arg0;
        ok &= jsval_to_std_string(cx, args.get(0), &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_UIAvatar_getIntegerForKey : Error processing arguments");

        int ret = UIAvatar::getIntegerForKey(arg0);
        jsval jsret = JSVAL_NULL;
        jsret = int32_to_jsval(cx, ret);
        args.rval().set(jsret);
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_UIAvatar_getIntegerForKey : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_UIAvatar_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    UIAvatar* cobj = new (std::nothrow) UIAvatar();

    js_type_class_t *typeClass = js_get_type_from_native<UIAvatar>(cobj);

    // link the native object with the javascript object
    JS::RootedObject jsobj(cx, jsb_ref_create_jsobject(cx, cobj, typeClass, "UIAvatar"));
    args.rval().set(OBJECT_TO_JSVAL(jsobj));
    if (JS_HasProperty(cx, jsobj, "_ctor", &ok) && ok)
        ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsobj), "_ctor", args);
    return true;
}


extern JSObject *jsb_cocos2d_ui_Widget_prototype;

void js_register_cocos2dx_engine_UIAvatar(JSContext *cx, JS::HandleObject global) {
    jsb_UIAvatar_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_UIAvatar_class->name = "UIAvatar";
    jsb_UIAvatar_class->addProperty = JS_PropertyStub;
    jsb_UIAvatar_class->delProperty = JS_DeletePropertyStub;
    jsb_UIAvatar_class->getProperty = JS_PropertyStub;
    jsb_UIAvatar_class->setProperty = JS_StrictPropertyStub;
    jsb_UIAvatar_class->enumerate = JS_EnumerateStub;
    jsb_UIAvatar_class->resolve = JS_ResolveStub;
    jsb_UIAvatar_class->convert = JS_ConvertStub;
    jsb_UIAvatar_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FN("setTexture", js_cocos2dx_engine_UIAvatar_setTexture, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("callbackDownload", js_cocos2dx_engine_UIAvatar_callbackDownload, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setImage", js_cocos2dx_engine_UIAvatar_setImage, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setDefaultImage", js_cocos2dx_engine_UIAvatar_setDefaultImage, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("asyncExecuteWithUrl", js_cocos2dx_engine_UIAvatar_asyncExecuteWithUrl, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setOpacity", js_cocos2dx_engine_UIAvatar_setOpacity, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getImageSize", js_cocos2dx_engine_UIAvatar_getImageSize, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("asyncExecute", js_cocos2dx_engine_UIAvatar_asyncExecute, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    static JSFunctionSpec st_funcs[] = {
        JS_FN("setIntegerForKey", js_cocos2dx_engine_UIAvatar_setIntegerForKey, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getAppversionString", js_cocos2dx_engine_UIAvatar_getAppversionString, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("create", js_cocos2dx_engine_UIAvatar_create, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getStringForKey", js_cocos2dx_engine_UIAvatar_getStringForKey, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("setStringForKey", js_cocos2dx_engine_UIAvatar_setStringForKey, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("createWithMask", js_cocos2dx_engine_UIAvatar_createWithMask, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("getIntegerForKey", js_cocos2dx_engine_UIAvatar_getIntegerForKey, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    JS::RootedObject parent_proto(cx, jsb_cocos2d_ui_Widget_prototype);
    jsb_UIAvatar_prototype = JS_InitClass(
        cx, global,
        parent_proto,
        jsb_UIAvatar_class,
        js_cocos2dx_engine_UIAvatar_constructor, 0, // constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, jsb_UIAvatar_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "UIAvatar"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::TrueHandleValue);
    // add the proto and JSClass to the type->js info hash table
    jsb_register_class<UIAvatar>(cx, jsb_UIAvatar_class, proto, parent_proto);
}

JSClass  *jsb_CircleMove_class;
JSObject *jsb_CircleMove_prototype;

bool js_cocos2dx_engine_CircleMove_create(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 2) {
        double arg0 = 0;
        double arg1 = 0;
        ok &= JS::ToNumber( cx, args.get(0), &arg0) && !std::isnan(arg0);
        ok &= JS::ToNumber( cx, args.get(1), &arg1) && !std::isnan(arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_CircleMove_create : Error processing arguments");

        auto ret = CircleMove::create(arg0, arg1);
        js_type_class_t *typeClass = js_get_type_from_native<CircleMove>(ret);
        JS::RootedObject jsret(cx, jsb_ref_autoreleased_create_jsobject(cx, ret, typeClass, "CircleMove"));
        args.rval().set(OBJECT_TO_JSVAL(jsret));
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_CircleMove_create : wrong number of arguments");
    return false;
}


extern JSObject *jsb_cocos2d_ActionInterval_prototype;

void js_register_cocos2dx_engine_CircleMove(JSContext *cx, JS::HandleObject global) {
    jsb_CircleMove_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_CircleMove_class->name = "CircleMove";
    jsb_CircleMove_class->addProperty = JS_PropertyStub;
    jsb_CircleMove_class->delProperty = JS_DeletePropertyStub;
    jsb_CircleMove_class->getProperty = JS_PropertyStub;
    jsb_CircleMove_class->setProperty = JS_StrictPropertyStub;
    jsb_CircleMove_class->enumerate = JS_EnumerateStub;
    jsb_CircleMove_class->resolve = JS_ResolveStub;
    jsb_CircleMove_class->convert = JS_ConvertStub;
    jsb_CircleMove_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FS_END
    };

    static JSFunctionSpec st_funcs[] = {
        JS_FN("create", js_cocos2dx_engine_CircleMove_create, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    JS::RootedObject parent_proto(cx, jsb_cocos2d_ActionInterval_prototype);
    jsb_CircleMove_prototype = JS_InitClass(
        cx, global,
        parent_proto,
        jsb_CircleMove_class,
        dummy_constructor<CircleMove>, 0, // no constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, jsb_CircleMove_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "CircleMove"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::TrueHandleValue);
    // add the proto and JSClass to the type->js info hash table
    jsb_register_class<CircleMove>(cx, jsb_CircleMove_class, proto, parent_proto);
}

JSClass  *jsb_TimeProgressEffect_class;
JSObject *jsb_TimeProgressEffect_prototype;

bool js_cocos2dx_engine_TimeProgressEffect_setNen(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    TimeProgressEffect* cobj = (TimeProgressEffect *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_TimeProgressEffect_setNen : Invalid Native Object");
    if (argc == 1) {
        cocos2d::Sprite* arg0 = nullptr;
        do {
            if (args.get(0).isNull()) { arg0 = nullptr; break; }
            if (!args.get(0).isObject()) { ok = false; break; }
            js_proxy_t *jsProxy;
            JS::RootedObject tmpObj(cx, args.get(0).toObjectOrNull());
            jsProxy = jsb_get_js_proxy(tmpObj);
            arg0 = (cocos2d::Sprite*)(jsProxy ? jsProxy->ptr : NULL);
            JSB_PRECONDITION2( arg0, cx, false, "Invalid Native Object");
        } while (0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_TimeProgressEffect_setNen : Error processing arguments");
        cobj->setNen(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_TimeProgressEffect_setNen : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_TimeProgressEffect_update(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    TimeProgressEffect* cobj = (TimeProgressEffect *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_TimeProgressEffect_update : Invalid Native Object");
    if (argc == 1) {
        double arg0 = 0;
        ok &= JS::ToNumber( cx, args.get(0), &arg0) && !std::isnan(arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_TimeProgressEffect_update : Error processing arguments");
        cobj->update(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_TimeProgressEffect_update : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_TimeProgressEffect_create(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 3) {
        cocos2d::ProgressTimer* arg0 = nullptr;
        double arg1 = 0;
        double arg2 = 0;
        do {
            if (args.get(0).isNull()) { arg0 = nullptr; break; }
            if (!args.get(0).isObject()) { ok = false; break; }
            js_proxy_t *jsProxy;
            JS::RootedObject tmpObj(cx, args.get(0).toObjectOrNull());
            jsProxy = jsb_get_js_proxy(tmpObj);
            arg0 = (cocos2d::CCProgressTimer*)(jsProxy ? jsProxy->ptr : NULL);
            JSB_PRECONDITION2( arg0, cx, false, "Invalid Native Object");
        } while (0);
        ok &= JS::ToNumber( cx, args.get(1), &arg1) && !std::isnan(arg1);
        ok &= JS::ToNumber( cx, args.get(2), &arg2) && !std::isnan(arg2);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_TimeProgressEffect_create : Error processing arguments");

        auto ret = TimeProgressEffect::create(arg0, arg1, arg2);
        js_type_class_t *typeClass = js_get_type_from_native<TimeProgressEffect>(ret);
        JS::RootedObject jsret(cx, jsb_ref_autoreleased_create_jsobject(cx, ret, typeClass, "TimeProgressEffect"));
        args.rval().set(OBJECT_TO_JSVAL(jsret));
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_TimeProgressEffect_create : wrong number of arguments");
    return false;
}

bool js_cocos2dx_engine_TimeProgressEffect_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    TimeProgressEffect* cobj = new (std::nothrow) TimeProgressEffect();

    js_type_class_t *typeClass = js_get_type_from_native<TimeProgressEffect>(cobj);

    // link the native object with the javascript object
    JS::RootedObject jsobj(cx, jsb_ref_create_jsobject(cx, cobj, typeClass, "TimeProgressEffect"));
    args.rval().set(OBJECT_TO_JSVAL(jsobj));
    if (JS_HasProperty(cx, jsobj, "_ctor", &ok) && ok)
        ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsobj), "_ctor", args);
    return true;
}


extern JSObject *jsb_cocos2d_ProgressFromTo_prototype;

void js_register_cocos2dx_engine_TimeProgressEffect(JSContext *cx, JS::HandleObject global) {
    jsb_TimeProgressEffect_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_TimeProgressEffect_class->name = "TimeProgressEffect";
    jsb_TimeProgressEffect_class->addProperty = JS_PropertyStub;
    jsb_TimeProgressEffect_class->delProperty = JS_DeletePropertyStub;
    jsb_TimeProgressEffect_class->getProperty = JS_PropertyStub;
    jsb_TimeProgressEffect_class->setProperty = JS_StrictPropertyStub;
    jsb_TimeProgressEffect_class->enumerate = JS_EnumerateStub;
    jsb_TimeProgressEffect_class->resolve = JS_ResolveStub;
    jsb_TimeProgressEffect_class->convert = JS_ConvertStub;
    jsb_TimeProgressEffect_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FN("setNen", js_cocos2dx_engine_TimeProgressEffect_setNen, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("update", js_cocos2dx_engine_TimeProgressEffect_update, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    static JSFunctionSpec st_funcs[] = {
        JS_FN("create", js_cocos2dx_engine_TimeProgressEffect_create, 3, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    JS::RootedObject parent_proto(cx, jsb_cocos2d_ProgressFromTo_prototype);
    jsb_TimeProgressEffect_prototype = JS_InitClass(
        cx, global,
        parent_proto,
        jsb_TimeProgressEffect_class,
        js_cocos2dx_engine_TimeProgressEffect_constructor, 0, // constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, jsb_TimeProgressEffect_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "TimeProgressEffect"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::TrueHandleValue);
    // add the proto and JSClass to the type->js info hash table
    jsb_register_class<TimeProgressEffect>(cx, jsb_TimeProgressEffect_class, proto, parent_proto);
}

JSClass  *jsb_MoveCircle_class;
JSObject *jsb_MoveCircle_prototype;

bool js_cocos2dx_engine_MoveCircle_startWithTarget(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    MoveCircle* cobj = (MoveCircle *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_MoveCircle_startWithTarget : Invalid Native Object");
    if (argc == 1) {
        cocos2d::Node* arg0 = nullptr;
        do {
            if (args.get(0).isNull()) { arg0 = nullptr; break; }
            if (!args.get(0).isObject()) { ok = false; break; }
            js_proxy_t *jsProxy;
            JS::RootedObject tmpObj(cx, args.get(0).toObjectOrNull());
            jsProxy = jsb_get_js_proxy(tmpObj);
            arg0 = (cocos2d::Node*)(jsProxy ? jsProxy->ptr : NULL);
            JSB_PRECONDITION2( arg0, cx, false, "Invalid Native Object");
        } while (0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_MoveCircle_startWithTarget : Error processing arguments");
        cobj->startWithTarget(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_MoveCircle_startWithTarget : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_MoveCircle_update(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    MoveCircle* cobj = (MoveCircle *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_MoveCircle_update : Invalid Native Object");
    if (argc == 1) {
        double arg0 = 0;
        ok &= JS::ToNumber( cx, args.get(0), &arg0) && !std::isnan(arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_MoveCircle_update : Error processing arguments");
        cobj->update(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_MoveCircle_update : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_MoveCircle_create(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    if (argc == 4) {
        double arg0 = 0;
        double arg1 = 0;
        double arg2 = 0;
        double arg3 = 0;
        ok &= JS::ToNumber( cx, args.get(0), &arg0) && !std::isnan(arg0);
        ok &= JS::ToNumber( cx, args.get(1), &arg1) && !std::isnan(arg1);
        ok &= JS::ToNumber( cx, args.get(2), &arg2) && !std::isnan(arg2);
        ok &= JS::ToNumber( cx, args.get(3), &arg3) && !std::isnan(arg3);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_MoveCircle_create : Error processing arguments");

        MoveCircle* ret = MoveCircle::create(arg0, arg1, arg2, arg3);
        jsval jsret = JSVAL_NULL;
        if (ret) {
        jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<MoveCircle>(cx, (MoveCircle*)ret));
    } else {
        jsret = JSVAL_NULL;
    };
        args.rval().set(jsret);
        return true;
    }
    JS_ReportError(cx, "js_cocos2dx_engine_MoveCircle_create : wrong number of arguments");
    return false;
}


void js_register_cocos2dx_engine_MoveCircle(JSContext *cx, JS::HandleObject global) {
    jsb_MoveCircle_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_MoveCircle_class->name = "MoveCircle";
    jsb_MoveCircle_class->addProperty = JS_PropertyStub;
    jsb_MoveCircle_class->delProperty = JS_DeletePropertyStub;
    jsb_MoveCircle_class->getProperty = JS_PropertyStub;
    jsb_MoveCircle_class->setProperty = JS_StrictPropertyStub;
    jsb_MoveCircle_class->enumerate = JS_EnumerateStub;
    jsb_MoveCircle_class->resolve = JS_ResolveStub;
    jsb_MoveCircle_class->convert = JS_ConvertStub;
    jsb_MoveCircle_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FN("startWithTarget", js_cocos2dx_engine_MoveCircle_startWithTarget, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("update", js_cocos2dx_engine_MoveCircle_update, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    static JSFunctionSpec st_funcs[] = {
        JS_FN("create", js_cocos2dx_engine_MoveCircle_create, 4, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    jsb_MoveCircle_prototype = JS_InitClass(
        cx, global,
        JS::NullPtr(),
        jsb_MoveCircle_class,
        dummy_constructor<MoveCircle>, 0, // no constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, jsb_MoveCircle_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "MoveCircle"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::FalseHandleValue);
    // add the proto and JSClass to the type->js info hash table
    jsb_register_class<MoveCircle>(cx, jsb_MoveCircle_class, proto, JS::NullPtr());
}

JSClass  *jsb_CCShake_class;
JSObject *jsb_CCShake_prototype;

bool js_cocos2dx_engine_CCShake_startWithTarget(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    CCShake* cobj = (CCShake *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_CCShake_startWithTarget : Invalid Native Object");
    if (argc == 1) {
        cocos2d::Node* arg0 = nullptr;
        do {
            if (args.get(0).isNull()) { arg0 = nullptr; break; }
            if (!args.get(0).isObject()) { ok = false; break; }
            js_proxy_t *jsProxy;
            JS::RootedObject tmpObj(cx, args.get(0).toObjectOrNull());
            jsProxy = jsb_get_js_proxy(tmpObj);
            arg0 = (cocos2d::CCNode*)(jsProxy ? jsProxy->ptr : NULL);
            JSB_PRECONDITION2( arg0, cx, false, "Invalid Native Object");
        } while (0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_CCShake_startWithTarget : Error processing arguments");
        cobj->startWithTarget(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_CCShake_startWithTarget : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_CCShake_stop(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    CCShake* cobj = (CCShake *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_CCShake_stop : Invalid Native Object");
    if (argc == 0) {
        cobj->stop();
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_CCShake_stop : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_cocos2dx_engine_CCShake_update(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    CCShake* cobj = (CCShake *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_CCShake_update : Invalid Native Object");
    if (argc == 1) {
        double arg0 = 0;
        ok &= JS::ToNumber( cx, args.get(0), &arg0) && !std::isnan(arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_CCShake_update : Error processing arguments");
        cobj->update(arg0);
        args.rval().setUndefined();
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_CCShake_update : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_cocos2dx_engine_CCShake_initWithDuration(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    CCShake* cobj = (CCShake *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_cocos2dx_engine_CCShake_initWithDuration : Invalid Native Object");
    if (argc == 3) {
        double arg0 = 0;
        double arg1 = 0;
        double arg2 = 0;
        ok &= JS::ToNumber( cx, args.get(0), &arg0) && !std::isnan(arg0);
        ok &= JS::ToNumber( cx, args.get(1), &arg1) && !std::isnan(arg1);
        ok &= JS::ToNumber( cx, args.get(2), &arg2) && !std::isnan(arg2);
        JSB_PRECONDITION2(ok, cx, false, "js_cocos2dx_engine_CCShake_initWithDuration : Error processing arguments");
        bool ret = cobj->initWithDuration(arg0, arg1, arg2);
        jsval jsret = JSVAL_NULL;
        jsret = BOOLEAN_TO_JSVAL(ret);
        args.rval().set(jsret);
        return true;
    }

    JS_ReportError(cx, "js_cocos2dx_engine_CCShake_initWithDuration : wrong number of arguments: %d, was expecting %d", argc, 3);
    return false;
}
bool js_cocos2dx_engine_CCShake_actionWithDuration(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    
    do {
        if (argc == 3) {
            double arg0 = 0;
            ok &= JS::ToNumber( cx, args.get(0), &arg0) && !std::isnan(arg0);
            if (!ok) { ok = true; break; }
            double arg1 = 0;
            ok &= JS::ToNumber( cx, args.get(1), &arg1) && !std::isnan(arg1);
            if (!ok) { ok = true; break; }
            double arg2 = 0;
            ok &= JS::ToNumber( cx, args.get(2), &arg2) && !std::isnan(arg2);
            if (!ok) { ok = true; break; }
            CCShake* ret = CCShake::actionWithDuration(arg0, arg1, arg2);
            jsval jsret = JSVAL_NULL;
            if (ret) {
                jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<CCShake>(cx, (CCShake*)ret));
            } else {
                jsret = JSVAL_NULL;
            };
            args.rval().set(jsret);
            return true;
        }
    } while (0);
    
    do {
        if (argc == 2) {
            double arg0 = 0;
            ok &= JS::ToNumber( cx, args.get(0), &arg0) && !std::isnan(arg0);
            if (!ok) { ok = true; break; }
            double arg1 = 0;
            ok &= JS::ToNumber( cx, args.get(1), &arg1) && !std::isnan(arg1);
            if (!ok) { ok = true; break; }
            CCShake* ret = CCShake::actionWithDuration(arg0, arg1);
            jsval jsret = JSVAL_NULL;
            if (ret) {
                jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<CCShake>(cx, (CCShake*)ret));
            } else {
                jsret = JSVAL_NULL;
            };
            args.rval().set(jsret);
            return true;
        }
    } while (0);
    JS_ReportError(cx, "js_cocos2dx_engine_CCShake_actionWithDuration : wrong number of arguments");
    return false;
}
bool js_cocos2dx_engine_CCShake_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
    JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
    bool ok = true;
    CCShake* cobj = new (std::nothrow) CCShake();

    js_type_class_t *typeClass = js_get_type_from_native<CCShake>(cobj);

    // link the native object with the javascript object
    JS::RootedObject jsobj(cx, jsb_create_weak_jsobject(cx, cobj, typeClass, "CCShake"));
    args.rval().set(OBJECT_TO_JSVAL(jsobj));
    if (JS_HasProperty(cx, jsobj, "_ctor", &ok) && ok)
        ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsobj), "_ctor", args);
    return true;
}


void js_CCShake_finalize(JSFreeOp *fop, JSObject *obj) {
    CCLOGINFO("jsbindings: finalizing JS object %p (CCShake)", obj);
    js_proxy_t* nproxy;
    js_proxy_t* jsproxy;
    JSContext *cx = ScriptingCore::getInstance()->getGlobalContext();
    JS::RootedObject jsobj(cx, obj);
    jsproxy = jsb_get_js_proxy(jsobj);
    if (jsproxy) {
        CCShake *nobj = static_cast<CCShake *>(jsproxy->ptr);
        nproxy = jsb_get_native_proxy(jsproxy->ptr);

        if (nobj) {
            jsb_remove_proxy(nproxy, jsproxy);
            delete nobj;
        }
        else
            jsb_remove_proxy(nullptr, jsproxy);
    }
}
void js_register_cocos2dx_engine_CCShake(JSContext *cx, JS::HandleObject global) {
    jsb_CCShake_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_CCShake_class->name = "CCShake";
    jsb_CCShake_class->addProperty = JS_PropertyStub;
    jsb_CCShake_class->delProperty = JS_DeletePropertyStub;
    jsb_CCShake_class->getProperty = JS_PropertyStub;
    jsb_CCShake_class->setProperty = JS_StrictPropertyStub;
    jsb_CCShake_class->enumerate = JS_EnumerateStub;
    jsb_CCShake_class->resolve = JS_ResolveStub;
    jsb_CCShake_class->convert = JS_ConvertStub;
    jsb_CCShake_class->finalize = js_CCShake_finalize;
    jsb_CCShake_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
        JS_FN("startWithTarget", js_cocos2dx_engine_CCShake_startWithTarget, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("stop", js_cocos2dx_engine_CCShake_stop, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("update", js_cocos2dx_engine_CCShake_update, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("initWithDuration", js_cocos2dx_engine_CCShake_initWithDuration, 3, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    static JSFunctionSpec st_funcs[] = {
        JS_FN("actionWithDuration", js_cocos2dx_engine_CCShake_actionWithDuration, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    jsb_CCShake_prototype = JS_InitClass(
        cx, global,
        JS::NullPtr(),
        jsb_CCShake_class,
        js_cocos2dx_engine_CCShake_constructor, 0, // constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);

    JS::RootedObject proto(cx, jsb_CCShake_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "CCShake"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::FalseHandleValue);
    // add the proto and JSClass to the type->js info hash table
    jsb_register_class<CCShake>(cx, jsb_CCShake_class, proto, JS::NullPtr());
}

void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj) {
    // Get the ns
    JS::RootedObject ns(cx);
    get_or_create_js_obj(cx, obj, "engine", &ns);

    js_register_cocos2dx_engine_HandlerManager(cx, ns);
    js_register_cocos2dx_engine_Handler(cx, ns);
    js_register_cocos2dx_engine_InPacket(cx, ns);
    js_register_cocos2dx_engine_WP8Bridgle(cx, ns);
    js_register_cocos2dx_engine_TimeProgressEffect(cx, ns);
    js_register_cocos2dx_engine_MoveCircle(cx, ns);
    js_register_cocos2dx_engine_OutPacket(cx, ns);
    js_register_cocos2dx_engine_CCShake(cx, ns);
    js_register_cocos2dx_engine_HttpMultipart(cx, ns);
    js_register_cocos2dx_engine_sPackage(cx, ns);
    js_register_cocos2dx_engine_UIAvatar(cx, ns);
    js_register_cocos2dx_engine_CircleMove(cx, ns);
    js_register_cocos2dx_engine_AsyncDownloader(cx, ns);
    js_register_cocos2dx_engine_GsnClient(cx, ns);
}

