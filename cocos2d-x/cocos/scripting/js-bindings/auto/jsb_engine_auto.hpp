#include "base/ccConfig.h"
#ifndef __cocos2dx_engine_h__
#define __cocos2dx_engine_h__

#include "jsapi.h"
#include "jsfriendapi.h"

extern JSClass  *jsb_AsyncDownloader_class;
extern JSObject *jsb_AsyncDownloader_prototype;

bool js_cocos2dx_engine_AsyncDownloader_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_cocos2dx_engine_AsyncDownloader_finalize(JSContext *cx, JSObject *obj);
void js_register_cocos2dx_engine_AsyncDownloader(JSContext *cx, JS::HandleObject global);
void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj);
bool js_cocos2dx_engine_AsyncDownloader_startDownload(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_AsyncDownloader_initDownload(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_AsyncDownloader_progressDownloaded(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_AsyncDownloader_setCallback(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_AsyncDownloader_httpCallback(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_AsyncDownloader_create(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_AsyncDownloader_AsyncDownloader(JSContext *cx, uint32_t argc, jsval *vp);

extern JSClass  *jsb_HttpMultipart_class;
extern JSObject *jsb_HttpMultipart_prototype;

bool js_cocos2dx_engine_HttpMultipart_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_cocos2dx_engine_HttpMultipart_finalize(JSContext *cx, JSObject *obj);
void js_register_cocos2dx_engine_HttpMultipart(JSContext *cx, JS::HandleObject global);
void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj);
bool js_cocos2dx_engine_HttpMultipart_executeAsyncTask(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_HttpMultipart_addFilePart(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_HttpMultipart_addImage(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_HttpMultipart_setCallback(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_HttpMultipart_addFormPart(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_HttpMultipart_create(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_HttpMultipart_HttpMultipart(JSContext *cx, uint32_t argc, jsval *vp);

extern JSClass  *jsb_sPackage_class;
extern JSObject *jsb_sPackage_prototype;

bool js_cocos2dx_engine_sPackage_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_cocos2dx_engine_sPackage_finalize(JSContext *cx, JSObject *obj);
void js_register_cocos2dx_engine_sPackage(JSContext *cx, JS::HandleObject global);
void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj);
bool js_cocos2dx_engine_sPackage_clean(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_sPackage_sPackage(JSContext *cx, uint32_t argc, jsval *vp);

extern JSClass  *jsb_InPacket_class;
extern JSObject *jsb_InPacket_prototype;

bool js_cocos2dx_engine_InPacket_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_cocos2dx_engine_InPacket_finalize(JSContext *cx, JSObject *obj);
void js_register_cocos2dx_engine_InPacket(JSContext *cx, JS::HandleObject global);
void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj);
bool js_cocos2dx_engine_InPacket_getDouble(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_getCmdId(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_getByte(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_getError(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_getLong(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_getBool(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_getString(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_getInt(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_init(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_getControllerId(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_clean(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_getUnsignedShort(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_getShort(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_getCharArray(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_getBytes(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_InPacket_InPacket(JSContext *cx, uint32_t argc, jsval *vp);

extern JSClass  *jsb_OutPacket_class;
extern JSObject *jsb_OutPacket_prototype;

bool js_cocos2dx_engine_OutPacket_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_cocos2dx_engine_OutPacket_finalize(JSContext *cx, JSObject *obj);
void js_register_cocos2dx_engine_OutPacket(JSContext *cx, JS::HandleObject global);
void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj);
bool js_cocos2dx_engine_OutPacket_reset(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_putInt(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_setCmdId(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_packHeader(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_setControllerId(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_putUnsignedShort(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_putShort(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_putString(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_initData(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_putBytes(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_putLong(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_clean(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_updateSize(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_getData(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_putByteArray(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_putByte(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_updateUnsignedShortAtPos(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_OutPacket_OutPacket(JSContext *cx, uint32_t argc, jsval *vp);

extern JSClass  *jsb_GsnClient_class;
extern JSObject *jsb_GsnClient_prototype;

bool js_cocos2dx_engine_GsnClient_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_cocos2dx_engine_GsnClient_finalize(JSContext *cx, JSObject *obj);
void js_register_cocos2dx_engine_GsnClient(JSContext *cx, JS::HandleObject global);
void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj);
bool js_cocos2dx_engine_GsnClient_onSubThreadStarted(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_onSubThreadEnded(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_setTimeoutForRead(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_disconnect(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_setReceiveDataListener(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_isDoConnection(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_initThread(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_setTimeoutForConnect(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_createSocket(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_onSubThreadLoop(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_getTimeoutForRead(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_setFinishConnectListener(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_onUIThreadReceiveMessage(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_setDisconnectListener(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_setListener(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_getTimeoutForConnect(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_connect(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_reconnect(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_clearQueue(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_send(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_destroyInstance(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_create(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_getInstance(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_GsnClient_GsnClient(JSContext *cx, uint32_t argc, jsval *vp);

extern JSClass  *jsb_Handler_class;
extern JSObject *jsb_Handler_prototype;

bool js_cocos2dx_engine_Handler_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_cocos2dx_engine_Handler_finalize(JSContext *cx, JSObject *obj);
void js_register_cocos2dx_engine_Handler(JSContext *cx, JS::HandleObject global);
void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj);
bool js_cocos2dx_engine_Handler_setTimeOut(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_Handler_setID(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_Handler_set(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_Handler_stop(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_Handler_Handler(JSContext *cx, uint32_t argc, jsval *vp);

extern JSClass  *jsb_HandlerManager_class;
extern JSObject *jsb_HandlerManager_prototype;

bool js_cocos2dx_engine_HandlerManager_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_cocos2dx_engine_HandlerManager_finalize(JSContext *cx, JSObject *obj);
void js_register_cocos2dx_engine_HandlerManager(JSContext *cx, JS::HandleObject global);
void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj);
bool js_cocos2dx_engine_HandlerManager_addHandler(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_HandlerManager_update(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_HandlerManager_forceRemoveHandler(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_HandlerManager_stopHandler(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_HandlerManager_exitIOS(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_HandlerManager_getHandler(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_HandlerManager_destroyInstance(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_HandlerManager_getInstance(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_HandlerManager_HandlerManager(JSContext *cx, uint32_t argc, jsval *vp);

extern JSClass  *jsb_WP8Bridgle_class;
extern JSObject *jsb_WP8Bridgle_prototype;

bool js_cocos2dx_engine_WP8Bridgle_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_cocos2dx_engine_WP8Bridgle_finalize(JSContext *cx, JSObject *obj);
void js_register_cocos2dx_engine_WP8Bridgle(JSContext *cx, JS::HandleObject global);
void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj);
bool js_cocos2dx_engine_WP8Bridgle_openURL(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_getIMEI(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_submitLoginZalo(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_showNoNetwork(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_logoutZALO(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_sendLogLogin(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_showUpdate2(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_showUpdate1(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_loginFB(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_getZALOFriends(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_loginZALO(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_checkNetworkAvaiable(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_sendMessage(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_logoutFB(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_payment(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_showMaintain(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_WP8Bridgle_openURLUpdate(JSContext *cx, uint32_t argc, jsval *vp);

extern JSClass  *jsb_UIAvatar_class;
extern JSObject *jsb_UIAvatar_prototype;

bool js_cocos2dx_engine_UIAvatar_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_cocos2dx_engine_UIAvatar_finalize(JSContext *cx, JSObject *obj);
void js_register_cocos2dx_engine_UIAvatar(JSContext *cx, JS::HandleObject global);
void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj);
bool js_cocos2dx_engine_UIAvatar_setTexture(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_callbackDownload(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_setImage(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_setDefaultImage(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_asyncExecuteWithUrl(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_setOpacity(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_getImageSize(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_asyncExecute(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_setIntegerForKey(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_getAppversionString(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_create(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_getStringForKey(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_setStringForKey(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_createWithMask(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_getIntegerForKey(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_UIAvatar_UIAvatar(JSContext *cx, uint32_t argc, jsval *vp);

extern JSClass  *jsb_CircleMove_class;
extern JSObject *jsb_CircleMove_prototype;

bool js_cocos2dx_engine_CircleMove_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_cocos2dx_engine_CircleMove_finalize(JSContext *cx, JSObject *obj);
void js_register_cocos2dx_engine_CircleMove(JSContext *cx, JS::HandleObject global);
void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj);
bool js_cocos2dx_engine_CircleMove_create(JSContext *cx, uint32_t argc, jsval *vp);

extern JSClass  *jsb_TimeProgressEffect_class;
extern JSObject *jsb_TimeProgressEffect_prototype;

bool js_cocos2dx_engine_TimeProgressEffect_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_cocos2dx_engine_TimeProgressEffect_finalize(JSContext *cx, JSObject *obj);
void js_register_cocos2dx_engine_TimeProgressEffect(JSContext *cx, JS::HandleObject global);
void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj);
bool js_cocos2dx_engine_TimeProgressEffect_setNen(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_TimeProgressEffect_update(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_TimeProgressEffect_create(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_TimeProgressEffect_TimeProgressEffect(JSContext *cx, uint32_t argc, jsval *vp);

extern JSClass  *jsb_MoveCircle_class;
extern JSObject *jsb_MoveCircle_prototype;

bool js_cocos2dx_engine_MoveCircle_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_cocos2dx_engine_MoveCircle_finalize(JSContext *cx, JSObject *obj);
void js_register_cocos2dx_engine_MoveCircle(JSContext *cx, JS::HandleObject global);
void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj);
bool js_cocos2dx_engine_MoveCircle_startWithTarget(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_MoveCircle_update(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_MoveCircle_create(JSContext *cx, uint32_t argc, jsval *vp);

extern JSClass  *jsb_CCShake_class;
extern JSObject *jsb_CCShake_prototype;

bool js_cocos2dx_engine_CCShake_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_cocos2dx_engine_CCShake_finalize(JSContext *cx, JSObject *obj);
void js_register_cocos2dx_engine_CCShake(JSContext *cx, JS::HandleObject global);
void register_all_cocos2dx_engine(JSContext* cx, JS::HandleObject obj);
bool js_cocos2dx_engine_CCShake_startWithTarget(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_CCShake_stop(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_CCShake_update(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_CCShake_initWithDuration(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_CCShake_actionWithDuration(JSContext *cx, uint32_t argc, jsval *vp);
bool js_cocos2dx_engine_CCShake_CCShake(JSContext *cx, uint32_t argc, jsval *vp);

#endif // __cocos2dx_engine_h__
