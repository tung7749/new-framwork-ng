/****************************************************************************
 Copyright (c) 2011-2012 cocos2d-x.org
 Copyright (c) 2013-2014 Chukong Technologies Inc.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/
/**
 * <p>cc.LoaderScene is a scene that you can load it when you loading files</p>
 * <p>cc.LoaderScene can present thedownload progress </p>
 * @class
 * @extends cc.Scene
 * @example
 * var lc = new cc.LoaderScene();
 */
cc.LoaderScene = cc.Scene.extend({
    _interval: null,
    _label: null,
    _logo: null,
    _className: "LoaderScene",
    cb: null,
    target: null,
    resLoader: ["res/LoadingUI/LogoG.json", "res/LoadingUI/LogoG.atlas", "res/LoadingUI/LogoG.png", "res/LoadingUI/light.png", "res/LoadingUI/bar.png"],
    _spLogo: null,
    /**
     * Contructor of cc.LoaderScene
     * @returns {boolean}
     */
    init: function () {
        var self = this;

        //logo
        var logoWidth = 160;
        var logoHeight = 200;

        // bg
        var bgLayer = self._bgLayer = new cc.LayerColor(cc.color(32, 32, 32, 255));
        self.addChild(bgLayer, 0);

        //image move to CCSceneFile.js
        var fontSize = 35, lblHeight = -logoHeight / 2 + 100;


        if (cc._bgImage) {
            //loading bg
            cc.loader.loadImg(cc._bgImage, {isCrossOrigin: false}, function (err, img) {
                logoWidth = img.width;
                logoHeight = img.height;
                self._initStage(img, cc.visibleRect.center);


                fontSize = 20;
                // lblHeight = -logoHeight / 2 - 10;
                lblHeight = 100;

                var label = self._label = new cc.LabelTTF("Loading... 0%", "Roboto-Bold", fontSize);
                label.setPosition(cc.pAdd(cc.visibleRect.center, cc.p(0, - lblHeight)));
                label.setColor(cc.color(255, 255, 255));
                bgLayer.addChild(self._label, 10);


                if (this.resLoader) {

                    console.error(this);
                    cc.loader.load(self.resLoader, function () {
                        },
                        function () {
                            this._initStageNew(this.resLoader, cc.pAdd(cc.visibleRect.center, cc.p(0, 130 - lblHeight)));
                            this._initProgressBar();
                        }.bind(this)
                    );
                }
            }.bind(this));
        }


        // if(cc._loaderImage){
        //     //loading logo
        //     cc.loader.loadImg(cc._loaderImage, {isCrossOrigin : false }, function(err, img){
        //         logoWidth = img.width;
        //         logoHeight = img.height;
        //         self._initStage(img, cc.visibleRect.center);
        //     });
        //     fontSize = 14;
        //     lblHeight = -logoHeight / 2 - 10;
        // }
        //loading percent
        return true;
    },


    _initProgressBar: function () {
        var mBar = new cc.ProgressTimer(new cc.Sprite("res/LoadingUI/bar.png"));
        mBar.setType(cc.ProgressTimer.TYPE_BAR);
        mBar.setBarChangeRate(cc.p(1.0, 0.0));
        // mBar.setPercentage(100);
        mBar.setMidpoint(cc.p(0.0, 0.5));
        mBar.setNumber = function (number) {
            if (number <= 0) {
                mBar.setPercentage(0.0);
            }
            else {
                if (number < 15) {
                    var w = 11 + (number - 1) * 9;
                    mBar.setPercentage(100.0 * w / mBar.width);
                }
                else {
                    mBar.setPercentage(100.0);
                }
            }
        };

        this._bgLayer.addChild(mBar, 10);
        mBar.setPosition(cc.pAdd(cc.visibleRect.center, cc.p(0, -130)));
        this.mBar = mBar

        var light = new cc.Sprite("res/LoadingUI/light.png");
        light.setPosition(cc.p(mBar.x - mBar.width/2,mBar.y));
        this._bgLayer.addChild(light, 10);
        this.light = light;

        return mBar;
    },

    _initStageNew: function (res, centerPos) {
        this._spLogo = new sp.SkeletonAnimation(res[0], res[1]);
        this._spLogo.setPosition(cc.p(0, 0));

        // this._spLogo.setMix("loading_in","loading_loop",1);
        this._spLogo.setAnimation(0, 'animation', false);
        this._spLogo.setPosition(cc.p(centerPos.x, centerPos.y));
        this._spLogo.setScale(0.7);
        this.addChild(this._spLogo);
    },

    _initStage: function (img, centerPos) {
        var self = this;
        var texture2d = self._texture2d = new cc.Texture2D();
        texture2d.initWithElement(img);
        texture2d.handleLoadedTexture();
        var logo = self._logo = new cc.Sprite(texture2d);
        logo.setScale(cc.contentScaleFactor());
        logo.x = centerPos.x;
        logo.y = centerPos.y;
        self._bgLayer.addChild(logo, 10);
    },
    /**
     * custom onEnter
     */
    onEnter: function () {
        var self = this;
        cc.Node.prototype.onEnter.call(self);
        self.schedule(self._startLoading, 0.3);
    },
    /**
     * custom onExit
     */
    onExit: function () {
        cc.Node.prototype.onExit.call(this);
        var tmpStr = "Loading... 0%";
        this._label.setString(tmpStr);
    },

    /**
     * init with resources
     * @param {Array} resources
     * @param {Function|String} cb
     * @param {Object} target
     */
    initWithResources: function (resources, cb, target) {
        if (cc.isString(resources))
            resources = [resources];
        this.resources = resources || [];
        this.cb = cb;
        this.target = target;
    },

    _startLoading: function () {
        var self = this;
        self.unschedule(self._startLoading);
        var res = self.resources;
        cc.loader.load(res,
            function (result, count, loadedCount) {
                var percent = (loadedCount / count * 100) | 0;
                percent = Math.min(percent, 100);
                self._label && self._label.setString("Loading... " + percent + "%");

                if(this.mBar){
                    var mBar = this.mBar;
                    this.mBar.stopAllActions();
                    // this.mBar.runAction(cc.progressTo(0.3, parseInt(percent)));

                    mBar.setPercentage(percent);

                    this.light.stopAllActions();
                    var newPosition = cc.p(mBar.x - mBar.width/2 + mBar.width * percent/100,mBar.y);
                    // this.light.runAction(cc.moveTo(0.3,newPosition));
                    this.light.setPosition(newPosition);
                }

            }.bind(this), function () {

                self._label && self._label.setString("Loading... " + 100 + "%");
                setTimeout(function () {
                    if (self.cb)
                        self.cb.call(self.target);
                },700)
            });
    },

    _updateTransform: function () {
        this._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
        this._bgLayer._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
        this._label._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
        this._logo && this._logo._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
    }
});
/**
 * <p>cc.LoaderScene.preload can present a loaderScene with download progress.</p>
 * <p>when all the resource are downloaded it will invoke call function</p>
 * @param resources
 * @param cb
 * @param target
 * @returns {cc.LoaderScene|*}
 * @example
 * //Example
 * cc.LoaderScene.preload(g_resources, function () {
        cc.director.runScene(new HelloWorldScene());
    }, this);
 */
cc.LoaderScene.preload = function (resources, cb, target) {
    var _cc = cc;
    if (!_cc.loaderScene) {
        _cc.loaderScene = new cc.LoaderScene();
        _cc.loaderScene.init();
        cc.eventManager.addCustomListener(cc.Director.EVENT_PROJECTION_CHANGED, function () {
            _cc.loaderScene._updateTransform();
        });
    }
    _cc.loaderScene.initWithResources(resources, cb, target);

    cc.director.runScene(_cc.loaderScene);
    return _cc.loaderScene;
};