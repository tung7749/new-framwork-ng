//
//  PlatformWrapper.h
//  Farmery
//
//  Created by KienVN on 2/17/14.
//
//

#ifndef __PlatformWrapper__
#define __PlatformWrapper__
#include "string"

class PlatformWrapper
{
public:
    static void sendSMS(const char* phone, const char* sms);
    static void openWebview(const char* url);

    static void showNotification(const char* content, long _time );
    static void cancelAllNotification();
    static void openLink(const char* link);
    static void openFacebookLink(const char* httpLink, const char* appLink);
    static void openSession();
    static void openUpdate(const char* link);
    static std::string getIMEI();
    static void openPortal();
    static void goStore();
    static void makingCall(std::string phoneNumber);
};

#endif /* defined(__Farmery__PlatformWrapper__) */
