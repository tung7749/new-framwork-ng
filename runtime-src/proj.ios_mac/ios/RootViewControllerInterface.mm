//
//  RootViewControllerInterface.m
//  cocosViewController
//
//  Created by toni on 25/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "RootViewControllerInterface.h"
#import <string.h>
#import <string>

using namespace std;

@implementation RootViewControllerInterface

@synthesize rootViewController;

#pragma mark -
#pragma mark Singleton Variables
static RootViewControllerInterface *rootViewControllerInterfaceSingletonDelegate = nil;

#pragma mark -
#pragma mark Singleton Methods
+ (RootViewControllerInterface *) rootViewControllerInterfaceSharedInstance {
	@synchronized(self) {
		if (rootViewControllerInterfaceSingletonDelegate == nil) {
			[[self alloc] init]; // assignment not done here
		}
	}
	return rootViewControllerInterfaceSingletonDelegate;
}

+ (id)allocWithZone:(NSZone *)zone {
	@synchronized(self) {
		if (rootViewControllerInterfaceSingletonDelegate == nil) {
			rootViewControllerInterfaceSingletonDelegate = [super allocWithZone:zone];
			// assignment and return on first allocation
			return rootViewControllerInterfaceSingletonDelegate;
		}
	}
	// on subsequent allocation attempts return nil
	return nil;
}

- (id)copyWithZone:(NSZone *)zone {
	return self;
}

- (id)retain {
	return self;
}

- (unsigned)retainCount {
	return UINT_MAX;  // denotes an object that cannot be released
}

- (void)release {
	//do nothing
    
}

- (id)autorelease {
	return self;
}

-(void) presentModalViewController:(UIViewController*)controller animated:(BOOL)animated {
    [rootViewController presentModalViewController:controller animated:animated];
}


- (void) sendSMS:(NSString *)phoneNumber message:(NSString *)message
{
    if(![MFMessageComposeViewController canSendText]) {
        std::string failMessage = "MESSAGE_DEVICE_NOT_SUPPORT_SMS";
        NSString *errorMessage = [NSString stringWithUTF8String:failMessage.c_str()];
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSArray *recipents = @[phoneNumber];
    
    MFMessageComposeViewController *controller = [[[MFMessageComposeViewController alloc] init] autorelease];
    if ([MFMessageComposeViewController canSendText])
    {
        controller.body = message;
       // controller.recipients = [NSArray arrayWithObjects:phoneNumber, nil];
       // controller.messageComposeDelegate = self;
        [controller setRecipients:recipents];
        [controller setBody:message];
        controller.messageComposeDelegate = self;
        [rootViewController presentModalViewController:controller animated:YES];
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}


-(void)makeToast:(NSString *)message
{
    [rootViewController makeToast:message];
}

- (void) messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
        {
            NSLog(@"Cancelled.");
            break;
        }
        case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Send SMS failed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [alert release];
            
            break;
        }
            
        case MessageComposeResultSent:
        {
            break;
        }
    }
    [rootViewController dismissModalViewControllerAnimated:YES];
}

-(void)openWebiew:(NSString *)urlString
{
    
    webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, rootViewController.view.frame.size.width, rootViewController.view.frame.size.height)];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [webView loadRequest:urlRequest];
    
    [rootViewController.view addSubview:webView];
    
    
    
    btnClose = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnClose setTitle:@"" forState:UIControlStateNormal];
    btnClose.frame = CGRectMake(0, 0, 90, 40);
    [btnClose sizeToFit];
    btnClose.center = CGPointMake(rootViewController.view.frame.size.width-25,25);
    [btnClose setBackgroundImage:[UIImage imageNamed:@"btnBack_s.png"] forState:UIControlStateNormal];
    // Add an action in current code file (i.e. target)
    [btnClose addTarget:self action:@selector(buttonPressed:)
     forControlEvents:UIControlEventTouchUpInside];
    
    [rootViewController.view addSubview:btnClose];
    
}
-(void)buttonPressed:(UIButton *)button {
    [webView removeFromSuperview];
    [btnClose removeFromSuperview];
    NSLog(@"Button Pressed");
}

-(void)hideWebview
{
    [rootViewController hideWebview];
}

@end
