//
//  IOSConnection.cpp
//  ChineseChess
//
//  Created by Dong Truong Quang on 3/5/14.
//
//

#import "IOSConnection.h"
#import "Reachability.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "EmailViewController.h"
#import "PlatformWrapper.h"
#include "RootViewControllerInterface.h"
#import <UIKit/UIKit.h>
#import "ObjCBridgle.h"
#include <string>

using namespace std;
void IOSConnection::sendEmail(std::string address, std::string subject, std::string message)
{
    NSString *nsAddress = [NSString stringWithUTF8String:address.c_str()];
    NSString *nsSubject = [NSString stringWithUTF8String:subject.c_str()];
    NSString *nsMessage = [NSString stringWithUTF8String:message.c_str()];
    [[EmailViewController instance] sendEmail:nsAddress subject:nsSubject message:nsMessage];
    
}

void IOSConnection::sendSMS(const char* phone, const char* sms)
{
    PlatformWrapper::sendSMS(phone,sms);
}

void IOSConnection::openURL(std::string url)
{
    PlatformWrapper::openWebview(url.c_str());
}
void IOSConnection::openURLUpdate(std::string url)
{
    NSString* nsURL = [NSString stringWithUTF8String:("" + url).c_str()];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:nsURL]];
}


void IOSConnection::makingCall(std::string phoneNumber)
{
    PlatformWrapper:makingCall(phoneNumber);
}

std::string IOSConnection::getIMEI()
{
	NSUUID *identify = [[UIDevice currentDevice] identifierForVendor];
    NSString *str = [identify UUIDString];
    return [str UTF8String];
}

std::string IOSConnection::getRefer()
{
    NSString *ret = @"";
    return [ret UTF8String];
}

std::string IOSConnection::getVersionString()
{
    NSString *version = @"0";
    
    @try {
        version =[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    }
    @catch (NSException *exception) {
        version = @"0";
    }
    
    NSString *build = @"0";
    
    @try {
        build =[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    }
    @catch (NSException *exception) {
        build = @"0";
    }
    
    NSString *ret = [NSString stringWithFormat:@"%@.%@",version,build];
    
    return [ret UTF8String];

}

std::string IOSConnection::getStringResourceByName(string name)
{
    NSString * key = [NSString stringWithUTF8String:name.c_str()];
    NSString *build = @"";
    @try {
        build =[[[NSBundle mainBundle] infoDictionary] objectForKey:key];
    }
    @catch (NSException *exception) {
        build = key;
    }
    return [build UTF8String];
}

std::string IOSConnection::getVersionCode()
{
    NSString *build = @"0";
    @try {
        build =[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    }
    @catch (NSException *exception) {
        build = @"0";
    }
    return [build UTF8String];
}

int IOSConnection::checkNetwork()
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"There IS NO internet connection");
        return 0;
    } else {
        NSLog(@"There IS internet connection");
        return 1;
    }
    return 1;
}

void IOSConnection::callZingPlayPortal()
{
    //[[ZingPlay shared] openSessionGame];
}
std::string IOSConnection::getDeviceModel()
{
	  return "iOS";
}

void IOSConnection::saveData(std::string key, std::string value)
{
    NSString *savingValue = [NSString stringWithUTF8String:value.c_str()];
    NSString *savingKey = [NSString stringWithUTF8String:key.c_str()];
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if(standardUserDefaults)
    {
        [standardUserDefaults setObject:savingValue forKey:savingKey];
        [standardUserDefaults synchronize];
    }
    
}

bool IOSConnection::networkReachable()
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"There IS NO internet connection");
        return false;
    } else {
        NSLog(@"There IS internet connection");
        return true;
    }
}

void IOSConnection::goStore()
{
    //[[ZingPlay shared] goStore];
}

void IOSConnection::openPortal()
{
    //
    //[[ZingPlay shared] closeGame];
}

void IOSConnection::deleteData(std::string key )
{
    NSString *deleteKey = [NSString stringWithUTF8String:key.c_str()];
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if(standardUserDefaults)
    {
        [standardUserDefaults removeObjectForKey:deleteKey];
        [standardUserDefaults synchronize];
    }
}

void IOSConnection::clearZingPlaySessionCache(){
    //[[ZingPlay shared] logout];
}

std::string IOSConnection::getData(std::string key)
{
    NSString* savingKey = [NSString stringWithCString:key.c_str() encoding: [NSString defaultCStringEncoding]];
    NSString* savingValue = [[NSUserDefaults standardUserDefaults]stringForKey:savingKey];
    if(savingValue != NULL)
        return [savingValue UTF8String];
    else
        return "";
}



void IOSConnection::onZingPlayPortalResponse(int error, std::string sessionKey, std::string accessToken, std::string openID, int social)
{
//    if (error == 1) {
//        // Call ZingPlay failed
//    }
//    else
//    {
//        // Call ZingPlay sucessful
//        CCLOG(" ZingPlay SessionKey : %s",sessionKey.c_str());
//        CCLOG(" ZingPlay AccessToken : %s",accessToken.c_str());
//        CCLOG(" ZingPlay OpenID : %s",openID.c_str());
//        CCLOG(" ZingPlay Social : %d",social);
//    }
//    ISocialMgr::onZingPlayPortalResponse(error, sessionKey, accessToken, openID, social);
}

string IOSConnection::getVersion()
{
    return [[[NSBundle mainBundle].infoDictionary objectForKey:@"VersionApplication"] UTF8String];
}

string IOSConnection::getZaloAppID()
{
    return [[[NSBundle mainBundle].infoDictionary objectForKey:@"ZaloAppID"] UTF8String];
}

string IOSConnection::getZaloAppSecret()
{
    return [[[NSBundle mainBundle].infoDictionary objectForKey:@"ZaloAppSecret"] UTF8String];
}
string IOSConnection::getAppVersion()
{
    return [[[NSBundle mainBundle].infoDictionary objectForKey:@"AppVersion"] UTF8String];
}
string IOSConnection::getZaloID()
{
    return "";
}

string IOSConnection::getZaloOAucode()
{
    return "";
}

void IOSConnection::showNoNetwork()
{
    [ObjCBridgle showNoNetwork];
}

bool IOSConnection::checkInstallApp(const char* link)
{
    
    NSString *text = [[[NSString alloc]initWithUTF8String: link] autorelease];
    
    NSString *ourPath = [NSString stringWithFormat:@"%@://",text];
    UIApplication *ourApplication = [UIApplication sharedApplication];
    NSURL *ourURL = [NSURL URLWithString:ourPath];
    return [ourApplication canOpenURL:ourURL];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

void IOSConnection::sendLogLogin(const char *uID, const char *typeLogin, const char *param3, const char *zName)
{
    NSLog(@"send login tracker");
    NSString *_uID = [[[NSString alloc]initWithUTF8String: uID] autorelease];
    NSString *_typeLogin = [[[NSString alloc]initWithUTF8String: typeLogin] autorelease];
    NSString *_pram3 = [[[NSString alloc]initWithUTF8String: param3] autorelease];
    NSString *_zName = [[[NSString alloc]initWithUTF8String: zName] autorelease];
    //[[GSNTracker shared] login:_uID accountType:_typeLogin openAccount:_pram3 zingName:_zName];
}

void IOSConnection::sendLogLoginZalo(const char *uID, const char *typeLogin, const char *source, const char *refer)
{
    NSString *acountIdData = [[NSString alloc] initWithUTF8String:uID];
    NSString *acountTypeData = [[NSString alloc] initWithUTF8String:typeLogin];
    NSString *distributionSourceData = [[NSString alloc] initWithUTF8String:source];
    NSString *referData = [[NSString alloc] initWithUTF8String:refer];
    
//    ZOLoginType type;
//    if(std::strcmp(typeLogin, "facebook") == 0)
//    {
//        type = ZOLoginTypeFacebook;
//    }
//    else if(std::strcmp(typeLogin, "google") == 0)
//    {
//        type = ZOLoginTypeGooglePlus;
//    }
//    else if(std::strcmp(typeLogin, "zalo") == 0)
//    {
//        type = ZOLoginTypeZalo;
//    }
//    else{
//        type = ZOLoginTypeZingMe;
//    }
//    
//    [[ZaloSDK sharedInstance] submitAppUser:acountIdData loginChannel:type distributionsource:distributionSourceData appUtmSource:referData callback:^(ZOResponseObject *response) {
//        
//    }];
}

void IOSConnection::showYesNoDialog(string title, string message, string ok, string cancel, int type)
{
    [ObjCBridgle setType:type];
    UIAlertView *nonetwork = [[UIAlertView alloc] initWithTitle:[NSString stringWithUTF8String:title.c_str()] message:[NSString stringWithUTF8String:message.c_str()] delegate:[ObjCBridgle alloc] cancelButtonTitle:[NSString stringWithUTF8String:ok.c_str()] otherButtonTitles:[NSString stringWithUTF8String:cancel.c_str()], nil];
    [nonetwork show];
}
