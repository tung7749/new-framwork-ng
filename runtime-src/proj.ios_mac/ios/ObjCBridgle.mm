//
//  ObjCBridgle.m
//  ZPSamLoc
//
//  Created by Hoang Nguyen on 11/19/15.
//
//

#import "ObjCBridgle.h"
#import "IOSConnection.h"
#import "Reachability.h"
#include "engine/Handler.h"
#import "RootViewController.h"
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

static int type;
@implementation ObjCBridgle

+(void) callNumberPhone:(NSString *) phone{
    IOSConnection::makingCall([phone UTF8String]);
}

+(void) sendMessage:(NSString*)phone	
            message:(NSString*)message
{
    IOSConnection::sendSMS([phone UTF8String],[message UTF8String]);
}
+(NSString *) getIMEI{
    return [[NSString alloc] initWithUTF8String:IOSConnection::getIMEI().c_str()];
}
+(void) purchase:(NSString*)productID
           uData:(NSString*)uData
{
    [[RootViewController instance] purchaseGame: productID];
}

+(NSString *) getDeviceModel{
    return [[NSString alloc] initWithUTF8String:IOSConnection::getDeviceModel().c_str()];
}

+(NSString *) getOsVersion{
    return [[UIDevice currentDevice] systemVersion];
}
+(NSString *) getDeviceInfo{
    NSString *strIMEI  = [self getIMEI];
    NSString *strDeviceModel = [self getDeviceModel];
    NSString *strOsVersion = [self getOsVersion];
    
    return [[NSString alloc] initWithFormat:@"%@:%@:IOS%@",strIMEI ,strDeviceModel, strOsVersion];
}
+(NSString *) networkAvaiable{
    
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"There IS NO internet connection");
        return @"NO";
    } else {
        NSLog(@"There IS internet connection");
        return @"YES";
    }
}

+(NSString *) getAppVersion{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
    //return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"abcxyzzzzzz"];
}

+(NSString *) getVersionString{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
}

+(void) openURL:(NSString *) url{
    IOSConnection::openURL([url UTF8String]);
}
+(void) setType: (int) _type{
    type = _type;
}

+(void)sendLoginGSN:(NSString*)acount
      acountType:(NSString*)acountType
          openID:(NSString*)openID
           zName:(NSString*)zName;{
    NSLog(@"hichic");
    NSLog(acount);
    NSLog(acountType);
    NSLog(openID);
    NSLog(zName);
    NSLog(@"hihi");
    //[[GSNTracker shared] login:acount accountType:acountType openAccount:openID zingName:zName];
}
+(void) showNoNetwork{
    type = 1;
    UIAlertView *nonetwork = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn cần có kết nối mạng" delegate:[ObjCBridgle alloc] cancelButtonTitle:@"Thử lại" otherButtonTitles:@"Thoát", nil];
    [nonetwork show];
}

+(void) showUpdate1:(NSString*)update{
    type = 3;
    UIAlertView *nonetwork = [[UIAlertView alloc] initWithTitle:@"ZingPlay Sâm có bản cập nhật mới" message:update delegate:[ObjCBridgle alloc] cancelButtonTitle:@"Cập nhật" otherButtonTitles:nil, nil];
    [nonetwork show];
}

+(void) showUpdate2:(NSString*)update{
    type = 3;
    UIAlertView *nonetwork = [[UIAlertView alloc] initWithTitle:@"ZingPlay Sâm có bản cập nhật mới" message:update delegate:[ObjCBridgle alloc] cancelButtonTitle:@"Cập nhật" otherButtonTitles:@"Bỏ qua", nil];
    [nonetwork show];
}

+(void) showMaintain:(NSString*)maintain{
    type = 2;
    UIAlertView *nonetwork = [[UIAlertView alloc] initWithTitle:@"Thông báo bảo trì" message:maintain delegate:[ObjCBridgle alloc] cancelButtonTitle:@"Thoát" otherButtonTitles:nil, nil];
    [nonetwork show];
}
    
    
+(NSString*)getCountryCode{
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    
    // Get carrier name
    NSString *carrierName = [carrier carrierName];
    if (carrierName != nil)
    NSLog(@"Carrier: %@", carrierName);
    
    // Get mobile country code
    NSString *mcc = [carrier mobileCountryCode];
    if (mcc != nil)
    NSLog(@"Mobile Country Code (MCC): %@", mcc);
    
    return mcc;
    
//    // Get mobile network code
//    NSString *mnc = [carrier mobileNetworkCode];
//    
//    if (mnc != nil)
//    NSLog(@"Mobile Network Code (MNC): %@", mnc);
}
    
+(NSString*)getNetworkCode{
        CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
        CTCarrier *carrier = [networkInfo subscriberCellularProvider];
        
        
        // Get carrier name
        NSString *carrierName = [carrier carrierName];
        if (carrierName != nil)
        NSLog(@"Carrier: %@", carrierName);
        
//        // Get mobile country code
//        NSString *mcc = [carrier mobileCountryCode];
//        if (mcc != nil)
//        NSLog(@"Mobile Country Code (MCC): %@", mcc);
    
        // Get mobile network code
        NSString *mnc = [carrier mobileNetworkCode];
        
        if (mnc != nil)
        NSLog(@"Mobile Network Code (MNC): %@", mnc);
    return mnc;
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0)
    {
        if(type == 1)
        {
            NSString *network = [ObjCBridgle networkAvaiable];
            if([network  isEqual: @"NO"])
            {
                [ObjCBridgle showNoNetwork];
            }
            else
            {
                HandlerManager::getInstance()->stopHandler("dialog", "{\"type\":1,\"button\":0}");
            }
        }
        else
        {
//            string res = format("{\"type\":%d,\"button\":0}",type);
//            HandlerManager::getInstance()->stopHandler("dialog", res);
        }
        
    }
    else {
//        string res = format("{\"type\":%d,\"button\":1}",type);
//        HandlerManager::getInstance()->stopHandler("dialog", res);
//        if(type == 1)
//        {
//            HandlerManager::getInstance()->stopHandler("dialog", "{\"type\":1,\"button\":1}");
//        }
//        else if(type == 2)
//        {
//            HandlerManager::getInstance()->stopHandler("dialog", "{\"type\":2,\"button\":1}");
//        }
//        else if(type == 3)
//        {
//            HandlerManager::getInstance()->stopHandler("dialog", "{\"type\":3,\"button\":1}");
//        }
        
    }
}

@end
