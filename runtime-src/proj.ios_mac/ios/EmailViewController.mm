//
//  EmailViewController.cpp
//  ChineseChess
//
//  Created by Dong Truong Quang on 3/14/14.
//
//

#import "EmailViewController.h"

@implementation EmailViewController

@synthesize rootViewController;

#pragma mark -
#pragma mark Singleton Methods
+ (EmailViewController *) instance {
    static EmailViewController* sharedController = nil;
	@synchronized(self) {
		if (sharedController == nil) {
			sharedController = [[self alloc] init]; // assignment not done here
		}
	}
	return sharedController;
}

-(void) presentViewController:(UIViewController*)controller animated:(BOOL)animated {
    [rootViewController presentViewController:controller animated:animated completion:NULL];
}

-(void)makeToast:(NSString *)message
{
    [rootViewController makeToast:message];
}

-(void) sendEmail:(NSString *)address subject:(NSString*)subject message:(NSString*) message
    {
        if([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
            NSArray *toRecipents = [NSArray arrayWithObject:address];
            [controller setToRecipients:toRecipents];
            [controller setSubject:subject];
            [controller setMessageBody:message isHTML:NO];
            [self presentViewController:controller animated:YES];
            
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"ALERT" message:@"This function is not available on your device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
    }

-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch(result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: @", [error localizedDescription]);
            break;
    }
    [rootViewController dismissViewControllerAnimated:YES completion:NULL];
}

@end


