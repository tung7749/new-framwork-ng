//
//  FacebookUtils.m
//  ZPSamLoc
//
//  Created by Hoang Nguyen on 11/16/15.
//
//

#import "FacebookUtils.h"
#import <FacebookSDK/FacebookSDK.h>
#include "engine/Handler.h"
#include "cocos2d.h"
#include <json/writer.h>
#include <json/stringbuffer.h>

USING_NS_CC;
using namespace rapidjson;
@implementation FacebookUtils
+(void)login{
    NSLog(@"Start login facebook");
    [FBSession openActiveSessionWithReadPermissions:@[@"user_friends"]
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session, FBSessionState state, NSError *error) {
         if (state == FBSessionStateClosedLoginFailed) {
             //[[FBSession activeSession] closeAndClearTokenInformation];
             //[FBSession setActiveSession:nil];
             //FBSession* session = [[FBSession alloc] init];
             //[FBSession setActiveSession: session];
             NSLog(@"Login facebook fail!");
             
             NSMutableDictionary *contentDictionary = [[NSMutableDictionary alloc] init];
             [contentDictionary setValue:@"1" forKey:@"error"];
             [contentDictionary setValue:@"" forKey:@"access_token"];
             NSData *data = [NSJSONSerialization dataWithJSONObject:contentDictionary options:NSJSONWritingPrettyPrinted error:nil];
             NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             NSLog(jsonStr);
             
             HandlerManager::getInstance()->stopHandler("login_facebook",[jsonStr UTF8String]);
             
         }
         else if(state == FBSessionStateOpen){
             NSLog(@"Login facebook successfully");
             //NSLog([[[FBSession activeSession] accessTokenData] accessToken]);
             
             NSMutableDictionary *contentDictionary = [[NSMutableDictionary alloc] init];
             [contentDictionary setValue:@"0" forKey:@"error"];
             [contentDictionary setValue:[[[FBSession activeSession] accessTokenData] accessToken] forKey:@"access_token"];
             NSData *data = [NSJSONSerialization dataWithJSONObject:contentDictionary options:NSJSONWritingPrettyPrinted error:nil];
             NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             NSLog(jsonStr);
             
             HandlerManager::getInstance()->stopHandler("login_facebook",[jsonStr UTF8String]);
             
         }
     }];

}

+(void)logout{
    [[FBSession activeSession] closeAndClearTokenInformation];
}
+(void)getFriends{
    
}
+(void)inviteFriends: (const char*) msg{
    
}

+(void)getAppUsers{
    FBRequest* friendsRequest = [FBRequest requestWithGraphPath:@"me/friends?fields=id,name,picture" parameters:nil HTTPMethod:@"GET"];
    [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
                                                  NSDictionary* result,
                                                  NSError *error) {
        rapidjson::Document d;d.SetObject();
        string test = "";
        if (error) {
            rapidjson::Value _error(1);
            d.AddMember("error", _error, d.GetAllocator());
        }
        else
        {
            NSArray* friends = [result objectForKey:@"data"];
            
            
            rapidjson::Value array;
            array.SetArray();
            
            
            for (NSDictionary<NSObject>* fr in friends) {
                rapidjson::Value _tmp;_tmp.SetObject();

//                _tmp.AddMember("id",[[[fr objectForKey:@"id"] description ]UTF8String] , d.GetAllocator());
//                _tmp.AddMember("name",[[[fr objectForKey:@"name"] description ]UTF8String], d.GetAllocator());
//                _tmp.AddMember("picture",[[[[fr objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"] UTF8String], d.GetAllocator());
                
                array.PushBack(_tmp, d.GetAllocator());
            }
            d.AddMember("data", array, d.GetAllocator());
            rapidjson::Value _error(0);
            d.AddMember("error", _error, d.GetAllocator());
            
        }
        rapidjson::StringBuffer buffer;
        rapidjson::Writer<StringBuffer> writer(buffer);
        d.Accept(writer);
        std::string ret = buffer.GetString();
        //CCLOG("friends :%s",ret.c_str());
        HandlerManager::getInstance()->stopHandler("getFriends", ret);
        
    }];
}

+(void)getInvitableFriends{
    FBRequest* friendsRequest = [FBRequest requestWithGraphPath:@"me/invitable_friends?fields=id,name,picture" parameters:nil HTTPMethod:@"GET"];
    [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
                                                  NSDictionary* result,
                                                  NSError *error) {
        rapidjson::Document d;d.SetObject();
        if (error) {
            rapidjson::Value _error(1);
            d.AddMember("error", _error, d.GetAllocator());
        }
        else
        {
            NSArray* friends = [result objectForKey:@"data"];
            rapidjson::Value array;
            array.SetArray();
            
            for (NSDictionary<NSObject>* fr in friends) {
                rapidjson::Value _tmp;_tmp.SetObject();
//                _tmp.AddMember("idfriend",[[[fr objectForKey:@"id"] description ]UTF8String] , d.GetAllocator());
//                _tmp.AddMember("id",[[[fr objectForKey:@"name"] description ]UTF8String],d.GetAllocator());
//                _tmp.AddMember("name",[[[fr objectForKey:@"name"] description ]UTF8String], d.GetAllocator());
//                _tmp.AddMember("picture",[[[[fr objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"] UTF8String], d.GetAllocator());
                
                array.PushBack(_tmp, d.GetAllocator());
            }
            d.AddMember("data", array, d.GetAllocator());
            rapidjson::Value _error(0);
            d.AddMember("error", _error, d.GetAllocator());
            
        }
        rapidjson::StringBuffer buffer;
        rapidjson::Writer<StringBuffer> writer(buffer);
        d.Accept(writer);
        std::string ret = buffer.GetString();
        //CCLOG("friends :%s",ret.c_str());
        HandlerManager::getInstance()->stopHandler("getInvitableFriends", ret.c_str());
        
    }];
}

+(void)sharePhoto:(NSString*) photo
{
    NSLog(photo);
    /* Code v3.x*/
    
    if ([FBDialogs canPresentShareDialogWithPhotos]) {
        UIImage *image = [[UIImage alloc] initWithContentsOfFile:photo];
        FBPhotoParams *params = [[FBPhotoParams alloc] init];
        params.photos = @[image];
        
        //        [FBDialogs presentMessageDialogWithPhotoParams:params clientState:nil handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
        //
        //        }];
        
        //        [FBWebDialogs presentFeedDialogModallyWithSession:[FBSession activeSession] parameters:nil handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
        //
        //        }];
        
        [FBDialogs presentShareDialogWithPhotoParams:params clientState:nil handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
            
            if (error) {
                rapidjson::Document d;d.SetObject();
                rapidjson::Value _error(2);
                 d.AddMember("error", _error, d.GetAllocator());
                rapidjson::StringBuffer buffer;
                rapidjson::Writer<StringBuffer> writer(buffer);
                d.Accept(writer);
                std::string ret = buffer.GetString();
                HandlerManager::getInstance()->stopHandler("share_fb", ret.c_str());
            }
            else
            {
                NSLog([results description]);
                NSString *_ret = [results objectForKey:@"completionGesture"];
                string ret = _ret == nil?"":[[[results objectForKey:@"completionGesture"] description] UTF8String];
                if ((_ret ==nil) ||ret.compare("cancel") == 0) {
                    
                    NSLog(@"cacel");
                    rapidjson::Document d;d.SetObject();
                    rapidjson::Value _error(1);
                    d.AddMember("error", _error, d.GetAllocator());
                    rapidjson::StringBuffer buffer;
                    rapidjson::Writer<StringBuffer> writer(buffer);
                    d.Accept(writer);
                    std::string ret = buffer.GetString();
                    HandlerManager::getInstance()->stopHandler("share_fb", ret.c_str());
                }
                else
                {
                    rapidjson::Document d;d.SetObject();
                    rapidjson::Value _error(0);
                    d.AddMember("error", _error, d.GetAllocator());
                    rapidjson::StringBuffer buffer;
                    rapidjson::Writer<StringBuffer> writer(buffer);
                    d.Accept(writer);
                    std::string ret = buffer.GetString();
                    HandlerManager::getInstance()->stopHandler("share_fb", ret.c_str());
                }
                    
            }
        }];
    }
    else
    {
        rapidjson::Document d;d.SetObject();
        rapidjson::Value _error(-1);
        d.AddMember("error", _error, d.GetAllocator());
        rapidjson::StringBuffer buffer;
        rapidjson::Writer<StringBuffer> writer(buffer);
        d.Accept(writer);
        std::string ret = buffer.GetString();
        HandlerManager::getInstance()->stopHandler("share_fb", ret.c_str());
    }
    
}

+(void)sendInvite:(NSString*)listFriends
tinnhan:(NSString*) message
{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:@"apprequests" forKey:@"method"];
    [params setObject:message forKey:@"message"];
    [params setObject:listFriends.autorelease forKey:@"to"];
    
    [FBWebDialogs presentRequestsDialogModallyWithSession:FBSession.activeSession
                                                  message:message
                                                    title:@"Danh sách bạn bè"
                                               parameters:params handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                   if (error) {
                                                       NSLog(@"Error sending request.");
                                                       string jData = "{\"error\":-1}";
                                                       HandlerManager::getInstance()->stopHandler("request", jData);
                                                   } else {
                                                       if (result == FBWebDialogResultDialogNotCompleted) {
                                                           string jData = "{\"error\":-2}";
                                                           HandlerManager::getInstance()->stopHandler("request", jData);
                                                           NSLog(@"User canceled request.");
                                                       } else {
                                                           string _url = [[resultURL description] UTF8String];
                                                           if(_url.compare("fbconnect://success?error_code=4201&error_message=User+canceled+the+Dialog+flow") == 0)
                                                           {
                                                               string jData = "{\"error\":-2}";
                                                               HandlerManager::getInstance()->stopHandler("request", jData);
                                                               NSLog(@"User canceled request.");
                                                               return;
                                                           }
                                                           vector<string> elements;
                                                           std::stringstream ss(_url);string item;
                                                           while (std::getline(ss, item, '&')) {
                                                               elements.push_back(item);
                                                           }
                                                           int number = elements.size() == 0? 0:elements.size() - 1;
                                                           string tmp = "{\"error\":0,\"number\":%d,\"type\":1}";
                                                           string jData = CCString::createWithFormat(tmp.c_str(),number)->getCString();
                                                           NSLog(@"send success");
                                                           HandlerManager::getInstance()->stopHandler("request", jData);
                                                       }
                                                   }
                                               }friendCache:nil];
}

+(void)sendMessage:(NSString *)listFriends
           tinnhan:(NSString *)message
{
    
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:@"send" forKey:@"data"];
    [params setObject:listFriends.autorelease forKey:@"to"];
    
    [FBWebDialogs presentRequestsDialogModallyWithSession:FBSession.activeSession
                                                  message:message
                                                    title:@"Danh sách bạn bè"
                                               parameters:params handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                   if (error) {
                                                       NSLog(@"Error sending request.");
                                                       string jData = "{\"error\":-1}";
                                                       HandlerManager::getInstance()->stopHandler("request", jData);
                                                   } else {
                                                       if (result == FBWebDialogResultDialogNotCompleted) {
                                                           string jData = "{\"error\":-2}";
                                                           HandlerManager::getInstance()->stopHandler("request", jData);
                                                           NSLog(@"User canceled request.");
                                                       } else {
                                                           string _url = [[resultURL description] UTF8String];
                                                           if(_url.compare("fbconnect://success?error_code=4201&error_message=User+canceled+the+Dialog+flow") == 0)
                                                           {
                                                               string jData = "{\"error\":-2}";
                                                               HandlerManager::getInstance()->stopHandler("request", jData);
                                                               NSLog(@"User canceled request.");
                                                               return;
                                                           }
                                                           vector<string> elements;
                                                           std::stringstream ss(_url);string item;
                                                           while (std::getline(ss, item, '&')) {
                                                               elements.push_back(item);
                                                           }

                                                           int number = elements.size() == 0? 0:elements.size() - 1;
                                                           string tmp = "{\"error\":0,\"number\":%d,\"type\":0}";
                                                           string jData = CCString::createWithFormat(tmp.c_str(),number)->getCString();
                                                           HandlerManager::getInstance()->stopHandler("request", jData);
                                                       }
                                                   }
                                               }friendCache:nil];
}

@end
