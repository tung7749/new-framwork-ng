//
//  EmailViewController.h
//  ChineseChess
//
//  Created by Dong Truong Quang on 3/14/14.
//
//

#ifndef ChineseChess_EmailViewController_h
#define ChineseChess_EmailViewController_h

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface EmailViewController : UIViewController <MFMailComposeViewControllerDelegate>
{
    UIViewController *rootViewController;
}
@property (nonatomic, retain) UIViewController *rootViewController;


#pragma mark -
#pragma mark Singleton Methods
+ (EmailViewController *) instance;

-(void) presentViewController:(UIViewController*)controller animated:(BOOL)animated;
-(void) makeToast:(NSString *) message;

-(void) sendEmail:(NSString *)address subject:(NSString*)subject message:(NSString*) message;

@end


#endif
