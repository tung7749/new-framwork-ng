//
//  PlatformWrapper.cpp
//  Farmery
//
//  Created by KienVN on 2/17/14.
//
//

#include "PlatformWrapper.h"
#include "RootViewControllerInterface.h"
#import <UIKit/UIKit.h>

void PlatformWrapper::sendSMS(const char* phone, const char* sms)
{
    NSString *textPhone = [[[NSString alloc]initWithUTF8String: phone] autorelease];
    NSString *textSMS = [[[NSString alloc]initWithUTF8String: sms] autorelease];
    [[RootViewControllerInterface rootViewControllerInterfaceSharedInstance] sendSMS:textPhone message:textSMS];
}
void PlatformWrapper::showNotification(const char* content, long _time )
{
    NSLog(@"showNotification %s, %ld", content, _time);
    
    NSString *text = [[[NSString alloc]initWithUTF8String: content] autorelease];
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:_time];
    localNotification.alertBody = text;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
   

}
void PlatformWrapper::makingCall(std::string phoneNumber)
{
    NSString* number = [NSString stringWithUTF8String:phoneNumber.c_str()];
    NSURL* callURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", number]];
    if([[UIApplication sharedApplication] canOpenURL:callURL])
    {
        [[UIApplication sharedApplication] openURL:callURL];;
    }
    else
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"ALERT" message:@"This function is only available on iPhone" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
}
void PlatformWrapper::cancelAllNotification()
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];

}
void PlatformWrapper::openLink(const char* link)
{
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%s",link]]];
}
void PlatformWrapper::openFacebookLink(const char* httpLink, const char* appLink)
{
    NSURL *facebookURL = [NSURL URLWithString:[NSString stringWithFormat:@"%s",appLink]];
    if ([[UIApplication sharedApplication] canOpenURL:facebookURL]) {
        [[UIApplication sharedApplication] openURL:facebookURL];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%s",httpLink]]];
    }
}

void PlatformWrapper::openSession()
{
    
    //[[ZingPlay shared] openSessionGame];
}

void PlatformWrapper::openUpdate(const char *link)
{
    NSString *text = [[[NSString alloc]initWithUTF8String: link] autorelease];
 //   NSURL *url = [NSURL URLWithString : @text];
    NSURL *url = [NSURL URLWithString:text];
    [[UIApplication sharedApplication] openURL:url];
}

std::string PlatformWrapper::getIMEI()
{
	NSUUID *identify = [[UIDevice currentDevice] identifierForVendor];
    NSString *str = [identify UUIDString];
    return [str UTF8String];
}

void PlatformWrapper::openPortal()
{
    //[[ZingPlay shared] closeGame];
}

void PlatformWrapper::goStore()
{
    //[[ZingPlay shared] goStore];
}

void PlatformWrapper::openWebview(const char *url)
{
    NSString *textUrl = [[[NSString alloc]initWithUTF8String: url] autorelease];
    
    [[RootViewControllerInterface rootViewControllerInterfaceSharedInstance] openWebiew:textUrl];

}

