//
//  ObjCBridgle.h
//  ZPSamLoc
//
//  Created by Hoang Nguyen on 11/19/15.
//
//

#import <Foundation/Foundation.h>

@interface ObjCBridgle : NSObject<UIAlertViewDelegate>
+(NSString *) networkAvaiable;
+(int) type;
@end
