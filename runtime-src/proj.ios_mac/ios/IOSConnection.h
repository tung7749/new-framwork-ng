//
//  IOSConnection.h
//  ChineseChess
//
//  Created by Dong Truong Quang on 3/5/14.
//
//

#ifndef __ChineseChess_IOSConnection_h
#define __ChineseChess_IOSConnection_h
#include "string"

using namespace std;

class IOSConnection
{
public:
    static void callNumberPhone(std::string phoneNumber);
    static std::string getIMEI();
	static std::string getDeviceModel();
    static void saveData(std::string key, std::string value);
    static void deleteData(std::string key);
    static std::string getData(std::string key);
    static bool networkReachable();
    static void makingCall(std::string phoneNumber);
    static void openURL(std::string url);
	static void openURLUpdate(std::string url);
    static std::string getVersion();
    
    static std::string getRefer();
    static std::string getVersionString();
    static std::string getVersionCode();
    static std::string getAppVersion();
    
    static std::string getStringResourceByName(string name);
    
    static int checkNetwork();
    

	static void sendSMS(const char* phone, const char* sms);
    static void sendEmail(std::string address, std::string subject, std::string message);
    static void callZingPlayPortal();               // Goi ZingPlay Portal lay sessionKey
    static void clearZingPlaySessionCache();        // Xoa Cache cua ZingPlayPortal
    static void openPortal();                       // Open pỏtal khi exit
    
    static void sendLogLogin(const char* uID,const char* typeLogin,const char* param3,const char* zName);
    static void sendLogLoginZalo(const char* uID,const char* typeLogin,const char* source,const char* refer);
    
    static std::string getZaloAppID();
	static std::string getZaloAppSecret();
	static std::string getZaloOAucode();
	static std::string getZaloID();
    
    
    static void goStore();
    static void onZingPlayPortalResponse(int error,std::string sessionKey,std::string accessToken,std::string openID,int social);
	static bool checkInstallApp(const char* package);
    static void showNoNetwork();
    
    static void showYesNoDialog(string title,string message,string ok,string cancel,int type);
    static void showOKDialog(string title,string message,string ok,int type);
};


#endif
