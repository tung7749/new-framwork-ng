//
//  RootViewControllerInterface.h
//  cocosViewController
//
//  Created by toni on 25/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBookUI/AddressBookUI.h>
#import <AddressBook/AddressBook.h>
#import <MessageUI/MessageUI.h>



@interface RootViewControllerInterface : NSObject  <ABPeoplePickerNavigationControllerDelegate, MFMessageComposeViewControllerDelegate> {
    UIViewController *rootViewController;
    UIWebView *webView;
    UIButton *btnClose;
    
}

@property (nonatomic, retain) UIViewController *rootViewController;

#pragma mark -
#pragma mark Singleton Methods
+ (RootViewControllerInterface *) rootViewControllerInterfaceSharedInstance;

-(void) presentModalViewController:(UIViewController*)controller animated:(BOOL)animated;
-(void) showAddressBook;

-(void) sendSMS:(NSString *)phoneNumber message:(NSString*) message;
-(void) makeToast:(NSString *) message;
-(void) openWebiew:(NSString *) url;
-(void) hideWebview;
@end
